/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <linux/rtnetlink.h>
#include <linux/ipv6.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_utils.h"
#include "mod_ipm_netlink.h"

#define IP_ADD "AddIPAddress"
#define IP_UPDATE "UpdateIPAddress"
#define IP_DELETE "DeleteIPAddress"

#define IPV6_CONF_DIR "ipv6/conf"

#define ME "ip-mod"

static int mod_update_ipv4address(UNUSED const char* function_name,
                                  amxc_var_t* params,
                                  amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = 1;
    const char* ifname = GET_CHAR(params, "ifname");
    const char* ip_addr = GETP_CHAR(params, "parameters.IPAddress");
    const char* old_ip_addr = GET_CHAR(params, "OldIPAddress");
    const char* subnetmask = GETP_CHAR(params, "parameters.SubnetMask");
    const char* old_subnetmask = GET_CHAR(params, "OldSubnetMask");
    int prefixlen = -1;
    int old_prefixlen = -1;

    when_str_empty_trace(subnetmask, exit, INFO, "No subnetmask set");
    when_str_empty_trace(ifname, exit, INFO, "No interface name provided");
    when_str_empty_trace(ip_addr, exit, INFO, "No IPv4 address provided");
    prefixlen = mask_to_prefix_length(subnetmask, true);
    old_prefixlen = mask_to_prefix_length(old_subnetmask, false);

    if(!str_empty(old_ip_addr) && (old_prefixlen != 0)) {
        SAH_TRACEZ_INFO(ME, "Update the IP on interface '%s' from '%s/%d' to '%s/%d'", ifname, old_ip_addr, old_prefixlen, ip_addr, prefixlen);
        rv = netdev_call_rpc(IP_UPDATE, true, ifname, ip_addr, prefixlen, old_ip_addr, old_prefixlen, ret);
        when_failed_trace(rv, exit, ERROR, "Failed to update from '%s/%d' to '%s/%d'", old_ip_addr, old_prefixlen, ip_addr, prefixlen);
    } else {
        SAH_TRACEZ_INFO(ME, "Add the IP '%s/%d' on interface '%s'", ip_addr, prefixlen, ifname);
        rv = netdev_call_rpc(IP_ADD, true, ifname, ip_addr, prefixlen, NULL, 0, ret);
        when_failed_trace(rv, exit, ERROR, "Failed to add '%s/%d'", ip_addr, prefixlen);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_delete_ipv4address(UNUSED const char* function_name,
                                  amxc_var_t* params,
                                  amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = 1;
    const char* ifname = GET_CHAR(params, "ifname");
    const char* ip_addr = GETP_CHAR(params, "parameters.IPAddress");
    const char* subnetmask = GETP_CHAR(params, "parameters.SubnetMask");
    int prefixlen = -1;

    when_str_empty_trace(ifname, exit, INFO, "No interface name provided");
    when_str_empty_trace(subnetmask, exit, INFO, "No subnetmask set");
    when_str_empty_trace(ip_addr, exit, INFO, "No IP set");

    prefixlen = mask_to_prefix_length(subnetmask, true);

    rv = netdev_call_rpc(IP_DELETE, true, ifname, ip_addr, prefixlen, NULL, 0, ret);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_update_ipv6address(UNUSED const char* function_name,
                                  amxc_var_t* params,
                                  amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = 1;
    const char* ifname = GET_CHAR(params, "ifname");
    const char* ip_addr = GETP_CHAR(params, "parameters.IPAddress");
    const char* old_ip_addr = GET_CHAR(params, "OldIPAddress");
    uint32_t prefixlen = GET_UINT32(params, "PrefixLength");

    when_str_empty_trace(ifname, exit, INFO, "No interface name provided");
    when_str_empty_trace(ip_addr, exit, INFO, "No IPv6 address provided");

    if(!str_empty(old_ip_addr)) {
        rv = netdev_call_rpc(IP_UPDATE, false, ifname, ip_addr, prefixlen, old_ip_addr, prefixlen, ret);
        when_failed_trace(rv, exit, ERROR, "Failed to update from '%s/%d' to '%s/%d'", old_ip_addr, prefixlen, ip_addr, prefixlen);
    } else {
        rv = netdev_call_rpc(IP_ADD, false, ifname, ip_addr, prefixlen, NULL, 0, ret);
        when_failed_trace(rv, exit, ERROR, "Failed to add '%s/%d'", ip_addr, prefixlen);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_delete_ipv6address(UNUSED const char* function_name,
                                  amxc_var_t* params,
                                  amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = 1;
    const char* ip_addr = GETP_CHAR(params, "parameters.IPAddress");
    const char* ifname = GET_CHAR(params, "ifname");
    uint32_t prefixlen = GET_UINT32(params, "PrefixLength");

    when_str_empty_trace(ifname, exit, INFO, "No interface name provided");
    when_str_empty_trace(ip_addr, exit, INFO, "No IPv6 address provided");

    rv = netdev_call_rpc(IP_DELETE, false, ifname, ip_addr, prefixlen, NULL, 0, ret);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_set_ula_prefix(UNUSED const char* function_name,
                              UNUSED amxc_var_t* params,
                              UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "ula prefix does not need to be set when using this module");
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int mod_toggle_ipv6(UNUSED const char* function_name,
                           amxc_var_t* params,
                           UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    const char* ifname = GET_CHAR(params, "ifname");
    bool disable_ipv6 = !GET_BOOL(params, "combined_enable");
    int rv = proc_file_write(IPV6_CONF_DIR, ifname, "disable_ipv6", disable_ipv6);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_set_mtu(UNUSED const char* function_name,
                       amxc_var_t* params,
                       UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    const char* ifname = GET_CHAR(params, "ifname");
    int mtu = GET_INT32(params, "MaxMTUSize");
    int rv = -1;
    int ipv6_path_mtu = 0;
    int accept_ra_mtu = 0;

    when_str_empty_trace(ifname, exit, ERROR, "No interface name provided");

    ipv6_path_mtu = proc_file_read(IPV6_CONF_DIR, ifname, "mtu");
    accept_ra_mtu = proc_file_read(IPV6_CONF_DIR, ifname, "accept_ra_mtu");

    if(mtu < IPV6_MIN_MTU) {
        SAH_TRACEZ_WARNING(ME, "New MTU is lower than minimum IPv6 MTU of %d," \
                           "you might lose IPv6 connectivity on interface %s", IPV6_MIN_MTU, ifname);
    }

    rv = sys_file_write(ifname, "mtu", mtu);

    /*
     * Writing to sysfs also changes the IPv6 MTU setting in procfs.
     * If the IPv6 MTU was configured by router advertisements,
     * write the original value back to procfs if it's smaller than the new MTU
     * and larger than the minimum IPv6 MTU.
     */
    if((ipv6_path_mtu != -1) && (accept_ra_mtu == 1)
       && (ipv6_path_mtu < mtu) && (mtu >= IPV6_MIN_MTU)) {
        proc_file_write(IPV6_CONF_DIR, ifname, "mtu", ipv6_path_mtu);
    }

    when_failed_trace(rv, exit, ERROR, "Failed to set MTU of interface %s to %d", ifname, mtu);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_get_mtu(UNUSED const char* function_name,
                       amxc_var_t* params,
                       amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    const char* ifname = GET_CHAR(params, "ifname");
    int rv = -1;

    when_str_empty_trace(ifname, exit, ERROR, "No interface name provided");
    rv = sys_file_read(ifname, "mtu");
    if(rv != -1) {
        amxc_var_set(uint32_t, ret, rv);
        rv = 0;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_set_neigh_config(UNUSED const char* function_name,
                                amxc_var_t* params,
                                UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    const char* ifname = GET_CHAR(params, "ifname");
    int interval = GET_UINT32(params, "NeighborReachableTime");
    uint32_t ip_version = GET_UINT32(params, "IPVersion");
    amxc_string_t path;
    int rv = -1;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "ipv%u/neigh", ip_version);

    when_str_empty_trace(ifname, exit, ERROR, "No interface name provided");
    rv = proc_file_write(amxc_string_get(&path, 0), ifname, "base_reachable_time_ms", interval);

    when_failed_trace(rv, exit, ERROR, "Failed to set /proc/sys/net/%s/%s/base_reachable_time_ms to %d", amxc_string_get(&path, 0), ifname, interval);

exit:
    amxc_string_clean(&path);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_set_ifname(UNUSED const char* function_name,
                          UNUSED amxc_var_t* params,
                          UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "This module does not set ifname");
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static AMXM_CONSTRUCTOR ipm_netlink_start(void) {
    SAH_TRACEZ_IN(ME);
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    amxm_module_register(&mod, so, MOD_IPM_CTRL);
    amxm_module_add_function(mod, "update-ipv4address", mod_update_ipv4address);
    amxm_module_add_function(mod, "delete-ipv4address", mod_delete_ipv4address);
    amxm_module_add_function(mod, "update-ipv6address", mod_update_ipv6address);
    amxm_module_add_function(mod, "delete-ipv6address", mod_delete_ipv6address);
    amxm_module_add_function(mod, "set-ulaprefix", mod_set_ula_prefix);
    amxm_module_add_function(mod, "toggle-ipv6", mod_toggle_ipv6);
    amxm_module_add_function(mod, "set-mtu", mod_set_mtu);
    amxm_module_add_function(mod, "get-mtu", mod_get_mtu);
    amxm_module_add_function(mod, "set-neigh-config", mod_set_neigh_config);
    amxm_module_add_function(mod, "set-ifname", mod_set_ifname);

    SAH_TRACEZ_OUT(ME);
    return 0;
}

static AMXM_DESTRUCTOR ipm_netlink_stop(void) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_OUT(ME);
    return 0;
}
