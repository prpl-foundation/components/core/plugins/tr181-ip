/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include <asm/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#include <errno.h>
#include <net/if.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_ipm_netlink.h"
#include "mod_utils.h"

#define ME "ip-mod"

int mask_to_prefix_length(const char* mask, bool mandatory) {
    SAH_TRACEZ_IN(ME);
    int i;
    bool breakout = false;
    int prefixlen = 0;
    unsigned char buffer[4];

    if(mandatory) {
        when_str_empty_trace(mask, exit, ERROR, "Failed to convert, subnetmask can not be empty");
    } else {
        when_str_empty_trace(mask, exit, INFO, "No subnetmask provided");
    }

    if(inet_pton(AF_INET, mask, buffer) != 1) {
        SAH_TRACEZ_ERROR(ME, "Failed to convert subnetmask");
        goto exit;
    }

    for(i = 0; i < 4 && !breakout; i++) {
        switch(buffer[i]) {
        case 0x00:
            breakout = true;
            break;
        case 0x80:
            prefixlen += 1;
            breakout = true;
            break;
        case 0xC0:
            prefixlen += 2;
            breakout = true;
            break;
        case 0xE0:
            prefixlen += 3;
            breakout = true;
            break;
        case 0xF0:
            prefixlen += 4;
            breakout = true;
            break;
        case 0xF8:
            prefixlen += 5;
            breakout = true;
            break;
        case 0xFC:
            prefixlen += 6;
            breakout = true;
            break;
        case 0xFE:
            prefixlen += 7;
            breakout = true;
            break;
        case 0xFF:
            prefixlen += 8;
            break;
        default:
            SAH_TRACEZ_ERROR(ME, "Failed to convert subnetmask %s, invalid value", mask);
            prefixlen = -1;
            goto exit;
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return prefixlen;
}

int netdev_call_rpc(const char* method, bool ipv4, const char* ifname, const char* ip_addr, int prefixlen, const char* old_ip_addr, int old_prefixlen, amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("NetDev.");
    amxc_string_t obj_string;
    amxc_var_t netdev_args;

    amxc_string_init(&obj_string, 0);
    amxc_string_setf(&obj_string, "NetDev.Link.[%s].", ifname);
    amxc_var_init(&netdev_args);
    amxc_var_set_type(&netdev_args, AMXC_VAR_ID_HTABLE);

    when_null_trace(ctx, exit, WARNING, "NetDev plugin context not found");
    when_str_empty_trace(ip_addr, exit, WARNING, "No IP address set");
    when_str_empty_trace(ifname, exit, WARNING, "No interface name set");
    when_true_trace(prefixlen <= 0, exit, ERROR, "Invalid prefix length");

    amxc_var_add_key(cstring_t, &netdev_args, "IPAddress", ip_addr);
    amxc_var_add_key(uint32_t, &netdev_args, "PrefixLen", prefixlen);
    amxc_var_add_key(bool, &netdev_args, "IPv4", ipv4);

    if(!str_empty(old_ip_addr)) {
        amxc_var_add_key(cstring_t, &netdev_args, "OldIPAddress", old_ip_addr);
        amxc_var_add_key(uint32_t, &netdev_args, "OldPrefixLen", old_prefixlen);
    }

    rv = amxb_call(ctx, amxc_string_get(&obj_string, 0), method, &netdev_args, ret, 3);
    when_failed_trace(rv, exit, ERROR, "Failed to call '%s' on '%s', error '%d'", method, amxc_string_get(&obj_string, 0), rv);

exit:
    amxc_var_clean(&netdev_args);
    amxc_string_clean(&obj_string);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int file_write(const char* full_path, const int val) {
    SAH_TRACEZ_IN(ME);
    FILE* fd = fopen(full_path, "w");
    int rv = -1;

    when_null_trace(fd, exit, ERROR, "Failed to open file[%s]", full_path);

    if(fprintf(fd, "%d", val) <= 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to write value '%d' to '%s'", val, full_path);
    } else {
        fflush(fd);
        SAH_TRACEZ_INFO(ME, "Value '%d' successfully written to '%s'", val, full_path);
    }

    when_failed_trace(fclose(fd), exit, ERROR, "Failed to close file descriptor: %s", strerror(errno));
    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int file_read(const char* full_path) {
    SAH_TRACEZ_IN(ME);
    FILE* fd = fopen(full_path, "r");
    char buf[32] = {0};
    int result = -1;

    when_null_trace(fd, exit, ERROR, "Failed to open file[%s]", full_path);
    when_null_trace(fgets(buf, sizeof(buf), fd), exit, ERROR, "Failed to read %s", full_path);

    result = atoi(buf);
exit:
    if(fd != NULL) {
        fclose(fd);
        fd = NULL;
    }
    SAH_TRACEZ_OUT(ME);
    return result;
}

int proc_file_read(const char* dir_name, const char* ifname, const char* file_name) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t path;
    const char* full_path = NULL;
    int result = -1;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "/proc/sys/net/%s/%s/%s", dir_name, ifname, file_name);
    full_path = amxc_string_get(&path, 0);

    result = file_read(full_path);

    amxc_string_clean(&path);
    SAH_TRACEZ_OUT(ME);
    return result;
}

int proc_file_write(const char* dir_name, const char* ifname, const char* file_name, const int val) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t path;
    const char* full_path = NULL;
    int rv = -1;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "/proc/sys/net/%s/%s/%s", dir_name, ifname, file_name);
    full_path = amxc_string_get(&path, 0);
    SAH_TRACEZ_INFO(ME, "write %s=%d", full_path, val);
    rv = file_write(full_path, val);

    amxc_string_clean(&path);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int sys_file_write(const char* ifname, const char* file_name, const int val) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t path;
    const char* full_path = NULL;
    int rv = -1;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "/sys/class/net/%s/%s", ifname, file_name);
    full_path = amxc_string_get(&path, 0);

    rv = file_write(full_path, val);

    amxc_string_clean(&path);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int sys_file_read(const char* ifname, const char* file_name) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t path;
    const char* full_path = NULL;
    int result = -1;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "/sys/class/net/%s/%s", ifname, file_name);
    full_path = amxc_string_get(&path, 0);

    result = file_read(full_path);

    amxc_string_clean(&path);
    SAH_TRACEZ_OUT(ME);
    return result;
}
