%populate {
    object IP {
        parameter IPv4Enable = true;
        parameter IPv6Enable = true;
    }

    object IP.Interface {
        instance add("wan") {
            parameter Enable = true;
            parameter IPv4Enable = true;
            parameter IPv6Enable = true;
            parameter ULAEnable = false;
            parameter MaxMTUSize = 1500;
            parameter Type = "Normal";
            parameter Loopback = false;
            parameter LowerLayers="Device.Ethernet.Link.2";

            object "IPv4Address" {
                instance add("static_ipv4") {
                    parameter Enable = true;
                    parameter IPAddress = "192.168.1.1";
                    parameter SubnetMask = "255.255.255.0";
                    parameter AddressingType = "Static";
                }
                instance add("dhcp_ipv4") {
                    parameter Enable = true;
                    parameter AddressingType = "DHCP";
                }
            }

            object 'IPv6Address' {
                instance add("dhcp_ipv6") {
                    parameter Enable = true;
                    parameter Origin = "DHCPv6";
                    parameter InterfaceIDType = "None";
                }
                instance add("autodetected") {
                    parameter Enable = true;
                    parameter Origin = "WellKnown";
                    parameter InterfaceIDType = "None";
                    parameter AutoDetected = 1;
                    parameter IPAddress = "fe80::1234:5678:9abc:deff";
                    parameter InterfaceIDType = "EUI64";
                }
            }

            object 'IPv6Prefix' {
                instance add ("ia_pd") {
                    parameter Enable = true;
                    parameter Origin = "PrefixDelegation";
                    parameter StaticType = "Inapplicable";
                }

                instance add ("child") {
                    parameter Enable = true;
                    parameter Origin = "Child";
                    parameter StaticType = "Inapplicable";
                }
            }
        }
    }
}
