/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxut/amxut_bus.h>
#include <ipat/ipat.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_ip.h"
#include "ip_manager_interface.h"
#include "ip_manager_entrypoint.h"
#include "ip_manager_common.h"
#include "ipv4_addresses.h"
#include "ipv6_addresses.h"
#include "ipv6_prefixes.h"

#include "../common/common_functions.h"
#include "../common/mock_netmodel.h"
#include "test_startup.h"

#define ODL_MOCK_NETMODEL "../common/mock_netmodel.odl"
#define ODL_MOCK_MAIN "../common/mock_ip-manager.odl"
#define ODL_DEFS "../../odl/ip-manager-definition.odl"

#define ODL_DEFAULTS "mock_defaults.odl"

int test_setup(void** state) {
    amxd_dm_t* dm = NULL;
    amxo_parser_t* parser = NULL;
    amxd_object_t* root = NULL;

    amxut_bus_setup(state);

    dm = amxut_bus_dm();
    assert_non_null(dm);
    parser = amxut_bus_parser();
    assert_non_null(parser);
    root = amxd_dm_get_root(dm);
    assert_non_null(root);

    resolver_add_all_functions(parser);
    resolve_dummy_netmodel_functions(parser);

    // Load ODL files
    assert_int_equal(amxo_parser_parse_file(parser, ODL_MOCK_NETMODEL, root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, ODL_MOCK_MAIN, root), 0); // A stripped down version of odl/ip-manager.odl
    assert_int_equal(amxo_parser_parse_file(parser, ODL_DEFS, root), 0);

    // Call entrypoint
    assert_int_equal(_ip_manager_main(AMXO_START, dm, parser), 0);

    // Load the defaults while events are disabled
    amxp_sigmngr_enable(&dm->sigmngr, false);
    assert_int_equal(amxo_parser_parse_file(parser, ODL_DEFAULTS, root), 0);
    amxp_sigmngr_enable(&dm->sigmngr, true);
    amxut_bus_handle_events();

    return 0;
}

int test_teardown(void** state) {

    assert_int_equal(_ip_manager_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser()), 0);
    amxut_bus_teardown(state);

    return 0;
}

/*
 * Helper function to check private data of all the instances in the datamodel
 * If null_expected is true, no instance should have private data,
 * If set to false, all of them should have private data.
 */
static void check_private_data(amxd_object_t* intf_tmpl_obj, bool null_expected) {
    int interfaces = 0;
    int ipv4_addresses = 0;
    int prefixes = 0;
    int ipv6_addresses = 0;

    amxd_object_for_each(instance, it, intf_tmpl_obj) {
        amxd_object_t* intf_obj = amxc_container_of(it, amxd_object_t, it);
        amxd_object_t* ipv4_tmpl_obj = amxd_object_get_child(intf_obj, "IPv4Address");
        amxd_object_t* prefix_tmpl_obj = amxd_object_get_child(intf_obj, "IPv6Prefix");
        amxd_object_t* ipv6_tmpl_obj = amxd_object_get_child(intf_obj, "IPv6Address");
        interfaces++;

        amxd_object_for_each(instance, it, ipv4_tmpl_obj) {
            amxd_object_t* addr_obj = amxc_container_of(it, amxd_object_t, it);
            assert_true((addr_obj->priv == NULL) == null_expected);
            ipv4_addresses++;
        }

        amxd_object_for_each(instance, it, prefix_tmpl_obj) {
            amxd_object_t* prefix_obj = amxc_container_of(it, amxd_object_t, it);
            assert_true((prefix_obj->priv == NULL) == null_expected);
            prefixes++;
        }

        amxd_object_for_each(instance, it, ipv6_tmpl_obj) {
            amxd_object_t* addr_obj = amxc_container_of(it, amxd_object_t, it);
            assert_true((addr_obj->priv == NULL) == null_expected);
            ipv6_addresses++;
        }
    }

    assert_int_equal(interfaces, 3);
    assert_int_equal(ipv4_addresses, 4);
    assert_int_equal(prefixes, 4);
    assert_int_equal(ipv6_addresses, 4);
}

/*
 * Before sending out the app start event, make sure no instance already has private data
 */
void test_before_app_start(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* intf_tmpl_obj = NULL;
    assert_non_null(dm);

    intf_tmpl_obj = amxd_dm_findf(dm, "IP.Interface.");
    assert_non_null(intf_tmpl_obj);
    check_private_data(intf_tmpl_obj, true);
}

/*
 * Calling the entrypoint with a unhandled reason should not affect the behavior
 */
void test_entry_point_with_unhandled_reason(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* intf_tmpl_obj = NULL;
    assert_non_null(dm);

    assert_int_equal(_ip_manager_main(99, dm, amxut_bus_parser()), 0);
    intf_tmpl_obj = amxd_dm_findf(dm, "IP.Interface.");
    assert_non_null(intf_tmpl_obj);
    check_private_data(intf_tmpl_obj, true);
}

/*
 * App start event should call the init function for all instances, after which they all should have private data
 */
void test_call_app_start(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* intf_tmpl_obj = NULL;
    assert_non_null(dm);

    // Emit the app:start event so the defaults get initialised
    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:start", NULL);
    amxut_bus_handle_events();

    intf_tmpl_obj = amxd_dm_findf(dm, "IP.Interface.");
    assert_non_null(intf_tmpl_obj);
    check_private_data(intf_tmpl_obj, false);
}

/*
 * The init function called by the app:start event should also toggle the persistency
 * This test checks a persistent and non persistent example of IPv4, IPv6 and prefix
 * The instances with the corresponding names should have the correct types/origins for this test
 */
void test_persistency_after_start(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* obj = NULL;

    obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.static_ipv4.");
    check_ipv4_param_persistency(obj, true);
    obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.dhcp_ipv4.");
    check_ipv4_param_persistency(obj, false);

    obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.static_ipv6.");
    check_ipv6_param_persistency(obj, true, false);
    obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.dhcp_ipv6.");
    check_ipv6_param_persistency(obj, false, false);

    obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.static_prefix.");
    check_ipv6_prefix_param_persistency(obj, true);
    obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.ia_pd.");
    check_ipv6_prefix_param_persistency(obj, false);
}

static void check_query(ip_intf_info_t* intf_info, netmodel_query_t* query, const char* flags, const char* name) {
    assert_non_null(intf_info);
    assert_non_null(query);
    assert_non_null(query->impl);
    assert_non_null(query->cb);

    const char* path = GET_CHAR(amxd_object_get_param_value(intf_info->obj, "LowerLayers"), NULL);
    assert_non_null(path);
    assert_non_null(query->impl->given_intf);
    assert_string_equal(query->impl->given_intf, path);
    assert_ptr_equal(query->cb->userdata, intf_info);
    assert_string_equal(query->impl->subscriber, "ip-mngr");
    assert_string_equal(GET_CHAR(&query->impl->arguments, "flag"), flags);
    assert_string_equal(GET_CHAR(&query->impl->arguments, "name"), name);
    assert_string_equal(GET_CHAR(&query->impl->arguments, "traverse"), "down");
}

void test_interface_name_queries_opened(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* intf_obj = NULL;
    ip_intf_info_t* intf_info = NULL;
    assert_non_null(dm);

    intf_obj = amxd_dm_findf(dm, "IP.Interface.wan.");
    assert_non_null(intf_obj);

    intf_info = (ip_intf_info_t*) intf_obj->priv;
    check_query(intf_info, intf_info->name_query, "netdev-bound vlan", "NetDevName");
    check_query(intf_info, intf_info->status_query, "netdev-bound vlan", "Status_ext");
    check_query(intf_info, intf_info->mtu_name_query, "netdev-up && !ppp", "NetDevName");

    intf_obj = amxd_dm_findf(dm, "IP.Interface.lan.");
    assert_non_null(intf_obj);

    intf_info = (ip_intf_info_t*) intf_obj->priv;
    check_query(intf_info, intf_info->name_query, "netdev-bound", "NetDevName");
    check_query(intf_info, intf_info->status_query, "netdev-bound", "Status_ext");
    check_query(intf_info, intf_info->mtu_name_query, "netdev-up && !ppp", "NetDevName");

    intf_obj = amxd_dm_findf(dm, "IP.Interface.ppp.");
    assert_non_null(intf_obj);

    intf_info = (ip_intf_info_t*) intf_obj->priv;
    check_query(intf_info, intf_info->name_query, "netdev-bound ppp", "NetDevName");
    check_query(intf_info, intf_info->status_query, "netdev-bound ppp", "Status_ext");
    check_query(intf_info, intf_info->mtu_name_query, "netdev-up && !ppp", "NetDevName");
}

/*
 * Test if the get_object_from_device_path can find the correct object from a given path
 * Tests a non device path
 * Tests indexed device path
 * Tests an Aliased device path
 * All of these should return the correct object
 */
void test_get_object_from_device_path(UNUSED void** state) {
    amxd_object_t* intended_obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.1.IPv4Address.1.");
    amxd_object_t* return_object = NULL;

    //return_object = get_object_from_device_path(NULL);
    //assert_null(return_object);

    //return_object = get_object_from_device_path("");
    //assert_null(return_object);

    return_object = get_object_from_device_path("IP.Interface.1.IPv4Address.1.");
    assert_ptr_equal(return_object, intended_obj);
    return_object = get_object_from_device_path("Device.IP.Interface.1.IPv4Address.1.");
    assert_ptr_equal(return_object, intended_obj);
    return_object = get_object_from_device_path("Device.IP.Interface.wan.IPv4Address.static_ipv4.");
    assert_ptr_equal(return_object, intended_obj);
}
