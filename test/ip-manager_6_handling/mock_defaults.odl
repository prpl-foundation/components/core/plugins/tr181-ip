%populate {
    object IP.Interface {
        instance add("wan") {
            parameter Enable = true;
            parameter IPv4Enable = true;
            parameter IPv6Enable = true;
            parameter ULAEnable = false;
            parameter MaxMTUSize = 1500;
            parameter Type = "Normal";
            parameter Loopback = false;
            parameter LowerLayers="Device.Ethernet.Link.2";

            object "IPv4Address" {
                instance add("static_ipv4") {
                    parameter Enable = true;
                    parameter IPAddress = "192.168.1.1";
                    parameter SubnetMask = "255.255.255.0";
                    parameter AddressingType = "Static";
                }
                instance add("dhcp_ipv4") {
                    parameter Enable = true;
                    parameter AddressingType = "DHCP";
                }
                instance add("ikev2_ipv4") {
                    parameter Enable = true;
                    parameter AddressingType = "IKEv2";
                }
                instance add("autoip_ipv4") {
                    parameter Enable = true;
                    parameter AddressingType = "AutoIP";
                }
                instance add("ipcp_ipv4") {
                    parameter Enable = true;
                    parameter AddressingType = "IPCP";
                }
            }

            object 'IPv6Address' {
                instance add("static_ipv6") {
                    parameter Enable = true;
                    parameter Origin = "Static";
                    parameter InterfaceIDType = "None";
                    parameter IPAddress = "2abc:1234:56:4200:dead:beaf:abba:7004";
                    parameter InterfaceID = "::0:0:0:0/128";
                    parameter RelativeValidLifetime = 4294967295;
                    parameter RelativePreferredLifetime = 4294967295;
                }
                instance add("wellknown_ipv6") {
                    parameter Enable = true;
                    parameter Origin = "WellKnown";
                    parameter IPAddress = "fe80::dead:beaf:abba:7004";
                    parameter InterfaceIDType = "EUI64";
                    parameter RelativeValidLifetime = 4294967295;
                    parameter RelativePreferredLifetime = 4294967295;
                }
                instance add("autoconf_ipv6") {
                    parameter Enable = true;
                    parameter Prefix = "Device.IP.Interface.3.IPv6Prefix.1";
                    parameter Origin = "AutoConfigured";
                }
                instance add("dhcp_ipv6") {
                    parameter Enable = true;
                    parameter Origin = "DHCPv6";
                    parameter InterfaceIDType = "None";
                }
            }

            object 'IPv6Prefix' {
                instance add ("GUA") {
                    parameter Enable = true;
                    parameter Origin = "Child";
                    parameter Prefix = "";
                    parameter Autonomous = true;
                    parameter ParentPrefix = "Device.IP.Interface.2.IPv6Prefix.1";
                    parameter StaticType = "Inapplicable";
                    parameter ChildPrefixBits = "0:0:0:1::/64";
                }

                instance add ("GUA_IAPD") {
                    parameter Enable = true;
                    parameter Origin = "Static";
                    parameter Prefix = "";
                    parameter ParentPrefix = "Device.IP.Interface.2.IPv6Prefix.1";
                    parameter StaticType = "Child";
                    parameter ChildPrefixBits = "0:0:0:20::/60";
                }
            }
        }
    }

    object IP {
        parameter IPv4Enable = true;
        parameter IPv6Enable = true;
    }
}
