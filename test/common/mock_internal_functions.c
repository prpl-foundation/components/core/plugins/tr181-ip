/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_entrypoint.h"
#include "ip_manager_ip.h"
#include "ipv4_addresses.h"
#include "ip_manager_common.h"

#include "mock_module_functions.h"
#include "mock_internal_functions.h"

int __wrap_cfgctrlr_execute_function(UNUSED amxd_object_t* obj, UNUSED const char* const func_name, UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret) {
    int rv = -1;

    when_str_empty(func_name, exit);

    if(strcmp(func_name, "update-ipv4address") == 0) {
        rv = mock_update_ipv4address();
    } else if(strcmp(func_name, "delete-ipv4address") == 0) {
        rv = mock_delete_ipv4address();
    } else if(strcmp(func_name, "update-ipv6address") == 0) {
        rv = mock_update_ipv6address();
    } else if(strcmp(func_name, "delete-ipv6address") == 0) {
        rv = mock_delete_ipv6address();
    } else if(strcmp(func_name, "set-ulaprefix") == 0) {
        rv = mock_set_ulaprefix();
    } else if(strcmp(func_name, "toggle-ipv6") == 0) {
        rv = mock_toggle_ipv6();
    } else if(strcmp(func_name, "set-mtu") == 0) {
        rv = mock_set_mtu();
    } else if(strcmp(func_name, "set-ifname") == 0) {
        rv = mock_set_ifname();
    } else if(strcmp(func_name, "set-neigh-config") == 0) {
        rv = mock_set_neigh_config();
    }

exit:
    return rv;
}

int __wrap_nm_open_dhcpv4_query(UNUSED ipv4_addr_info_t* addr_info) {
    function_called();
    return 0;
}

int __wrap_nm_open_ipcpv4_query(UNUSED ipv4_addr_info_t* addr_info) {
    function_called();
    return 0;
}

int __wrap_update_ipv4_address(UNUSED ipv4_addr_info_t* addr_info) {
    function_called();
    return 0;
}

int __wrap_update_ipv6_address(UNUSED ipv6_addr_info_t* addr_info) {
    function_called();
    return 0;
}

int __wrap_nm_open_dhcpoption_query(UNUSED amxd_object_t* intf_obj,
                                    UNUSED netmodel_query_t** query,
                                    UNUSED const char* type,
                                    UNUSED int option,
                                    UNUSED netmodel_callback_t nm_cb,
                                    UNUSED void* userdata) {
    function_called();
    return 0;
}

void __wrap_start_listening_for_changes(const char* prefix_path, const char* parameter, amxp_slot_fn_t fn, void* const priv) {
    function_called();
}

int __wrap_ipv6_generate_and_set_in_dm(UNUSED ipv6_addr_info_t* addr_info, UNUSED const char* prefix_path) {
    function_called();
    return 0;
}

int __wrap_ipv6_prefix_autoconfigured(UNUSED ipv6_prefix_info_t* prefix_info) {
    function_called();
    return 0;
}

int __wrap_ipv6_prefix_child(UNUSED ipv6_prefix_info_t* prefix_info) {
    function_called();
    return 0;
}

bool __wrap_netdev_name_known(UNUSED amxd_object_t* intf_obj) {
    return true;
}

int __wrap_ipv4_addr_apply(UNUSED ipv4_addr_info_t* addr_info) {
    function_called();
    return 0;
}

int __wrap_ipv4_addr_disable(UNUSED ipv4_addr_info_t* addr_info) {
    function_called();
    return 0;
}

int __wrap_ipv6_addr_apply(UNUSED ipv6_addr_info_t* addr_info) {
    function_called();
    return 0;
}

int __wrap_ipv6_addr_disable(UNUSED ipv6_addr_info_t* addr_info) {
    function_called();
    return 0;
}

int __wrap_ipv6_prefix_apply(UNUSED ipv6_prefix_info_t* addr_info) {
    function_called();
    return 0;
}

int __wrap_ipv6_prefix_disable(UNUSED ipv6_prefix_info_t* addr_info) {
    function_called();
    return 0;
}

int __wrap_subscribe_if_added_to_netdev(UNUSED amxd_object_t* intf_obj, UNUSED amxd_object_t* addr_obj, UNUSED const char* ip_addr, UNUSED uint8_t ip_family) {
    function_called();
    return 0;
}
int __wrap_dm_ipv6_prefix_set(UNUSED const amxc_string_t* prefix, UNUSED ipv6_prefix_info_t* prefix_info, UNUSED const amxc_ts_t* preferred_lt, UNUSED const amxc_ts_t* valid_lt, UNUSED uint32_t plt, UNUSED uint32_t vlt) {
    function_called();
    return 0;
}


