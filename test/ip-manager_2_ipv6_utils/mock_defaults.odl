%populate {

    object IP.Interface {
        instance add("loopback") {
            parameter Enable = true;
            parameter IPv4Enable = true;
            parameter IPv6Enable = true;
            parameter ULAEnable = false;
            parameter MaxMTUSize = 65535;
            parameter Type = "Loopback";
            parameter Loopback = true;
            parameter LowerLayers="Device.Ethernet.Link.1";
        }

        instance add("wan") {
            parameter Enable = true;
            parameter IPv4Enable = true;
            parameter IPv6Enable = true;
            parameter ULAEnable = false;
            parameter MaxMTUSize = 1500;
            parameter Type = "Normal";
            parameter Loopback = false;
            parameter UpLink = true;
            parameter LowerLayers="Device.Ethernet.Link.2";
        }

        instance add("lan") {
            parameter Enable = true;
            parameter IPv4Enable = true;
            parameter IPv6Enable = true;
            parameter ULAEnable = false;
            parameter MaxMTUSize = 1500;
            parameter Type = "Normal";
            parameter Loopback = false;
            parameter LowerLayers="Device.Ethernet.Link.3";
        }

        instance add("ppp") {
            parameter Enable = true;
            parameter IPv4Enable = true;
            parameter IPv6Enable = true;
            parameter ULAEnable = false;
            parameter MaxMTUSize = 1500;
            parameter Type = "Normal";
            parameter Loopback = false;
            parameter LowerLayers="Device.PPP.Interface.1";
        }
    }

// IPv6 defaults
    object IP {
        parameter ULAPrefix = "fd01:2345:6789::/48";
        parameter IPv4Enable = true;
        parameter IPv6Enable = true;
    }

    object IP.Interface.lan {
        parameter ULAEnable = 1;
    }

    object IP.Interface.loopback.IPv6Address {
        instance add("loopbackipv6") {
            parameter Enable = true;
            parameter IPAddress = "::1";
            parameter Prefix = "";
            parameter Origin = "WellKnown";
        }

        instance add("PERSISTENT") {
            parameter Enable = true;
            parameter IPAddress = "::1";
            parameter Prefix = "";
            parameter Origin = "WellKnown";
        }
    }

    object IP.Interface.wan.IPv6Prefix {
        instance add ("GUA_IAPD") {
            parameter Enable = true;
            parameter Origin = "PrefixDelegation";
            parameter StaticType = "Inapplicable";
            parameter Prefix = "fedc::/56";
        }

        instance add ("GUA_RA") {
            parameter Enable = true;
            parameter Origin = "RouterAdvertisement";
            parameter Autonomous = true;
            parameter StaticType = "Inapplicable";
            parameter Prefix = "fedd::/56";
        }

        instance add ("DHCP_IAPD") {
            parameter Enable = true;
            parameter Origin = "Child";
            parameter Prefix = "";
            parameter Autonomous = true;
            parameter ParentPrefix = "Device.IP.Interface.2.IPv6Prefix.1";
            parameter StaticType = "Inapplicable";
            parameter ChildPrefixBits = "0:0:0:FFFF::/64";
        }
    }

    object IP.Interface.wan.IPv6Address {
        instance add ("GUA_RA") {
            parameter Enable = true;
            parameter Origin = "AutoConfigured";
            parameter Prefix = "Device.IP.Interface.2.IPv6Prefix.2";
        }

        instance add("DHCP_IAPD") {
            parameter Enable = true;
            parameter Prefix = "Device.IP.Interface.2.IPv6Prefix.3";
            parameter Origin = "AutoConfigured";
        }
    }

    object IP.Interface.lan.IPv6Address {
        instance add("ULA") {
            parameter Enable = true;
            parameter IPAddress = "";
            parameter Prefix = "Device.IP.Interface.3.IPv6Prefix.1";
            parameter Origin = "AutoConfigured";
        }

        instance add("GUA") {
            parameter Enable = true;
            parameter IPAddress = "";
            parameter Prefix = "Device.IP.Interface.3.IPv6Prefix.2";
            parameter Origin = "AutoConfigured";
            parameter InterfaceIDType = "Fixed";
            parameter InterfaceID = "::1/128";
        }
    }

    object IP.Interface.lan.IPv6Prefix {
        instance add ("ULA64") {
            parameter Enable = true;
            parameter Origin = "AutoConfigured";
            parameter Prefix = "";
            parameter Autonomous = true;
            parameter ChildPrefixBits = "0:0:0:1::/64";
        }

        instance add ("GUA") {
            parameter Enable = true;
            parameter Origin = "Child";
            parameter Prefix = "";
            parameter Autonomous = true;
            parameter ParentPrefix = "Device.IP.Interface.2.IPv6Prefix.1";
            parameter StaticType = "Inapplicable";
            parameter ChildPrefixBits = "0:0:0:0::/64";
        }

        instance add ("GUA_IAPD") {
            parameter Enable = true;
            parameter Origin = "Child";
            parameter Prefix = "";
            parameter ParentPrefix = "Device.IP.Interface.2.IPv6Prefix.1";
            parameter StaticType = "Inapplicable";
            parameter ChildPrefixBits = "0:0:0:10::/60";
        }

        instance add ("PERSISTENT") {
            parameter Enable = true;
            parameter Origin = "Static";
            parameter StaticType = "Inapplicable";
            parameter Prefix = "";
            parameter Autonomous = true;
            parameter ChildPrefixBits = "0:0:0:1::/64";
        }
    }
}
