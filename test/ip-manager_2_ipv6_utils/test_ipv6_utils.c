/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxo/amxo.h>

#include <cmocka.h>
#include <amxut/amxut_bus.h>

#include "ip_manager_ip.h"
#include "ip_manager_common.h"
#include "ip_manager_entrypoint.h"
#include "ipv6_utils.h"
#include "ipv6_prefixes.h"
#include "ipv6_addresses.h"

#include "../common/common_functions.h"
#include "../common/mock_netmodel.h"
#include "test_ipv6_utils.h"

#define ODL_MOCK_NETMODEL "../common/mock_netmodel.odl"
#define ODL_NETDEV_DEFINITION "../common/mock_netdev_dm/netdev_definitions.odl"
#define ODL_NETDEV_DEFAULTS "mock_netdev_defaults.odl"
#define ODL_MOCK_MAIN "../common/mock_ip-manager.odl"
#define ODL_DEFS "../../odl/ip-manager-definition.odl"

#define ODL_DEFAULTS "mock_defaults.odl"

int test_setup(void** state) {

    common_setup(state, true, true);
    // Two name queries will be opened for every interface (name_query and mtu_name_query), name should be in list twice
    set_query_getResult_value("lo lo eth0 eth0 br-lan br-lan ppp ppp");
    amxut_bus_handle_events();

    return 0;
}

int test_teardown(void** state) {

    common_teardown(state);

    return 0;
}

/*
 * Test that controls the persistency of the following parameters in the IPv6's interfaces:
 * IPAddress, Prefix, PreferredLifetime and ValidLifetime following the Origin value set to "WellKnown" or "Static".
 */
void test_ipv6_address_persistent(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* obj = amxd_dm_findf(dm, "IP.Interface.loopback.IPv6Address.PERSISTENT.");
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    // Should be persistent to start
    check_ipv6_param_persistency(obj, true, false);

    // We are going to change the Origin parameter artificially and see if the persistency changes accordingly
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "IP.Interface.loopback.IPv6Address.PERSISTENT.");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "AutoConfigured");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();

    // Persistency should be removed
    check_ipv6_param_persistency(obj, false, false);

    amxd_trans_clean(&trans);
}

void test_ipv6_prefix_persistent(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* obj = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Prefix.PERSISTENT.");
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    check_ipv6_prefix_param_persistency(obj, false);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "IP.Interface.lan.IPv6Prefix.PERSISTENT.");
    amxd_trans_set_value(cstring_t, &trans, "StaticType", "Static");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    check_ipv6_prefix_param_persistency(obj, true);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "IP.Interface.lan.IPv6Prefix.PERSISTENT.");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "AutoConfigured");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();

    check_ipv6_prefix_param_persistency(obj, false);

    amxd_trans_clean(&trans);
}

void test_get_prefix_object_from_addr(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* prefix_1_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.GUA_RA.");
    // GUA_RA address should have its prefix param path set to prefix_1_obj in mock_defaults.odl
    amxd_object_t* address_1_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.GUA_RA.");
    // This address should have an empty prefix param path in mock_defaults.odl
    amxd_object_t* address_no_prefix_obj = amxd_dm_findf(dm, "IP.Interface.loopback.IPv6Address.loopbackipv6.");
    amxd_object_t* returned_obj = NULL;

    returned_obj = get_prefix_object_from_addr(NULL);
    assert_null(returned_obj);
    returned_obj = get_prefix_object_from_addr(returned_obj);
    assert_null(returned_obj);
    returned_obj = get_prefix_object_from_addr(address_no_prefix_obj);
    assert_null(returned_obj);

    // Valid options
    returned_obj = get_prefix_object_from_addr(address_1_obj);
    assert_ptr_equal(returned_obj, prefix_1_obj);

}

void test_dm_ipv6_prefix_set(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* return_object = NULL;
    amxc_string_t test;
    ipv6_prefix_info_t* prefix_info = NULL;
    char* prefix = NULL;
    amxc_string_init(&test, 0);

    assert_int_equal(dm_ipv6_prefix_set(NULL, NULL, NULL, NULL, 0, 0), -1);
    assert_int_equal(dm_ipv6_prefix_set(&test, NULL, NULL, NULL, 0, 0), -1);

    amxc_string_set(&test, "fedc:0:0:10::/60");
    return_object = amxd_dm_findf(dm, "IP.Interface.2.IPv6Prefix.1.");
    assert_non_null(return_object);
    prefix_info = (ipv6_prefix_info_t*) return_object->priv;
    assert_non_null(prefix_info);
    assert_int_equal(dm_ipv6_prefix_set(&test, prefix_info, NULL, NULL, 0, 0), 0);

    assert_int_equal(check_param_equals("Prefix", return_object, "fedc:0:0:10::/60"), 0);

    assert_int_equal(dm_ipv6_prefix_set(NULL, prefix_info, NULL, NULL, 0, 0), 0);
    assert_int_equal(check_param_equals("Prefix", return_object, ""), 0);

    amxc_string_clean(&test);
}

void test_ipv6_add_eui(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* return_object = NULL;
    amxd_object_t* netdev_obj = NULL;
    amxc_string_t test;

    amxc_string_init(&test, 0);
    return_object = amxd_dm_findf(dm, "IP.Interface.2.");
    assert_non_null(return_object);

    // Invalid inputs
    assert_int_equal(ipv6_add_eui(NULL, &test), -1);
    assert_int_equal(ipv6_add_eui(return_object, NULL), -1);

    // No prefix length
    amxc_string_set(&test, "fedc:0:0:10::");
    assert_int_equal(ipv6_add_eui(return_object, &test), -1);

    // MACAddress not yet set
    amxc_string_set(&test, "fedc:0:0:10::/64");
    assert_int_equal(ipv6_add_eui(return_object, &test), -1);

    // Set the MAC address
    netdev_obj = amxd_dm_findf(dm, "NetDev.Link.1.");
    assert_non_null(netdev_obj);
    assert_int_equal(amxd_object_set_value(cstring_t, netdev_obj, "LLAddress", "01:23:45:67:89:AB"), 0);

    amxc_string_set(&test, "fedc:0:0:10::/64");
    assert_int_equal(ipv6_add_eui(return_object, &test), 0);
    assert_string_equal(amxc_string_get(&test, 0), "fedc::10:323:45ff:fe67:89ab");

    amxc_string_set(&test, "fedc::/64");
    assert_int_equal(ipv6_add_eui(return_object, &test), 0);
    assert_string_equal(amxc_string_get(&test, 0), "fedc::323:45ff:fe67:89ab");

    amxc_string_clean(&test);
}

void test_ipv6_add_eui_ppp(UNUSED void** state) {
    /* ipv6_add_eui ignores prefixes for ppp -> all string should be empty */
    amxd_object_t* return_object = NULL;
    amxc_string_t test;

    amxc_string_init(&test, 0);
    return_object = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.ppp.");
    assert_non_null(return_object);

    assert_int_equal(ipv6_add_eui(NULL, &test), -1);
    assert_int_equal(ipv6_add_eui(return_object, NULL), -1);

    amxc_string_set(&test, "2b12:3456:78:9A::");
    assert_int_equal(ipv6_add_eui(return_object, &test), 0);
    assert_string_equal(amxc_string_get(&test, 0), "");

    amxc_string_set(&test, "2b12:3456:78:9A::/64");
    assert_int_equal(ipv6_add_eui(return_object, &test), 0);
    assert_string_equal(amxc_string_get(&test, 0), "");

    amxc_string_set(&test, "2b12::/64");
    assert_int_equal(ipv6_add_eui(return_object, &test), 0);
    assert_string_equal(amxc_string_get(&test, 0), "");

    amxc_string_set(&test, "2b12:3456:78:9A:1234::/64");
    assert_int_equal(ipv6_add_eui(return_object, &test), 0);
    assert_string_equal(amxc_string_get(&test, 0), "");

    amxc_string_clean(&test);
}

void test_ipv6_add_fixed(UNUSED void** state) {
    amxd_object_t* obj = NULL;
    ipv6_addr_info_t* addr_info = NULL;
    amxc_string_t test;

    amxc_string_init(&test, 0);
    obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.lan.IPv6Address.GUA");

    addr_info = (ipv6_addr_info_t*) obj->priv;

    assert_int_equal(ipv6_add_fixed(NULL, &test), -1);
    assert_int_equal(ipv6_add_fixed(addr_info, NULL), -1);

    amxc_string_set(&test, "fedc:0:0:10::");                // If no prefix length provided 128 is assumed
    assert_int_equal(ipv6_add_fixed(addr_info, &test), 0);  // Default fixed Prefix used ::1/128
    assert_string_equal(amxc_string_get(&test, 0), "fedc:0:0:10::");

    amxc_string_set(&test, "fedc:0:0:10::/64");
    assert_int_equal(ipv6_add_fixed(addr_info, &test), 0); // Default fixed Prefix used ::1/128
    assert_string_equal(amxc_string_get(&test, 0), "fedc:0:0:10::1");

    amxc_string_set(&test, "fedc::/64");
    assert_int_equal(amxd_object_set_value(cstring_t, addr_info->addr_obj, "InterfaceID", "::1234:5678:9ABC:0DEF/128"), 0);
    assert_int_equal(ipv6_add_fixed(addr_info, &test), 0);
    assert_string_equal(amxc_string_get(&test, 0), "fedc::1234:5678:9abc:def");

    amxc_string_clean(&test);
}
