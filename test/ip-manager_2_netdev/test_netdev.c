/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>

#include <cmocka.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_entrypoint.h"
#include "ip_manager_ip.h"
#include "ip_manager_common.h"
#include "ip_manager_netdev.h"

#include "../common/common_functions.h"
#include "test_netdev.h"

#define ODL_NETDEV_INSTANCES "mock_netdev_instances.odl"

#define ODL_DEFAULTS "mock_defaults.odl"

int test_setup(void** state) {

    common_setup(state, true, false);
    amxut_bus_handle_events();

    return 0;
}

int test_teardown(void** state) {

    common_teardown(state);

    return 0;
}

static void check_netdev_params(amxd_object_t* addr_obj,
                                const char* flags,
                                const char* scope,
                                const char* type_flags,
                                const char* peer,
                                uint32_t prefixlen,
                                const char* status) {
    amxc_var_t addr_params;

    assert_non_null(addr_obj);
    amxc_var_init(&addr_params);
    amxc_var_set_type(&addr_params, AMXC_VAR_ID_HTABLE);
    amxd_object_get_params(addr_obj, &addr_params, amxd_dm_access_protected);

    assert_string_equal(GET_CHAR(&addr_params, "Flags"), flags);
    assert_string_equal(GET_CHAR(&addr_params, "Scope"), scope);
    assert_string_equal(GET_CHAR(&addr_params, "TypeFlags"), type_flags);
    assert_string_equal(GET_CHAR(&addr_params, "Peer"), peer);
    assert_int_equal(GET_UINT32(&addr_params, "PrefixLen"), prefixlen);
    assert_string_equal(GET_CHAR(&addr_params, "Status"), status);

    amxc_var_clean(&addr_params);
}

static void check_ip_params(amxd_object_t* addr_obj) {
    amxc_var_t addr_params;

    assert_non_null(addr_obj);
    amxc_var_init(&addr_params);
    amxc_var_set_type(&addr_params, AMXC_VAR_ID_HTABLE);
    amxd_object_get_params(addr_obj, &addr_params, amxd_dm_access_protected);

    assert_string_equal(GET_CHAR(&addr_params, "Alias"), "LLA");
    assert_string_equal(GET_CHAR(&addr_params, "IPAddress"), "fe80::dead:beef:cafe:bad");
    assert_string_equal(GET_CHAR(&addr_params, "Origin"), "WellKnown");
    assert_int_equal(GET_UINT32(&addr_params, "RelativePreferredLifetime"), 4294967295);
    assert_int_equal(GET_UINT32(&addr_params, "RelativeValidLifetime"), 4294967295);

    amxc_var_clean(&addr_params);
}

void test_netdev_ipvx_addr_param_change_subscribe_invalid(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* addr_v4_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv4Address.1.");
    amxd_object_t* addr_v6_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv6Address.1.");

    assert_int_equal(netdev_ipv4_addr_param_change_subscribe(NULL, NULL), -1);
    assert_int_equal(netdev_ipv6_addr_param_change_subscribe(NULL, NULL), -1);

    assert_int_equal(netdev_ipv4_addr_param_change_subscribe(NULL, "NetDev.Link.1.IPv4Addr.1."), -1);
    assert_int_equal(netdev_ipv6_addr_param_change_subscribe(NULL, "NetDev.Link.1.IPv6Addr.1."), -1);

    assert_int_equal(netdev_ipv4_addr_param_change_subscribe(addr_v4_obj, ""), -1);
    assert_int_equal(netdev_ipv6_addr_param_change_subscribe(addr_v6_obj, ""), -1);

    assert_int_equal(netdev_ipv4_addr_param_change_subscribe(addr_v4_obj, NULL), -1);
    assert_int_equal(netdev_ipv6_addr_param_change_subscribe(addr_v6_obj, NULL), -1);
}

void test_netdev_ipv4_addr_param_change_subscribe(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* addr_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv4Address.1.");
    amxc_var_t addr_params;
    amxd_trans_t trans;

    // Make sure that the values are not yet filled in
    assert_non_null(addr_obj);
    check_netdev_params(addr_obj, "", "", "", "", 0, "Disabled");

    // Call the function and make sure that the values are initialised
    assert_int_equal(netdev_ipv4_addr_param_change_subscribe(addr_obj, "NetDev.Link.1.IPv4Addr.1."), 0);
    check_netdev_params(addr_obj, "permanent", "global", "@private", "8.8.8.8", 24, "Enabled");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.IPv4Addr.1.");
    amxd_trans_set_value(cstring_t, &trans, "Flags", "homeaddress");
    amxd_trans_set_value(cstring_t, &trans, "Scope", "host");
    amxd_trans_set_value(cstring_t, &trans, "TypeFlags", "@loopback");
    amxd_trans_set_value(cstring_t, &trans, "Peer", "9.9.9.9");
    amxd_trans_set_value(uint8_t, &trans, "PrefixLen", 28);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    // After handling the event, the values should be changed in the IP instance as well
    check_netdev_params(addr_obj, "homeaddress", "host", "@loopback", "9.9.9.9", 28, "Enabled");

    // Change the info partial to make sure the parameter that are not changed are not cleared
    // Also give the same value for TypeFlags
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.IPv4Addr.1.");
    amxd_trans_set_value(cstring_t, &trans, "Flags", "permanent");
    amxd_trans_set_value(cstring_t, &trans, "TypeFlags", "@loopback");
    amxd_trans_set_value(uint8_t, &trans, "PrefixLen", 24);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    check_netdev_params(addr_obj, "permanent", "host", "@loopback", "9.9.9.9", 24, "Enabled");
}

void test_netdev_ipv6_addr_param_change_subscribe(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* addr_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv6Address.1.");
    amxc_var_t addr_params;
    amxd_trans_t trans;

    // Make sure that the values are not yet filled in
    assert_non_null(addr_obj);
    check_netdev_params(addr_obj, "", "", "", "", 0, "Disabled");

    // Call the function and make sure that the values are initialised
    assert_int_equal(netdev_ipv6_addr_param_change_subscribe(addr_obj, "NetDev.Link.1.IPv6Addr.1."), 0);
    check_netdev_params(addr_obj, "deprecated", "global", "@gua", "2345:6789:ab:cd::/64", 64, "Enabled");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.IPv6Addr.1.");
    amxd_trans_set_value(cstring_t, &trans, "Flags", "permanent");
    amxd_trans_set_value(cstring_t, &trans, "Scope", "host");
    amxd_trans_set_value(cstring_t, &trans, "TypeFlags", "@loopback");
    amxd_trans_set_value(cstring_t, &trans, "Peer", "::1/128");
    amxd_trans_set_value(uint8_t, &trans, "PrefixLen", 128);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    // After handling the event, the values should be changed in the IP instance as well
    check_netdev_params(addr_obj, "permanent", "host", "@loopback", "::1/128", 128, "Enabled");

    // Change the info partial to make sure the parameter that are not changed are not cleared
    // Also give the same value for TypeFlags
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.IPv6Addr.1.");
    amxd_trans_set_value(cstring_t, &trans, "Flags", "deprecated");
    amxd_trans_set_value(cstring_t, &trans, "TypeFlags", "@loopback");
    amxd_trans_set_value(uint8_t, &trans, "PrefixLen", 64);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    check_netdev_params(addr_obj, "deprecated", "host", "@loopback", "::1/128", 64, "Enabled");
}

/*
 * Before this tests, the test setup is teared down and setup again
 * This ensures a clean environment
 */
void test_ip_address_open_subscriptions(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* intf_obj = amxd_dm_findf(dm, "IP.Interface.1.");
    amxd_object_t* addr_v4_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv4Address.1.");
    amxd_object_t* addr_v6_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv6Address.1.");
    amxd_object_t* addr_v6_lla_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv6Address.2.");
    amxd_trans_t trans;

    assert_non_null(addr_v4_obj);
    assert_non_null(addr_v6_obj);
    // LLA instance should not exist in IP at the start
    assert_null(addr_v6_lla_obj);
    check_netdev_params(addr_v4_obj, "", "", "", "", 0, "Disabled");
    check_netdev_params(addr_v6_obj, "", "", "", "", 0, "Disabled");

    // Name should match with the name in mock_netdev_defaults
    assert_int_equal(ip_address_open_subscriptions(intf_obj, "eth0"), 0);

    // An initial sync should be done after creating the subscription
    check_netdev_params(addr_v4_obj, "permanent", "global", "@private", "8.8.8.8", 24, "Enabled");
    check_netdev_params(addr_v6_obj, "deprecated", "global", "@gua", "2345:6789:ab:cd::/64", 64, "Enabled");

    // Check if the initial LLA instance was added
    addr_v6_lla_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv6Address.2.");
    assert_non_null(addr_v6_lla_obj);
    check_netdev_params(addr_v6_lla_obj, "permanent", "link", "@lla", "", 64, "Enabled");
    check_ip_params(addr_v6_lla_obj);
}

/*
 * This test continues on test_ip_address_open_subscriptions
 * The defaults instances are removed in NetDev
 * The instance status in IP should be set to Disabled
 */
void test_ip_address_subscriptions_remove(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* intf_obj = amxd_dm_findf(dm, "IP.Interface.1.");
    amxd_object_t* addr_v4_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv4Address.1.");
    amxd_object_t* addr_v6_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv6Address.1.");
    amxd_object_t* addr_v6_lla_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv6Address.2.");
    amxd_trans_t trans;

    assert_non_null(addr_v4_obj);
    assert_non_null(addr_v6_obj);
    assert_non_null(addr_v6_lla_obj);

    // Remove the default instances from the DM
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.IPv4Addr.");
    amxd_trans_del_inst(&trans, 1, NULL);
    amxd_trans_select_pathf(&trans, "NetDev.Link.1.IPv6Addr.");
    amxd_trans_del_inst(&trans, 1, NULL);
    amxd_trans_del_inst(&trans, 2, NULL);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    // When an interface is removed, the status should go to disabled
    check_netdev_params(addr_v4_obj, "permanent", "global", "@private", "8.8.8.8", 24, "Disabled");
    check_netdev_params(addr_v6_obj, "deprecated", "global", "@gua", "2345:6789:ab:cd::/64", 64, "Disabled");

    // LLA instance should be removed from IP if removed in NetDev
    addr_v6_lla_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv6Address.2.");
    assert_null(addr_v6_lla_obj);
}

/*
 * This test continues on test_ip_address_subscriptions_remove
 * New instances are added to NetDev by loading the ODL_NETDEV_INSTANCES odl
 * The instances should link to the instances in IP and be enabled
 */
void test_ip_address_subscriptions_add(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* intf_obj = amxd_dm_findf(dm, "IP.Interface.1.");
    amxd_object_t* addr_v4_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv4Address.1.");
    amxd_object_t* addr_v6_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv6Address.1.");
    amxd_object_t* addr_v6_lla_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv6Address.2.");

    assert_non_null(addr_v4_obj);
    assert_non_null(addr_v6_obj);
    assert_null(addr_v6_lla_obj);

    assert_int_equal(amxo_parser_parse_file(amxut_bus_parser(), ODL_NETDEV_INSTANCES, amxd_dm_get_root(dm)), 0);
    amxut_bus_handle_events();

    // When new interface is removed, the status should go to disabled
    check_netdev_params(addr_v4_obj, "permanent", "global", "@private", "8.8.8.8", 24, "Enabled");
    check_netdev_params(addr_v6_obj, "deprecated", "global", "@gua", "2345:6789:ab:cd::/64", 64, "Enabled");

    // Check if the LLA instance was added from the event
    addr_v6_lla_obj = amxd_dm_findf(dm, "IP.Interface.1.IPv6Address.3."); // Index should be increased (we should not reuse indexes)
    assert_non_null(addr_v6_lla_obj);
    check_netdev_params(addr_v6_lla_obj, "permanent", "link", "@lla", "", 64, "Enabled");
    check_ip_params(addr_v6_lla_obj);
}