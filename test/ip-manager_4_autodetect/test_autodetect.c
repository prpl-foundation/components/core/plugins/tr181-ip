/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include <cmocka.h>
#include <amxut/amxut_bus.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_entrypoint.h"
#include "ip_manager_ip.h"
#include "ip_manager_common.h"
#include "ipv6_utils.h"
#include "ipv6_prefixes.h"
#include "ipv6_addresses.h"

#include "../common/common_functions.h"
#include "../common/mock_netmodel.h"
#include "test_autodetect.h"

#define ODL_MOCK_NETMODEL "../common/mock_netmodel.odl"
#define ODL_NETDEV_DEFINITION "../common/mock_netdev_dm/netdev_definitions.odl"
#define ODL_NETDEV_DEFAULTS "mock_netdev_defaults.odl"
#define ODL_MOCK_MAIN "../common/mock_ip-manager.odl"
#define ODL_DEFS "../../odl/ip-manager-definition.odl"

#define ODL_DEFAULTS "mock_defaults.odl"

int test_setup(void** state) {

    common_setup(state, true, true);
    // Two name queries will be opened for every interface (name_query and mtu_name_query), name should be in list twice
    set_query_getResult_value("eth0 eth0 br-lan br-lan");
    amxut_bus_handle_events();

    return 0;
}

int test_teardown(void** state) {

    common_teardown(state);

    return 0;
}

void test_autodetect_link_address(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    //Test if the LLA address are in the IP-manager datamodel
    amxd_object_t* lla_ip = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.[Alias == 'LLA'].");

    assert_non_null(lla_ip);

    lla_ip = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.[Alias == 'LLA'].");
    assert_non_null(lla_ip);
}

void test_autodetect_add_address(UNUSED void** state) {
    //Test for adding an IP address to NetDev
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxd_object_t* ip = NULL;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.eth0.IPv6Addr.");
    amxd_trans_add_inst(&trans, 0, "MAN");
    amxd_trans_set_value(cstring_t, &trans, "Address", "fe78::10:18ff:fe01:c01");
    amxd_trans_set_value(int32_t, &trans, "CreatedTimestamp", 10);
    amxd_trans_set_value(cstring_t, &trans, "Flags", "permanent");
    amxd_trans_set_value(cstring_t, &trans, "Scope", "link");
    amxd_trans_set_value(cstring_t, &trans, "TypeFlags", "@mua");
    amxd_trans_set_value(int32_t, &trans, "UpdatedTimestamp", 31);

    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();

    ip = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.[Alias == 'MUA'].");
    assert_non_null(ip);
    assert_int_equal(check_param_equals("IPAddress", ip, "fe78::10:18ff:fe01:c01"), 0);
    assert_int_equal(check_param_equals("Origin", ip, "WellKnown"), 0);
    assert_int_equal(check_param_equals("Status", ip, "Enabled"), 0);

    amxd_trans_clean(&trans);
}

void test_autodetect_add_duplicate(UNUSED void** state) {
    //Test for adding an IP address to NetDev
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxd_object_t* ip = NULL;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.eth0.IPv6Addr.");
    amxd_trans_add_inst(&trans, 0, "MAN2");
    amxd_trans_set_value(cstring_t, &trans, "Address", "fe78::10:18ff:fe01:c01");
    amxd_trans_set_value(int32_t, &trans, "CreatedTimestamp", 10);
    amxd_trans_set_value(cstring_t, &trans, "Flags", "permanent");
    amxd_trans_set_value(cstring_t, &trans, "Scope", "link");
    amxd_trans_set_value(cstring_t, &trans, "TypeFlags", "@mua");
    amxd_trans_set_value(int32_t, &trans, "UpdatedTimestamp", 31);

    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();

    ip = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.[Alias == 'MUA'].");
    assert_non_null(ip);
    assert_int_equal(check_param_equals("IPAddress", ip, "fe78::10:18ff:fe01:c01"), 0);
    assert_int_equal(check_param_equals("Origin", ip, "WellKnown"), 0);
    assert_int_equal(check_param_equals("Status", ip, "Enabled"), 0);

    // amxd_dm_findf returns NULL if more than one instance matches the search so if not NULL we should only have one instance
    ip = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.[IPAddress == 'fe78::10:18ff:fe01:c01'].");
    assert_non_null(ip);
    assert_int_equal(check_param_equals("Alias", ip, "MUA"), 0);
    assert_int_equal(check_param_equals("Origin", ip, "WellKnown"), 0);
    assert_int_equal(check_param_equals("Status", ip, "Enabled"), 0);

    amxd_trans_clean(&trans);
}

void test_autodetect_remove_address(UNUSED void** state) {
    //Test for removing an IP address to NetDev
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxd_object_t* ip = NULL;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.eth0.IPv6Addr.");
    amxd_trans_del_inst(&trans, 0, "MAN");

    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();

    ip = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.[Alias == 'MUA'].");
    if(ip == NULL) {
        ip = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.[Alias == 'MUA'].");
    }
    assert_null(ip);

    amxd_trans_clean(&trans);
}

void test_autodetect_change_address(UNUSED void** state) {
    //Test the change of the IP instance in NetDev and results in IP-manager
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxd_object_t* ip = NULL;
    int8_t lt = 0;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "NetDev.Link.br-lan.IPv6Addr.");
    amxd_trans_add_inst(&trans, 0, "MAN");
    amxd_trans_set_value(cstring_t, &trans, "Address", "fe78::10:18ff:fe01:c01");
    amxd_trans_set_value(int32_t, &trans, "CreatedTimestamp", 10);
    amxd_trans_set_value(cstring_t, &trans, "Flags", "permanent");
    amxd_trans_set_value(cstring_t, &trans, "Scope", "link");
    amxd_trans_set_value(cstring_t, &trans, "TypeFlags", "@mua");
    amxd_trans_set_value(int32_t, &trans, "UpdatedTimestamp", 31);

    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();

    ip = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.[Alias == 'MUA'].");
    assert_non_null(ip);

    lt = amxd_object_get_value(int8_t, ip, "PrefixLen", NULL);
    assert_int_equal(lt, 0);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "NetDev.Link.br-lan.IPv6Addr.[Alias == 'MAN'].");
    amxd_trans_set_value(int32_t, &trans, "PrefixLen", 64);

    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();

    ip = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.[Alias == 'MUA'].");
    assert_non_null(ip);

    lt = amxd_object_get_value(int8_t, ip, "PrefixLen", NULL);
    assert_int_equal(lt, 64);

    amxd_trans_clean(&trans);
}

// For this test there is already a duplicated instance in br-lan
// We expect the duplicate instances to be removed and the new one to be added
void test_autodetect_add_with_duplicate(UNUSED void** state) {
    //Test for adding an IP address to NetDev
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    ipv6_addr_info_t* addr_info = NULL;
    amxd_object_t* ip = NULL;

    // Make the addresses as auto detected in the internal structure
    ip = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.[Alias == 'test1'].");
    addr_info = (ipv6_addr_info_t*) ip->priv;
    addr_info->auto_detected = true;
    ip = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.[Alias == 'test2'].");
    addr_info = (ipv6_addr_info_t*) ip->priv;
    addr_info->auto_detected = true;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetDev.Link.br-lan.IPv6Addr.");
    amxd_trans_add_inst(&trans, 0, "DUP");
    amxd_trans_set_value(cstring_t, &trans, "Address", "fe80::10:0bad:fe01:c01");
    amxd_trans_set_value(int32_t, &trans, "CreatedTimestamp", 10);
    amxd_trans_set_value(cstring_t, &trans, "Flags", "permanent");
    amxd_trans_set_value(cstring_t, &trans, "Scope", "link");
    amxd_trans_set_value(cstring_t, &trans, "TypeFlags", "@DUP");
    amxd_trans_set_value(int32_t, &trans, "UpdatedTimestamp", 31);

    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();

    // The default instances should be removed
    ip = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.[Alias == 'test1'].");
    assert_null(ip);
    ip = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.[Alias == 'test2'].");
    assert_null(ip);

    ip = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.[Alias == 'DUP'].");
    assert_non_null(ip);
    assert_int_equal(check_param_equals("IPAddress", ip, "fe80::10:0bad:fe01:c01"), 0);
    assert_int_equal(check_param_equals("Origin", ip, "WellKnown"), 0);
    assert_int_equal(check_param_equals("Status", ip, "Enabled"), 0);

    // amxd_dm_findf returns NULL if more than one instance matches the search so if not NULL we should only have one instance
    ip = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.[IPAddress == 'fe80::10:0bad:fe01:c01'].");
    assert_non_null(ip);
    assert_int_equal(check_param_equals("Alias", ip, "DUP"), 0);
    assert_int_equal(check_param_equals("Origin", ip, "WellKnown"), 0);
    assert_int_equal(check_param_equals("Status", ip, "Enabled"), 0);

    amxd_trans_clean(&trans);
}
