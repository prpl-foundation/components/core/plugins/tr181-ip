/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>

#include <cmocka.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_entrypoint.h"
#include "ip_manager_ip.h"
#include "ip_manager_common.h"
#include "ipv6_utils.h"
#include "ipv6_prefixes.h"
#include "ipv6_addresses.h"

#include "../common/common_functions.h"
#include "test_lifetimes.h"

#define ODL_MOCK_MAIN "../common/mock_ip-manager.odl"
#define ODL_DEFS "../../odl/ip-manager-definition.odl"

#define ODL_DEFAULTS "mock_defaults.odl"

static void test_relative_lifetimes_update(amxd_object_t* obj, int32_t plt, int32_t vlt, const char* status_param, const char* expected_status) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxc_ts_t* test_time = NULL;
    amxc_ts_t* preferredlifetime = NULL;
    amxc_ts_t* validlifetime = NULL;
    uint32_t lower_plt = plt - 1;
    uint32_t lower_vlt = vlt - 1;

    test_time = (amxc_ts_t*) calloc(1, sizeof(amxc_ts_t));
    preferredlifetime = (amxc_ts_t*) calloc(1, sizeof(amxc_ts_t));
    validlifetime = (amxc_ts_t*) calloc(1, sizeof(amxc_ts_t));

    assert_non_null(obj);
    amxd_trans_init(&trans);
    amxc_var_init(&params);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, obj);

    assert_int_equal(amxc_ts_now(test_time), 0);
    preferredlifetime->sec = test_time->sec + plt;
    validlifetime->sec = test_time->sec + vlt;

    amxd_trans_set_value(amxc_ts_t, &trans, "PreferredLifetime", preferredlifetime);
    amxd_trans_set_value(amxc_ts_t, &trans, "ValidLifetime", validlifetime);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    assert_int_equal(amxd_object_get_params(obj, &params, amxd_dm_access_protected), 0);

    if(plt < 0) {
        plt = 0;
        lower_plt = 0;
    }
    assert_in_range(GET_INT32(&params, "RelativePreferredLifetime"), lower_plt, plt);
    if(vlt < 0) {
        vlt = 0;
        lower_vlt = 0;
    }
    assert_in_range(GET_INT32(&params, "RelativeValidLifetime"), lower_vlt, vlt);
    assert_non_null(GET_CHAR(&params, status_param));
    assert_string_equal(GET_CHAR(&params, status_param), expected_status);

    amxc_var_clean(&params);
    free(test_time);
    free(preferredlifetime);
    free(validlifetime);
}

static void validate_timestamp(amxc_ts_t* timestamp, const char* time) {
    char timestamp_str[36];
    assert_true(amxc_ts_is_valid(timestamp));
    amxc_ts_format_precision(timestamp, timestamp_str, sizeof(timestamp_str), 0);
    assert_non_null(timestamp_str);
    assert_string_equal(timestamp_str, time);
}

int test_setup(void** state) {

    common_setup(state, false, false);
    amxut_bus_handle_events();

    return 0;
}

int test_teardown(void** state) {

    common_teardown(state);

    return 0;
}

void test_toggle_lifetime_read_only_invalid(UNUSED void** state) {
    assert_int_equal(toggle_lifetime_read_only(NULL, true), -1);
    assert_int_equal(toggle_lifetime_read_only(NULL, false), -1);
}

static void check_toggle_lifetime_read_only(const char* path) {
    amxd_object_t* obj = amxd_dm_findf(amxut_bus_dm(), path);
    amxd_param_t* preferred_lifetime = NULL;
    amxd_param_t* valid_lifetime = NULL;

    assert_non_null(obj);
    preferred_lifetime = amxd_object_get_param_def(obj, "PreferredLifetime");
    valid_lifetime = amxd_object_get_param_def(obj, "ValidLifetime");

    assert_non_null(preferred_lifetime);
    assert_non_null(valid_lifetime);

    // Static addresses should not be read only
    assert_false(amxd_param_is_attr_set(preferred_lifetime, amxd_pattr_read_only));
    assert_false(amxd_param_is_attr_set(valid_lifetime, amxd_pattr_read_only));

    // Make it read only
    toggle_lifetime_read_only(obj, true);
    assert_true(amxd_param_is_attr_set(preferred_lifetime, amxd_pattr_read_only));
    assert_true(amxd_param_is_attr_set(valid_lifetime, amxd_pattr_read_only));

    // Remove the read only
    toggle_lifetime_read_only(obj, false);
    assert_false(amxd_param_is_attr_set(preferred_lifetime, amxd_pattr_read_only));
    assert_false(amxd_param_is_attr_set(valid_lifetime, amxd_pattr_read_only));
}

void test_toggle_lifetime_read_only_addr(UNUSED void** state) {
    check_toggle_lifetime_read_only("IP.Interface.wan.IPv6Address.[Alias == 'STATIC_ADDRESS'].");
}

void test_toggle_lifetime_read_only_prefix(UNUSED void** state) {
    check_toggle_lifetime_read_only("IP.Interface.wan.IPv6Prefix.[Alias == 'STATIC_PREFIX'].");
}

void test_calculate_lifetimes(UNUSED void** state) {
    amxc_ts_t preferred_lt;
    amxc_ts_t valid_lt;
    amxc_ts_t test_time;
    int32_t time_diff = 0;

    // Setting a valid time that is smaller as the preferred time should failed
    assert_int_equal(calculate_lifetimes(100, 50, &preferred_lt, &valid_lt), -1);

    // A relative time of 0 should result in a time stamp equal to "0001-01-01T00:00:00Z"
    assert_int_equal(calculate_lifetimes(0, 0, &preferred_lt, &valid_lt), 0);
    validate_timestamp(&preferred_lt, UNKNOWN_LIFETIME);
    validate_timestamp(&valid_lt, UNKNOWN_LIFETIME);

    // A relative time of UINT32_MAX should result in a time stamp equal to "9999-12-31T23:59:59Z"
    assert_int_equal(calculate_lifetimes(UINT32_MAX, UINT32_MAX, &preferred_lt, &valid_lt), 0);
    validate_timestamp(&preferred_lt, INFINITE_LIFETIME);
    validate_timestamp(&valid_lt, INFINITE_LIFETIME);

    // For relative times > 0 and < UINT32_MAX the absolute time should be calculated
    assert_int_equal(amxc_ts_now(&test_time), 0);
    assert_int_equal(calculate_lifetimes(500, 1000, &preferred_lt, &valid_lt), 0);
    assert_int_equal(amxc_ts_compare(&preferred_lt, &valid_lt), -1);

    time_diff = valid_lt.sec - preferred_lt.sec;
    assert_in_range(time_diff, 499, 500);
    time_diff = preferred_lt.sec - test_time.sec;
    assert_in_range(time_diff, 499, 500);
    time_diff = valid_lt.sec - test_time.sec;
    assert_in_range(time_diff, 999, 1000);

}

/**
 * Static IPv6Addresses should have an infinite lifetime by default
 * Expectation:
 *      * PreferredLifetime and ValidLifetime should equal "9999-12-31T23:59:59Z"
 *      * RelativePreferredLifetime and RelativeValidLifetime should be set to the max value for a UINT32
 *      * IPv6AddressStatus should be "Preferred"
 */
void test_static_addr_lifetimes(UNUSED void** state) {
    amxc_var_t params;
    amxc_ts_t* plt = NULL;
    amxc_ts_t* vlt = NULL;
    amxd_object_t* obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.IPv6Address.[Alias == 'STATIC_ADDRESS'].");

    assert_non_null(obj);
    amxc_var_init(&params);

    assert_int_equal(amxd_object_get_params(obj, &params, amxd_dm_access_protected), 0);

    plt = amxc_var_dyncast(amxc_ts_t, GET_ARG(&params, "PreferredLifetime"));
    vlt = amxc_var_dyncast(amxc_ts_t, GET_ARG(&params, "ValidLifetime"));

    validate_timestamp(plt, INFINITE_LIFETIME);
    validate_timestamp(vlt, INFINITE_LIFETIME);
    assert_int_equal(GET_UINT32(&params, "RelativePreferredLifetime"), UINT32_MAX);
    assert_int_equal(GET_UINT32(&params, "RelativeValidLifetime"), UINT32_MAX);
    assert_non_null(GET_CHAR(&params, "IPAddressStatus"));
    assert_string_equal(GET_CHAR(&params, "IPAddressStatus"), ADDR_STATUS_PREFERRED);

    amxc_var_clean(&params);
    free(plt);
    free(vlt);
}

/**
 * Static IPv6Prefixes should have an infinite lifetime by default
 * Expectation:
 *      * PreferredLifetime and ValidLifetime should equal "9999-12-31T23:59:59Z"
 *      * RelativePreferredLifetime and RelativeValidLifetime should be set to the max value for a UINT32
 *      * IPv6PrefixesStatus should be "Preferred"
 */
void test_static_prefix_lifetimes(UNUSED void** state) {
    amxc_var_t params;
    amxc_ts_t* plt = NULL;
    amxc_ts_t* vlt = NULL;
    amxd_object_t* obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.IPv6Prefix.[Alias == 'STATIC_PREFIX'].");

    assert_non_null(obj);
    amxc_var_init(&params);

    assert_int_equal(amxd_object_get_params(obj, &params, amxd_dm_access_protected), 0);

    plt = amxc_var_dyncast(amxc_ts_t, GET_ARG(&params, "PreferredLifetime"));
    vlt = amxc_var_dyncast(amxc_ts_t, GET_ARG(&params, "ValidLifetime"));

    validate_timestamp(plt, INFINITE_LIFETIME);
    validate_timestamp(vlt, INFINITE_LIFETIME);
    assert_int_equal(GET_UINT32(&params, "RelativePreferredLifetime"), UINT32_MAX);
    assert_int_equal(GET_UINT32(&params, "RelativeValidLifetime"), UINT32_MAX);
    assert_non_null(GET_CHAR(&params, "PrefixStatus"));
    assert_string_equal(GET_CHAR(&params, "PrefixStatus"), PREFIX_STATUS_PREFERRED);

    amxc_var_clean(&params);
    free(plt);
    free(vlt);
}

static void update_lifetime(amxd_object_t* obj) {
    uint32_t plt = 500;
    uint32_t vlt = 1000;
    char preferred[36];
    char valid[36];
    amxd_trans_t trans;
    amxc_ts_t preferred_lt;
    amxc_ts_t valid_lt;

    assert_non_null(obj);

    calculate_lifetimes(plt, vlt, &preferred_lt, &valid_lt);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, obj);
    amxc_ts_format_precision(&preferred_lt, preferred, sizeof(preferred), 0);
    amxd_trans_set_value(cstring_t, &trans, "PreferredLifetime", preferred);
    amxc_ts_format_precision(&valid_lt, valid, sizeof(valid), 0);
    amxd_trans_set_value(cstring_t, &trans, "ValidLifetime", valid);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
}

/*
 * This tests sets the preferred lifetime for the STATIC_ADDRESS to 500s into the future and a valid lifetime 1000s into the future
 * During the first 500s IPAddressStatus should be equal to ADDR_STATUS_PREFERRED, after 500s in should become ADDR_STATUS_DEPRECATED
 * When the full 1000s are over, the status should go to ADDR_STATUS_INVALID
 * The test uses amxut_timer_go_to_future_ms to skip time and go into the future
 */
void test_addr_lifetime_expiration(UNUSED void** state) {
    amxd_object_t* obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.IPv6Address.[Alias == 'STATIC_ADDRESS'].");

    assert_non_null(obj);
    update_lifetime(obj);

    assert_int_equal(check_param_equals("IPAddressStatus", obj, ADDR_STATUS_PREFERRED), 0);

    amxut_timer_go_to_future_ms(490000);
    assert_int_equal(check_param_equals("IPAddressStatus", obj, ADDR_STATUS_PREFERRED), 0);

    amxut_timer_go_to_future_ms(10000);
    assert_int_equal(check_param_equals("IPAddressStatus", obj, ADDR_STATUS_DEPRECATED), 0);

    amxut_timer_go_to_future_ms(490000);
    assert_int_equal(check_param_equals("IPAddressStatus", obj, ADDR_STATUS_DEPRECATED), 0);

    amxut_timer_go_to_future_ms(10000);
    assert_int_equal(check_param_equals("IPAddressStatus", obj, ADDR_STATUS_INVALID), 0);
}

/*
 * This tests sets the preferred lifetime for the STATIC_PREFIX to 500s into the future and a valid lifetime 1000s into the future
 * During the first 500s PrefixStatus should be equal to ADDR_STATUS_PREFERRED, after 500s in should become PREFIX_STATUS_DEPRECATED
 * When the full 1000s are over, the status should go to PREFIX_STATUS_INVALID
 * The test uses amxut_timer_go_to_future_ms to skip time and go into the future
 */
void test_prefix_lifetime_expiration(UNUSED void** state) {
    amxd_object_t* obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.IPv6Prefix.[Alias == 'STATIC_PREFIX'].");

    assert_non_null(obj);
    update_lifetime(obj);

    assert_int_equal(check_param_equals("PrefixStatus", obj, PREFIX_STATUS_PREFERRED), 0);

    amxut_timer_go_to_future_ms(490000);
    assert_int_equal(check_param_equals("PrefixStatus", obj, PREFIX_STATUS_PREFERRED), 0);

    amxut_timer_go_to_future_ms(10000);
    assert_int_equal(check_param_equals("PrefixStatus", obj, PREFIX_STATUS_DEPRECATED), 0);

    amxut_timer_go_to_future_ms(490000);
    assert_int_equal(check_param_equals("PrefixStatus", obj, PREFIX_STATUS_DEPRECATED), 0);

    amxut_timer_go_to_future_ms(10000);
    assert_int_equal(check_param_equals("PrefixStatus", obj, PREFIX_STATUS_INVALID), 0);
}

/*
 * Test that when setting a lifetime on an address, the relative lifetimes update accordingly
 * Also verifies the status
 */
void test_update_addr_lifetimes(UNUSED void** state) {
    amxd_object_t* obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.IPv6Address.[Alias == 'STATIC_ADDRESS'].");
    test_relative_lifetimes_update(obj, 1800, 3600, "IPAddressStatus", ADDR_STATUS_PREFERRED);
    test_relative_lifetimes_update(obj, 3600, 1800, "IPAddressStatus", ADDR_STATUS_PREFERRED);
    test_relative_lifetimes_update(obj, -100, 1800, "IPAddressStatus", ADDR_STATUS_DEPRECATED);
    test_relative_lifetimes_update(obj, 3600, -100, "IPAddressStatus", ADDR_STATUS_INVALID);
    test_relative_lifetimes_update(obj, -200, -100, "IPAddressStatus", ADDR_STATUS_INVALID);
}

/*
 * Tests that when setting a lifetime on a prefix, the relative lifetimes update accordingly
 * Also verifies the status
 */
void test_update_prefix_lifetimes(UNUSED void** state) {
    amxd_object_t* obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.wan.IPv6Prefix.[Alias == 'STATIC_PREFIX'].");
    test_relative_lifetimes_update(obj, 1800, 3600, "PrefixStatus", PREFIX_STATUS_PREFERRED);
    test_relative_lifetimes_update(obj, 3600, 1800, "PrefixStatus", PREFIX_STATUS_PREFERRED);
    test_relative_lifetimes_update(obj, -100, 1800, "PrefixStatus", PREFIX_STATUS_DEPRECATED);
    test_relative_lifetimes_update(obj, 3600, -100, "PrefixStatus", PREFIX_STATUS_INVALID);
    test_relative_lifetimes_update(obj, -200, -100, "PrefixStatus", PREFIX_STATUS_INVALID);
}
