/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include <cmocka.h>
#include <amxut/amxut_bus.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_entrypoint.h"
#include "ip_manager_ip.h"
#include "ip_manager_common.h"
#include "ipv4_addresses.h"
#include "ipv6_utils.h"
#include "ipv6_prefixes.h"
#include "ipv6_addresses.h"

#include "../common/common_functions.h"
#include "../common/mock_netmodel.h"
#include "mock_internal_functions.h"
#include "mock_module_functions.h"
#include "test_apply.h"

#define ODL_MOCK_NETMODEL "../common/mock_netmodel.odl"
#define ODL_MOCK_MAIN "../common/mock_ip-manager.odl"
#define ODL_DEFS "../../odl/ip-manager-definition.odl"

#define ODL_DEFAULTS "mock_defaults.odl"

int test_setup(void** state) {

    common_setup(state, false, false);
    amxut_bus_handle_events();

    return 0;
}

int test_teardown(void** state) {

    ignore_function_calls(mock_delete_ipv4address);
    ignore_function_calls(mock_delete_ipv6address);
    common_teardown(state);

    return 0;
}

/*
 * This tests mocks the functions that are called by ipv4_addr_apply to make sure if the correct functions are called.
 * For this test to work, ipv4_addr_apply can not be defined in the same file as any of the functions that are mocked.
 * If not, the function can not be wrapped and the original version will still be called.
 */
void test_ipv4_addr_apply(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* addr_obj = NULL;

    // Expect a call to update_ipv4_address
    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.static_ipv4.");
    assert_non_null(addr_obj);
    expect_function_call(__wrap_update_ipv4_address);
    assert_int_equal(ipv4_addr_apply(addr_obj->priv), 0);

    // Expect a call to nm_open_dhcpv4_query
    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.dhcp_ipv4.");
    assert_non_null(addr_obj);
    expect_function_call(__wrap_nm_open_dhcpv4_query);
    assert_int_equal(ipv4_addr_apply(addr_obj->priv), 0);

    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.ipcp_ipv4.");
    assert_non_null(addr_obj);
    expect_function_call(__wrap_nm_open_ipcpv4_query);
    assert_int_equal(ipv4_addr_apply(addr_obj->priv), 0);

    // No functions expected to be called for these AddressTypes
    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.ikev2_ipv4.");
    assert_non_null(addr_obj);
    assert_int_equal(ipv4_addr_apply(addr_obj->priv), 0);

    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.autoip_ipv4.");
    assert_non_null(addr_obj);
    assert_int_equal(ipv4_addr_apply(addr_obj->priv), 0);
}

/*
 * This tests mocks the functions that are called by ipv6_addr_apply to make sure if the correct functions are called.
 * For this test to work, ipv6_addr_apply can not be defined in the same file as any of the functions that are mocked.
 * If not, the function can not be wrapped and the original version will still be called.
 */
void test_ipv6_addr_apply(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* addr_obj = NULL;

    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.static_ipv6.");
    assert_non_null(addr_obj);
    expect_function_call(__wrap_update_ipv6_address);
    assert_int_equal(ipv6_addr_apply(addr_obj->priv), 0);

    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.wellknown_ipv6.");
    assert_non_null(addr_obj);
    expect_function_call(__wrap_update_ipv6_address);
    assert_int_equal(ipv6_addr_apply(addr_obj->priv), 0);

    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.autoconf_ipv6.");
    assert_non_null(addr_obj);
    expect_function_call(__wrap_start_listening_for_changes);
    expect_function_call(__wrap_ipv6_generate_and_set_in_dm);
    assert_int_equal(ipv6_addr_apply(addr_obj->priv), 0);

    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.dhcp_ipv6.");
    assert_non_null(addr_obj);
    expect_function_call(__wrap_nm_open_dhcpoption_query);
    assert_int_equal(ipv6_addr_apply(addr_obj->priv), 0);
}

/*
 * This tests mocks the functions that are called by ipv6_prefix_apply to make sure if the correct functions are called.
 * For this test to work, ipv6_prefix_apply can not be defined in the same file as any of the functions that are mocked.
 * If not, the function can not be wrapped and the original version will still be called.
 */
void test_ipv6_prefix_apply(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* prefix_obj = NULL;

    prefix_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.child.");
    assert_non_null(prefix_obj);
    expect_function_call(__wrap_ipv6_prefix_child);
    assert_int_equal(ipv6_prefix_apply(prefix_obj->priv), 0);

    prefix_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.static.");
    assert_non_null(prefix_obj);
    expect_function_call(__wrap_ipv6_prefix_child);
    assert_int_equal(ipv6_prefix_apply(prefix_obj->priv), 0);

    prefix_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.iapd.");
    assert_non_null(prefix_obj);
    expect_function_call(__wrap_nm_open_dhcpoption_query);
    assert_int_equal(ipv6_prefix_apply(prefix_obj->priv), 0);

    prefix_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.ra.");
    assert_non_null(prefix_obj);
    expect_function_call(__wrap_nm_open_dhcpoption_query);
    assert_int_equal(ipv6_prefix_apply(prefix_obj->priv), 0);

    prefix_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.autoconf.");
    assert_non_null(prefix_obj);
    expect_function_call(__wrap_ipv6_prefix_autoconfigured);
    assert_int_equal(ipv6_prefix_apply(prefix_obj->priv), 0);

    prefix_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.static_pd.");
    assert_non_null(prefix_obj);
    // Expect no function calls and a NOK return
    assert_int_equal(ipv6_prefix_apply(prefix_obj->priv), -1);
}

/*
 * This tests mocks the functions that are called by ipv4_address_apply.
 * All instances in mock_defaults.odl will be disabled, non of the mocked functions should be called
 * For this test to work, ipv4_address_apply can not be defined in the same file as any of the functions that are mocked.
 * If not, the function can not be wrapped and the original version will still be called.
 */
void test_ipv4_addr_apply_disabled(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* addr_obj = NULL;
    amxd_object_t* templ_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.");

    amxd_object_for_each(instance, it, templ_obj) {
        amxd_object_t* obj = amxc_container_of(it, amxd_object_t, it);
        amxd_object_set_value(bool, obj, "Enable", false);
    }

    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.static_ipv4.");
    assert_non_null(addr_obj);
    assert_int_equal(ipv4_addr_apply(addr_obj->priv), 0);

    // Expect a call to nm_open_dhcpv4_query
    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.dhcp_ipv4.");
    assert_non_null(addr_obj);
    assert_int_equal(ipv4_addr_apply(addr_obj->priv), 0);

    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.ipcp_ipv4.");
    assert_non_null(addr_obj);
    assert_int_equal(ipv4_addr_apply(addr_obj->priv), 0);

    // No functions expected to be called for these AddressTypes
    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.ikev2_ipv4.");
    assert_non_null(addr_obj);
    assert_int_equal(ipv4_addr_apply(addr_obj->priv), 0);

    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv4Address.autoip_ipv4.");
    assert_non_null(addr_obj);
    assert_int_equal(ipv4_addr_apply(addr_obj->priv), 0);
}

/*
 * This tests mocks the functions that are called by ipv6_address_apply.
 * All instances in mock_defaults.odl will be disabled, non of the mocked functions should be called
 * For this test to work, ipv6_address_apply can not be defined in the same file as any of the functions that are mocked.
 * If not, the function can not be wrapped and the original version will still be called.
 */
void test_ipv6_addr_apply_disabled(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.static_ipv6.");
    amxd_object_t* templ_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.");

    amxd_object_for_each(instance, it, templ_obj) {
        amxd_object_t* obj = amxc_container_of(it, amxd_object_t, it);
        amxd_object_set_value(bool, obj, "Enable", false);
    }

    assert_non_null(addr_obj);
    assert_int_equal(ipv6_addr_apply(addr_obj->priv), 0);

    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.wellknown_ipv6.");
    assert_non_null(addr_obj);
    assert_int_equal(ipv6_addr_apply(addr_obj->priv), 0);

    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.autoconf_ipv6.");
    assert_non_null(addr_obj);
    assert_int_equal(ipv6_addr_apply(addr_obj->priv), 0);

    addr_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Address.dhcp_ipv6.");
    assert_non_null(addr_obj);
    assert_int_equal(ipv6_addr_apply(addr_obj->priv), 0);
}

/*
 * This tests mocks the functions that are called by ipv6_prefix_apply.
 * All instances in mock_defaults.odl will be disabled, non of the mocked functions should be called
 * For this test to work, ipv6_prefix_apply can not be defined in the same file as any of the functions that are mocked.
 * If not, the function can not be wrapped and the original version will still be called.
 */
void test_ipv6_prefix_apply_disabled(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* prefix_obj = NULL;
    amxd_object_t* templ_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.");

    amxd_object_for_each(instance, it, templ_obj) {
        amxd_object_t* obj = amxc_container_of(it, amxd_object_t, it);
        amxd_object_set_value(bool, obj, "Enable", false);
    }

    prefix_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.child.");
    assert_non_null(prefix_obj);
    assert_int_equal(ipv6_prefix_apply(prefix_obj->priv), 0);

    prefix_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.static.");
    assert_non_null(prefix_obj);
    assert_int_equal(ipv6_prefix_apply(prefix_obj->priv), 0);

    prefix_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.iapd.");
    assert_non_null(prefix_obj);
    assert_int_equal(ipv6_prefix_apply(prefix_obj->priv), 0);

    prefix_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.ra.");
    assert_non_null(prefix_obj);
    assert_int_equal(ipv6_prefix_apply(prefix_obj->priv), 0);

    prefix_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.autoconf.");
    assert_non_null(prefix_obj);
    assert_int_equal(ipv6_prefix_apply(prefix_obj->priv), 0);

    prefix_obj = amxd_dm_findf(dm, "IP.Interface.wan.IPv6Prefix.static_pd.");
    assert_non_null(prefix_obj);
    assert_int_equal(ipv6_prefix_apply(prefix_obj->priv), 0);
}

void test_ipv4_address_static_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv4Address.");
    amxd_trans_add_inst(&trans, 0, "new_static");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.10.1");
    amxd_trans_set_value(cstring_t, &trans, "SubnetMask", "255.255.255.0");
    amxd_trans_set_value(cstring_t, &trans, "AddressingType", "Static");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    expect_function_call(__wrap_update_ipv4_address);
    amxut_bus_handle_events();
}

void test_ipv4_address_dhcp_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv4Address.");
    amxd_trans_add_inst(&trans, 0, "new_dhcp");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.10.1");
    amxd_trans_set_value(cstring_t, &trans, "SubnetMask", "255.255.255.0");
    amxd_trans_set_value(cstring_t, &trans, "AddressingType", "DHCP");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    expect_function_call(__wrap_nm_open_dhcpv4_query);
    amxut_bus_handle_events();
}

void test_ipv4_address_ipcp_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv4Address.");
    amxd_trans_add_inst(&trans, 0, "new_ipcp");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.10.1");
    amxd_trans_set_value(cstring_t, &trans, "SubnetMask", "255.255.255.0");
    amxd_trans_set_value(cstring_t, &trans, "AddressingType", "IPCP");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    expect_function_call(__wrap_nm_open_ipcpv4_query);
    amxut_bus_handle_events();
}

void test_ipv4_address_static_toggle(UNUSED void** state) {
    trans_set_enable("IP.Interface.wan.IPv4Address.new_static.", false);
    expect_function_call(mock_delete_ipv4address);
    amxut_bus_handle_events();

    trans_set_enable("IP.Interface.wan.IPv4Address.new_static.", true);
    expect_function_call(__wrap_update_ipv4_address);
    amxut_bus_handle_events();
}

void test_ipv4_address_dhcp_toggle(UNUSED void** state) {
    trans_set_enable("IP.Interface.wan.IPv4Address.new_dhcp.", false);
    // Called twice once because of ipv4_addr_delete in ipv4_addr_disable
    // Once because of the make_ip_address_empty in ipv4_addr_disable
    expect_function_call(mock_delete_ipv4address);
    expect_function_call(mock_delete_ipv4address);
    amxut_bus_handle_events();

    trans_set_enable("IP.Interface.wan.IPv4Address.new_dhcp.", true);
    expect_function_call(__wrap_nm_open_dhcpv4_query);
    amxut_bus_handle_events();
}

void test_ipv4_address_ipcp_toggle(UNUSED void** state) {
    trans_set_enable("IP.Interface.wan.IPv4Address.new_ipcp.", false);
    // Called twice once because of ipv4_addr_delete in ipv4_addr_disable
    // Once because of the make_ip_address_empty in ipv4_addr_disable
    expect_function_call(mock_delete_ipv4address);
    expect_function_call(mock_delete_ipv4address);
    amxut_bus_handle_events();

    trans_set_enable("IP.Interface.wan.IPv4Address.new_ipcp.", true);
    expect_function_call(__wrap_nm_open_ipcpv4_query);
    amxut_bus_handle_events();
}

void test_ipv6_address_static_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv6Address.");
    amxd_trans_add_inst(&trans, 0, "new_static");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "2abc:1234:56:4200:b7ac:coff:ebad:7004");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "Static");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    expect_function_call(__wrap_update_ipv6_address);
    amxut_bus_handle_events();
}

void test_ipv6_address_wellknown_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv6Address.");
    amxd_trans_add_inst(&trans, 0, "new_wellknown");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "fe80::dead:beaf:7004:7004");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "WellKnown");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    expect_function_call(__wrap_update_ipv6_address);
    amxut_bus_handle_events();
}

void test_ipv6_address_autoconf_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv6Address.");
    amxd_trans_add_inst(&trans, 0, "new_autoconf");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "2abc:1234:56:4200:dead:beaf:abba:7004");
    amxd_trans_set_value(cstring_t, &trans, "Prefix", "Device.IP.Interface.3.IPv6Prefix.1.");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "AutoConfigured");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    expect_function_call(__wrap_start_listening_for_changes);
    expect_function_call(__wrap_ipv6_generate_and_set_in_dm);
    amxut_bus_handle_events();
}

void test_ipv6_address_dhcp_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv6Address.");
    amxd_trans_add_inst(&trans, 0, "new_dhcp");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "2abc:1234:56:4200:dead:beaf:abba:7004");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "DHCPv6");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    expect_function_call(__wrap_nm_open_dhcpoption_query);
    amxut_bus_handle_events();
}

void test_ipv6_address_static_toggle(UNUSED void** state) {
    trans_set_enable("IP.Interface.wan.IPv6Address.new_static.", false);
    expect_function_call(mock_delete_ipv6address);
    amxut_bus_handle_events();

    trans_set_enable("IP.Interface.wan.IPv6Address.new_static.", true);
    expect_function_call(__wrap_update_ipv6_address);
    amxut_bus_handle_events();
}

void test_ipv6_address_wellknown_toggle(UNUSED void** state) {
    trans_set_enable("IP.Interface.wan.IPv6Address.new_wellknown.", false);
    expect_function_call(mock_delete_ipv6address);
    amxut_bus_handle_events();

    trans_set_enable("IP.Interface.wan.IPv6Address.new_wellknown.", true);
    expect_function_call(__wrap_update_ipv6_address);
    amxut_bus_handle_events();
}

void test_ipv6_address_autoconf_toggle(UNUSED void** state) {
    trans_set_enable("IP.Interface.wan.IPv6Address.new_autoconf.", false);
    expect_function_call(mock_delete_ipv6address);
    expect_function_call(mock_delete_ipv6address);
    amxut_bus_handle_events();

    trans_set_enable("IP.Interface.wan.IPv6Address.new_autoconf.", true);
    expect_function_call(__wrap_start_listening_for_changes);
    expect_function_call(__wrap_ipv6_generate_and_set_in_dm);
    amxut_bus_handle_events();
}

void test_ipv6_address_dhcp_toggle(UNUSED void** state) {
    trans_set_enable("IP.Interface.wan.IPv6Address.new_dhcp.", false);
    expect_function_call(mock_delete_ipv6address);
    expect_function_call(mock_delete_ipv6address);
    amxut_bus_handle_events();

    trans_set_enable("IP.Interface.wan.IPv6Address.new_dhcp.", true);
    expect_function_call(__wrap_nm_open_dhcpoption_query);
    amxut_bus_handle_events();
}

void test_ipv6_prefix_child_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv6Prefix.");
    amxd_trans_add_inst(&trans, 0, "new_child");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ParentPrefix", "Device.IP.Interface.2.IPv6Prefix.1.");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "Child");
    amxd_trans_set_value(cstring_t, &trans, "StaticType", "Child");
    amxd_trans_set_value(cstring_t, &trans, "ChildPrefixBits", "0:0:0:3::/60");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    expect_function_call(__wrap_ipv6_prefix_child);
    amxut_bus_handle_events();
}

void test_ipv6_prefix_static_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv6Prefix.");
    amxd_trans_add_inst(&trans, 0, "new_static");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ParentPrefix", "Device.IP.Interface.2.IPv6Prefix.1.");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "Static");
    amxd_trans_set_value(cstring_t, &trans, "StaticType", "Child");
    amxd_trans_set_value(cstring_t, &trans, "ChildPrefixBits", "0:0:0:30::/60");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    expect_function_call(__wrap_ipv6_prefix_child);
    amxut_bus_handle_events();
}

void test_ipv6_prefix_static_pd_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv6Prefix.");
    amxd_trans_add_inst(&trans, 0, "new_static_pd");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ParentPrefix", "Device.IP.Interface.2.IPv6Prefix.1.");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "Static");
    amxd_trans_set_value(cstring_t, &trans, "StaticType", "PrefixDelegation");
    amxd_trans_set_value(cstring_t, &trans, "ChildPrefixBits", "0:0:0:30::/60");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();
}

void test_ipv6_prefix_iapd_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv6Prefix.");
    amxd_trans_add_inst(&trans, 0, "new_iapd");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Origin", "PrefixDelegation");
    amxd_trans_set_value(cstring_t, &trans, "StaticType", "PrefixDelegation");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    expect_function_call(__wrap_nm_open_dhcpoption_query);
    amxut_bus_handle_events();
}

void test_ipv6_prefix_ra_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv6Prefix.");
    amxd_trans_add_inst(&trans, 0, "new_ra");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Origin", "RouterAdvertisement");
    amxd_trans_set_value(cstring_t, &trans, "StaticType", "PrefixDelegation");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    expect_function_call(__wrap_nm_open_dhcpoption_query);
    amxut_bus_handle_events();
}

void test_ipv6_prefix_autoconf_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.wan.IPv6Prefix.");
    amxd_trans_add_inst(&trans, 0, "new_autoconf");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Origin", "AutoConfigured");
    amxd_trans_set_value(bool, &trans, "Autonomous", true);
    amxd_trans_set_value(cstring_t, &trans, "ChildPrefixBits", "0:0:0:3::/60");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    expect_function_call(__wrap_ipv6_prefix_autoconfigured);
    amxut_bus_handle_events();
}

void test_ipv6_prefix_child_toggle(UNUSED void** state) {
    trans_set_enable("IP.Interface.wan.IPv6Prefix.new_child.", false);
    expect_function_call(__wrap_dm_ipv6_prefix_set);
    amxut_bus_handle_events();

    trans_set_enable("IP.Interface.wan.IPv6Prefix.new_child.", true);
    expect_function_call(__wrap_ipv6_prefix_child);
    amxut_bus_handle_events();
}

void test_ipv6_prefix_static_toggle(UNUSED void** state) {
    trans_set_enable("IP.Interface.wan.IPv6Prefix.new_static.", false);
    expect_function_call(__wrap_dm_ipv6_prefix_set);
    amxut_bus_handle_events();

    trans_set_enable("IP.Interface.wan.IPv6Prefix.new_static.", true);
    expect_function_call(__wrap_ipv6_prefix_child);
    amxut_bus_handle_events();
}

void test_ipv6_prefix_static_pd_toggle(UNUSED void** state) {
    trans_set_enable("IP.Interface.wan.IPv6Prefix.new_static_pd.", false);
    expect_function_call(__wrap_dm_ipv6_prefix_set);
    amxut_bus_handle_events();

    trans_set_enable("IP.Interface.wan.IPv6Prefix.new_static_pd.", true);
    amxut_bus_handle_events();
}

void test_ipv6_prefix_iapd_toggle(UNUSED void** state) {
    trans_set_enable("IP.Interface.wan.IPv6Prefix.new_iapd.", false);
    expect_function_call(__wrap_dm_ipv6_prefix_set);
    amxut_bus_handle_events();

    trans_set_enable("IP.Interface.wan.IPv6Prefix.new_iapd.", true);
    expect_function_call(__wrap_nm_open_dhcpoption_query);
    amxut_bus_handle_events();
}

void test_ipv6_prefix_ra_toggle(UNUSED void** state) {
    trans_set_enable("IP.Interface.wan.IPv6Prefix.new_ra.", false);
    expect_function_call(__wrap_dm_ipv6_prefix_set);
    amxut_bus_handle_events();

    trans_set_enable("IP.Interface.wan.IPv6Prefix.new_ra.", true);
    expect_function_call(__wrap_nm_open_dhcpoption_query);
    amxut_bus_handle_events();
}

void test_ipv6_prefix_autoconf_toggle(UNUSED void** state) {
    trans_set_enable("IP.Interface.wan.IPv6Prefix.new_autoconf.", false);
    expect_function_call(__wrap_dm_ipv6_prefix_set);
    amxut_bus_handle_events();

    trans_set_enable("IP.Interface.wan.IPv6Prefix.new_autoconf.", true);
    expect_function_call(__wrap_ipv6_prefix_autoconfigured);
    amxut_bus_handle_events();
}
