/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "../common/mock.h"
#include "../../include_priv/mod_ipm_uci_utils.h"
#include "test_module.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

#define DUMMY_TEST_ODL "../common/dummy.odl"
#define UCI_ADD "uci_add"
#define UCI_DELETE "uci_delete"
#define UCI_SET "uci_set"
#define UCI_GET "uci_get"
#define UCI_COMMIT "uci_commit"

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

// read sig alarm is used to wait for a timer to run out so the callback
// function for this timer will be executed before continuing with the
// next test. In these tests it is used to wait on the delayed uci commit
static void read_sig_alarm(void) {
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    ssize_t s;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    assert_int_equal(s, sizeof(struct signalfd_siginfo));
    if(fdsi.ssi_signo == SIGALRM) {
        amxp_timers_calculate();
        amxp_timers_check();
        handle_events();
    }
}


int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    amxo_resolver_ftab_add(&parser, "get", AMXO_FUNC(dummy_function_get));
    amxo_resolver_ftab_add(&parser, "commit", AMXO_FUNC(dummy_function_commit));
    amxo_resolver_ftab_add(&parser, "set", AMXO_FUNC(dummy_function_set));
    amxo_resolver_ftab_add(&parser, "delete", AMXO_FUNC(dummy_function_del));
    amxo_resolver_ftab_add(&parser, "add", AMXO_FUNC(dummy_function_add));
    amxo_resolver_ftab_add(&parser, "changes", AMXO_FUNC(dummy_function_changes));

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, DUMMY_TEST_ODL, root_obj), 0);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    // Register data model
    amxb_register(bus_ctx, &dm);

    _dummy_main(0, &dm, &parser);

    handle_events();

    dummy_set_file(UCI_COMMIT, "../common/data/uci_commit.json", "uci_commit");
    // Interface used for the IPv4 tests
    dummy_set_file(UCI_GET, "../common/data/uci_get_network_interface_guest.json", "network_interface_guest");
    // Interface used for the set IfName test
    dummy_set_file(UCI_GET, "../common/data/uci_get_network_interface_wan.json", "network_interface_wan");
    // Used by the set IfName test to test interface of type bridge
    dummy_set_file(UCI_GET, "../common/data/uci_get_network_interface_guest_type.json", "network_interface_guest_type");
    // Interface used for the IPv6 tests
    dummy_set_file(UCI_GET, "../common/data/uci_get_network_interface_guest6.json", "network_interface_guest6");
    // Current UCI configuration used for IPv6 tests
    dummy_set_file(UCI_GET, "../common/data/get_option_ipaddr6_multiple_ip.json", "network_interface_guest6_ip6addr");

    return 0;
}

int test_teardown(UNUSED void** state) {
    amxm_close_all();

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();

    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, "target_module", "../modules_under_test/target_module.so"), 0);
}

void test_has_correct_functions(UNUSED void** state) {
    assert_true(amxm_has_function("target_module", "ipm-cfgctrlr", "update-ipv4address"));
    assert_true(amxm_has_function("target_module", "ipm-cfgctrlr", "delete-ipv4address"));
    assert_true(amxm_has_function("target_module", "ipm-cfgctrlr", "update-ipv6address"));
    assert_true(amxm_has_function("target_module", "ipm-cfgctrlr", "delete-ipv6address"));
    assert_true(amxm_has_function("target_module", "ipm-cfgctrlr", "set-ulaprefix"));
    assert_true(amxm_has_function("target_module", "ipm-cfgctrlr", "toggle-ipv6"));
    assert_true(amxm_has_function("target_module", "ipm-cfgctrlr", "set-mtu"));
    assert_true(amxm_has_function("target_module", "ipm-cfgctrlr", "set-ifname"));
    assert_true(amxm_has_function("target_module", "ipm-cfgctrlr", "set-neigh-config"));
}

void test_add_ipv4address(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;
    amxc_var_init(&ret);

    dummy_set_file(UCI_GET, "../common/data/get_option_ipaddr_empty.json", "network_interface_guest_ipaddr");
    dummy_set_file(UCI_SET, "../common/data/test_add_ipv4address_result_1.json", "network_interface_guest");

    data = dummy_read_json_from_file("test_data/test_add_ipv4.json");
    assert_non_null(data);

    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "update-ipv4address", data, &ret), 0);
    amxc_var_delete(&data);

    read_sig_alarm();
    amxc_var_clean(&ret);
}

void test_update_ipv4address(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;
    amxc_var_init(&ret);

    dummy_set_file(UCI_GET, "../common/data/get_option_ipaddr_single_ip.json", "network_interface_guest_ipaddr");
    dummy_set_file(UCI_SET, "../common/data/test_update_ipv4address_result_1.json", "network_interface_guest");

    data = dummy_read_json_from_file("test_data/test_change_ipv4.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "update-ipv4address", data, &ret), 0);
    amxc_var_delete(&data);

    dummy_set_file(UCI_GET, "../common/data/get_option_ipaddr_single_ip.json", "network_interface_guest_ipaddr");
    dummy_set_file(UCI_SET, "../common/data/test_update_ipv4address_result_2.json", "network_interface_guest");

    data = dummy_read_json_from_file("test_data/test_disable_ipv4.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "update-ipv4address", data, &ret), 0);
    amxc_var_delete(&data);

    data = dummy_read_json_from_file("test_data/test_invalid_ifname.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "update-ipv4address", data, &ret), -1);
    amxc_var_delete(&data);

    read_sig_alarm();
    amxc_var_clean(&ret);
}

void test_delete_ipv4address(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;
    amxc_var_init(&ret);

    // Delete an address
    dummy_set_file(UCI_GET, "../common/data/get_option_ipaddr_multiple_ip.json", "network_interface_guest_ipaddr");
    dummy_set_file(UCI_SET, "../common/data/test_delete_ipv4address_result_1.json", "network_interface_guest");

    data = dummy_read_json_from_file("test_data/test_delete_ipv4.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "delete-ipv4address", data, &ret), 0);
    amxc_var_delete(&data);

    // Check if a disabled address can also be deleted
    dummy_set_file(UCI_GET, "../common/data/get_option_ipaddr_multiple_ip.json", "network_interface_guest_ipaddr");
    dummy_set_file(UCI_SET, "../common/data/test_delete_ipv4address_result_2.json", "network_interface_guest");

    data = dummy_read_json_from_file("test_data/test_disable_ipv4.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "delete-ipv4address", data, &ret), 0);
    amxc_var_delete(&data);

    // Trying to delete an address from an invalid interface should fail
    data = dummy_read_json_from_file("test_data/test_invalid_ifname.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "delete-ipv4address", data, &ret), -1);
    amxc_var_delete(&data);

    read_sig_alarm();
    amxc_var_delete(&data);
    amxc_var_clean(&ret);
}

void test_add_ipv6address(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;
    amxc_var_init(&ret);

    dummy_set_file(UCI_SET, "../common/data/test_add_ipv6address_result_1.json", "network_interface_guest6");

    data = dummy_read_json_from_file("test_data/test_add_ipv6.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "update-ipv6address", data, &ret), 0);
    amxc_var_delete(&data);

    dummy_set_file(UCI_SET, "../common/data/test_add_ipv6address_result_2.json", "network_interface_guest6");

    data = dummy_read_json_from_file("test_data/test_add_existing_ipv6.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "update-ipv6address", data, &ret), 0);
    amxc_var_delete(&data);

    read_sig_alarm();
    amxc_var_clean(&ret);
}

void test_update_ipv6address(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;
    amxc_var_init(&ret);

    dummy_set_file(UCI_SET, "../common/data/test_update_ipv6address_result_1.json", "network_interface_guest6");

    data = dummy_read_json_from_file("test_data/test_replace_ipv6.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "update-ipv6address", data, &ret), 0);
    amxc_var_delete(&data);

    dummy_set_file(UCI_SET, "../common/data/test_update_ipv6address_result_2.json", "network_interface_guest6");

    data = dummy_read_json_from_file("test_data/test_disable_ipv6.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "update-ipv6address", data, &ret), 0);
    amxc_var_delete(&data);

    read_sig_alarm();
    amxc_var_clean(&ret);
}

void test_delete_ipv6address(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;
    amxc_var_init(&ret);

    dummy_set_file(UCI_SET, "../common/data/test_delete_ipv6address_result_1.json", "network_interface_guest6");

    data = dummy_read_json_from_file("test_data/test_delete_ipv6.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "delete-ipv6address", data, &ret), 0);
    amxc_var_delete(&data);

    data = dummy_read_json_from_file("test_data/test_delete_no_existing_ipv6.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "delete-ipv6address", data, &ret), 0);
    amxc_var_delete(&data);

    read_sig_alarm();
    amxc_var_clean(&ret);
}

void test_set_ulaprefix(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;
    amxc_var_init(&ret);

    dummy_set_file(UCI_SET, "../common/data/test_set_ulaprefix_result_1.json", "network_globals");

    data = dummy_read_json_from_file("test_data/test_set_ula_prefix.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "set-ulaprefix", data, &ret), 0);
    amxc_var_delete(&data);

    read_sig_alarm();
    amxc_var_delete(&data);
    amxc_var_clean(&ret);
}

void test_toggle_ipv6(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;
    amxc_var_init(&ret);

    dummy_set_file(UCI_SET, "../common/data/test_toggle_ipv6_result_1.json", "network_interface_guest6");

    data = dummy_read_json_from_file("test_data/test_toggle_ipv6.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "toggle-ipv6", data, &ret), 0);
    amxc_var_delete(&data);

    read_sig_alarm();
    amxc_var_delete(&data);
    amxc_var_clean(&ret);
}

void test_set_mtu(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;
    amxc_var_init(&ret);

    // Different section name for IPv4 and IPv6
    dummy_set_file(UCI_SET, "../common/data/test_set_mtu_v4_result_1.json", "network_interface_guest");
    dummy_set_file(UCI_SET, "../common/data/test_set_mtu_v6_result_1.json", "network_interface_guest6");

    data = dummy_read_json_from_file("test_data/test_set_mtu.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "set-mtu", data, &ret), 0);
    amxc_var_delete(&data);

    // Same section name for IPv4 and IPv6
    dummy_set_file(UCI_SET, "../common/data/test_set_mtu_result_2.json", "network_interface_guest");

    data = dummy_read_json_from_file("test_data/test_set_mtu_2.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "set-mtu", data, &ret), 0);
    amxc_var_delete(&data);

    read_sig_alarm();
    amxc_var_delete(&data);
    amxc_var_clean(&ret);
}

void test_set_ifname(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;
    amxc_var_init(&ret);

    dummy_set_file(UCI_SET, "../common/data/test_set_ifname_result_1.json", "network_interface_wan");
    dummy_set_file(UCI_SET, "../common/data/test_set_ifname_result_1.json", "network_interface_wan6");

    data = dummy_read_json_from_file("test_data/test_set_ifname.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "set-ifname", data, &ret), 0);
    read_sig_alarm();
    amxc_var_delete(&data);

    // Call set IfName on a bridge interface
    data = dummy_read_json_from_file("test_data/test_set_ifname_bridge.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "set-ifname", data, &ret), 0);
    amxc_var_delete(&data);

    // Sectionname not set for both IPv4 and IPv6
    data = dummy_read_json_from_file("test_data/test_set_ifname_missing_section.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "set-ifname", data, &ret), -1);
    amxc_var_clean(&ret);
    amxc_var_delete(&data);

    dummy_set_file(UCI_SET, "../common/data/THIS_FILE_SHOULD_NOT_BE_CALLED.json", "network_interface_wan");
    dummy_set_file(UCI_SET, "../common/data/THIS_FILE_SHOULD_NOT_BE_CALLED.json", "network_interface_wan6");

    // Test that the ifname does not get set for type==ppp
    data = dummy_read_json_from_file("test_data/test_set_ifname_ppp.json");
    assert_non_null(data);
    assert_int_equal(amxm_execute_function("target_module", "ipm-cfgctrlr", "set-ifname", data, &ret), 0);
    amxc_var_delete(&data);

}

void test_util_mask_to_prefix_length(UNUSED void** state) {
    assert_int_equal(mask_to_prefix_length("", true), 0);
    assert_int_equal(mask_to_prefix_length("abcd", true), 0);
    assert_int_equal(mask_to_prefix_length("", false), 0);
    assert_int_equal(mask_to_prefix_length("255.255.300.0", true), 0);
    assert_int_equal(mask_to_prefix_length("255.255.128.0", true), 17);
    assert_int_equal(mask_to_prefix_length("255.255.192.0", true), 18);
    assert_int_equal(mask_to_prefix_length("255.255.224.0", true), 19);
    assert_int_equal(mask_to_prefix_length("255.255.240.0", true), 20);
    assert_int_equal(mask_to_prefix_length("255.255.248.0", true), 21);
    assert_int_equal(mask_to_prefix_length("255.255.252.0", true), 22);
    assert_int_equal(mask_to_prefix_length("255.255.254.0", true), 23);
    assert_int_equal(mask_to_prefix_length("255.255.255.0", true), 24);
}