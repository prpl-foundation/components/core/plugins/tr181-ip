/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <amxc/amxc_variant.h>
#include <amxm/amxm.h>
#include <amxc/amxc_macros.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_ipm_uci_commit.h"
#include "mod_ipm_uci_utils.h"

#define ME "ip-mod"
#define OPTION_IPV4 "ipaddr"
#define OPTION_IPV6 "ip6addr"
#define MAX_INTF_NAME_LENGTH 64

static int mod_update_ipv4address(UNUSED const char* function_name,
                                  amxc_var_t* params,
                                  UNUSED amxc_var_t* ret) {
    int prefixlen = mask_to_prefix_length(GETP_CHAR(params, "parameters.SubnetMask"), true);
    int old_prefixlen = mask_to_prefix_length(GET_CHAR(params, "OldSubnetMask"), false);
    bool enable = GET_BOOL(params, "combined_enable");

    return uci_set_ip_address(params, (enable ? IP_ADD : IP_DELETE), prefixlen,
                              old_prefixlen, OPTION_IPV4);
}

static int mod_delete_ipv4address(UNUSED const char* function_name,
                                  amxc_var_t* params,
                                  UNUSED amxc_var_t* ret) {
    int rv = -1;
    int prefixlen = mask_to_prefix_length(GETP_CHAR(params, "parameters.SubnetMask"), true);

    rv = uci_set_ip_address(params, IP_DELETE, prefixlen, 0, OPTION_IPV4);

    return rv;
}

static int mod_update_ipv6address(UNUSED const char* function_name,
                                  amxc_var_t* params,
                                  UNUSED amxc_var_t* ret) {
    bool enable = GETP_BOOL(params, "parameters.Enable");
    int prefixlen = GET_INT32(params, "PrefixLength");
    int old_prefixlen = GET_INT32(params, "OldPrefixLength");

    return uci_set_ip_address(params, (enable ? IP_ADD : IP_DELETE), prefixlen,
                              old_prefixlen, OPTION_IPV6);
}

static int mod_delete_ipv6address(UNUSED const char* function_name,
                                  amxc_var_t* params,
                                  UNUSED amxc_var_t* ret) {
    int rv = -1;
    int prefixlen = GET_INT32(params, "PrefixLength");

    rv = uci_set_ip_address(params, IP_DELETE, prefixlen, 0, OPTION_IPV6);

    return rv;
}

static int mod_set_ula_prefix(UNUSED const char* function_name,
                              amxc_var_t* params,
                              UNUSED amxc_var_t* ret) {
    int rv = -1;
    amxc_var_t uci_args;
    amxc_var_t* values = NULL;
    amxc_var_t uci_ret;
    const char* ula_prefix = GETP_CHAR(params, "ULAPrefix.to");

    amxc_var_init(&uci_args);
    amxc_var_init(&uci_ret);

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "config", "network");
    amxc_var_add_key(cstring_t, &uci_args, "section", "globals");
    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_var_add_key(cstring_t, values, "ula_prefix", ula_prefix);

    rv = uci_call("set", &uci_args, &uci_ret, true);

    amxc_var_clean(&uci_args);
    amxc_var_clean(&uci_ret);
    return rv;
}

static int mod_toggle_ipv6(UNUSED const char* function_name,
                           amxc_var_t* params,
                           UNUSED amxc_var_t* ret) {
    int rv = -1;
    amxc_var_t uci_args;
    amxc_var_t* values = NULL;
    amxc_var_t uci_ret;
    const char* section_name = GET_CHAR(params, "UCIsectionName");
    const char* type = GET_CHAR(params, "Type");

    amxc_var_init(&uci_args);
    amxc_var_init(&uci_ret);

    if(type != NULL) {
        when_false_status(strcmp(type, "ppp") != 0, exit, rv = 0);
    }

    when_failed(init_uci_args(section_name, &uci_args), exit);
    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_var_add_key(bool, values, "ipv6", GET_BOOL(params, "combined_enable"));

    rv = uci_call("set", &uci_args, &uci_ret, true);

exit:
    amxc_var_clean(&uci_args);
    amxc_var_clean(&uci_ret);
    return rv;
}

static int mod_set_mtu(UNUSED const char* function_name,
                       amxc_var_t* params,
                       UNUSED amxc_var_t* ret) {
    int rv = -1;
    amxc_var_t uci_args;
    amxc_var_t* values = NULL;
    amxc_var_t uci_ret;
    const char* section_name4 = GET_CHAR(params, "UCIsectionName4");
    const char* section_name6 = GET_CHAR(params, "UCIsectionName6");

    amxc_var_init(&uci_args);
    amxc_var_init(&uci_ret);
    when_str_empty_trace(section_name4, exit, ERROR, "No UCIsectionName for IPv4 provided");
    when_str_empty_trace(section_name6, exit, ERROR, "No UCIsectionName for IPv6 provided");
    when_failed(init_uci_args(section_name4, &uci_args), exit);
    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_var_add_key(uint32_t, values, "mtu", GET_UINT32(params, "MaxMTUSize"));

    rv = uci_call("set", &uci_args, &uci_ret, true);
    when_failed_trace(rv, exit, ERROR, "Failed to set MTU on '%s'", section_name4);

    if(strncmp(section_name4, section_name6, MAX_INTF_NAME_LENGTH) != 0) {
        amxc_var_clean(&uci_args);
        when_failed(init_uci_args(section_name6, &uci_args), exit);
        values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
        amxc_var_add_key(uint32_t, values, "mtu", GET_UINT32(params, "MaxMTUSize"));
        rv = uci_call("set", &uci_args, &uci_ret, true);
        when_failed_trace(rv, exit, ERROR, "Failed to set MTU on '%s'", section_name6);
    }

exit:
    amxc_var_clean(&uci_args);
    amxc_var_clean(&uci_ret);
    return rv;
}

static int mod_set_ifname(UNUSED const char* function_name,
                          amxc_var_t* params,
                          UNUSED amxc_var_t* ret) {
    int rv = -1;
    amxc_var_t uci_args;
    amxc_var_t uci_ret;
    amxc_var_t* values = NULL;
    amxc_var_t* type = NULL;
    const char* section_name4 = GET_CHAR(params, "UCIsectionName4");
    const char* section_name6 = GET_CHAR(params, "UCIsectionName6");
    const char* ifname = GET_CHAR(params, "ifname");
    const char* ll_type = GET_CHAR(params, "Type");

    amxc_var_init(&uci_args);
    amxc_var_init(&uci_ret);
    when_str_empty_trace(section_name4, exit, ERROR, "No UCIsectionName for IPv4 provided");
    when_str_empty_trace(section_name6, exit, ERROR, "No UCIsectionName for IPv6 provided");
    when_str_empty_trace(ll_type, exit, ERROR, "No LowerLayers type provided");

    // Check if the given interface is a ppp interface
    // If so, do not write the interface name to uci
    // Uci requires the name of the interface on which the pppoe session
    // must be started. Writing the name of the ppp interface itself will
    // cause the ppp connection to be terminated.
    when_false_status(strcmp(ll_type, "ppp") != 0, exit, rv = 0);

    type = uci_get_network_section(section_name4, "type");
    if(GETP_CHAR(type, "0.value") == NULL) {
        // Set the ifname in the IPv4 section
        when_failed(init_uci_args(section_name4, &uci_args), exit);
        values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
        amxc_var_add_key(cstring_t, values, "ifname", ifname);
        rv = uci_call("set", &uci_args, &uci_ret, true);

        if(strncmp(section_name4, section_name6, MAX_INTF_NAME_LENGTH) != 0) {
            // Set the ifname in the IPv6 section
            amxc_var_clean(&uci_args);
            when_failed(init_uci_args(section_name6, &uci_args), exit);
            values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
            amxc_var_add_key(cstring_t, values, "ifname", ifname);
            rv = uci_call("set", &uci_args, &uci_ret, true);
        }
    } else {
        // If type is set, section is bridge currently do nothing
        rv = 0;
    }

exit:
    amxc_var_delete(&type);
    amxc_var_clean(&uci_args);
    amxc_var_clean(&uci_ret);
    return rv;
}

static int mod_set_neigh_config(UNUSED const char* function_name,
                                UNUSED amxc_var_t* params,
                                UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "This module does not set the neighbor config");
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static AMXM_CONSTRUCTOR ipm_uci_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    amxm_module_register(&mod, so, "ipm-cfgctrlr");
    amxm_module_add_function(mod, "update-ipv4address", mod_update_ipv4address);
    amxm_module_add_function(mod, "delete-ipv4address", mod_delete_ipv4address);
    amxm_module_add_function(mod, "update-ipv6address", mod_update_ipv6address);
    amxm_module_add_function(mod, "delete-ipv6address", mod_delete_ipv6address);
    amxm_module_add_function(mod, "set-ulaprefix", mod_set_ula_prefix);
    amxm_module_add_function(mod, "toggle-ipv6", mod_toggle_ipv6);
    amxm_module_add_function(mod, "set-mtu", mod_set_mtu);
    amxm_module_add_function(mod, "set-ifname", mod_set_ifname);
    amxm_module_add_function(mod, "set-neigh-config", mod_set_neigh_config);

    return uci_commit_init();
}

static AMXM_DESTRUCTOR ipm_uci_stop(void) {

    uci_commit_clean();
    return 0;
}
