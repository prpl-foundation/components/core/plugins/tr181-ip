/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__TR_IPV4_UTILS_H__)
#define __TR_IPV4_UTILS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <netmodel/common_api.h>

typedef enum _ipv4_addressing_type {
    IPV4_ADDR_TYPE_UNKNOWN,
    IPV4_ADDR_TYPE_DHCP,
    IPV4_ADDR_TYPE_IKEV2,
    IPV4_ADDR_TYPE_AUTOIP,
    IPV4_ADDR_TYPE_IPCP,
    IPV4_ADDR_TYPE_3GPP_NAS,
    IPV4_ADDR_TYPE_STATIC,
} ipv4_addressing_type_t;

typedef struct _ipv4_addr_info {
    amxd_object_t* addr_obj;                       // pointer to object it self
    amxd_object_t* intf_obj;                       // pointer to interface object this ipv4 is part of
    netmodel_query_t* nm_ip_query;                 // pointer to a netmodel query for the ip
    netmodel_query_t* nm_subnet_query;             // pointer to a netmodel query for the subnetmask
    ipv4_addressing_type_t addr_type;              // AddressingType enum for this address
    amxb_subscription_t* ipv4_param_subscr;        // Subscriptions for changes to the IP address parameters in NetDev
} ipv4_addr_info_t;

typedef int (* ipv4_addr_handler_t) (ipv4_addr_info_t* addr_info);

char* ipv4_addr_get_netmask_from_prefixlen(const int prefixlen);
amxd_status_t ipv4_addr_info_new(ipv4_addr_info_t** addr_info, amxd_object_t* addr_obj);
amxd_status_t ipv4_addr_info_clean(ipv4_addr_info_t** addr_info);
int ipv4_addr_info_subscription_clean(ipv4_addr_info_t* addr_info);
int nm_open_dhcpv4_query(ipv4_addr_info_t* addr_info);
void nm_close_ipv4_query(ipv4_addr_info_t* addr_info);
int nm_open_ipcpv4_query(ipv4_addr_info_t* addr_info);
int nm_open_3gppnasv4_query(ipv4_addr_info_t* addr_info);
ipv4_addressing_type_t convert_to_ipv4_addr_type(const char* s_addr_type);
int update_ipv4_address(ipv4_addr_info_t* addr_info);
int ipv4_addr_delete(ipv4_addr_info_t* addr_info);

#ifdef __cplusplus
}
#endif

#endif // __TR_IPV4_UTILS_H__
