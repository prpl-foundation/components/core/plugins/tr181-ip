/**
 * IP.Interface from TR-181 datamodel 2.12
 *
 * @version 1.0
 */
%define {
    select IP.Interface {
        /**
         * This table contains the interface's IPv6 unicast addresses.
         * There MUST be an entry for each such address, including anycast addresses.
         *
         * @version 1.0
         */
        %persistent object IPv6Address[] {
            on action destroy call ipv6_addr_destroy;
            /**
            * Configures the module that should be used to apply this IPAddress configuration.
            * Can only be one of the supported controllers configured in IP.SupportedControllers
            *
            * @version 1.0
            */
            %persistent %protected string Controller = "mod-ipm-netlink" {
                on action validate call check_is_empty_or_in "IP.SupportedControllers";
            }

            /**
             * The number of entries in the IPv6Address table.
             *
             * @version 1.0
             */
            counted with IPv6AddressNumberOfEntries;

            /**
             * Enables or disables this IPv6Address entry.
             *
             * @version 1.0
             */
            %persistent bool Enable = false {
                userflags %upc;
            }

            /**
             * The status of this IPv6Address table entry. Enumeration of:
             *
             * - Disabled
             * - Enabled
             * - Error_Misconfigured
             * - Error (OPTIONAL)
             *
             * @version 1.0
             */
            %read-only string Status {
                on action validate call check_enum ["Disabled", "Enabled", "Error_Misconfigured", "Error"];
                default "Disabled";
            }

            /**
             * The status of IPAddress, indicating whether it can be used
             * for communication. Enumeration of:
             *
             * - Preferred
             * - Deprecated
             * - Invalid
             * - Inaccessible
             * - Unknown
             * - Tentative
             * - Duplicate
             * - Optimistic
             *
             * @version 1.0
             */
            %read-only string IPAddressStatus {
                on action validate call check_enum ["Preferred",
                                                    "Deprecated",
                                                    "Invalid",
                                                    "Inaccessible",
                                                    "Unknown",
                                                    "Tentative",
                                                    "Duplicate",
                                                    "Optimistic"];
                default "Invalid";
            }

            /**
             * A non-volatile unique key used to reference this instance.
             *
             * @version 1.0
             */
            %persistent %write-once %unique %key string Alias {
                on action validate call check_maximum_length 64;
                userflags %upc;
            }

            /**
             * IPv6 address.
             *
             * @version 1.0
             */
            %persistent string IPAddress {
                on action validate call is_valid_ipv6;
                userflags %upc;
            }

            /**
             * Mechanism via which the IP address was assigned. Enumeration of:
             * - AutoConfigured
             * - DHCPv6
             * - IKEv2
             * - MAP
             * - WellKnown
             * - Static
             *
             * @version 1.0
             */
            %persistent %read-only string Origin {
                on action validate call check_enum ["AutoConfigured",
                                                    "DHCPv6",
                                                    "IKEv2",
                                                    "MAP",
                                                    "WellKnown",
                                                    "Static"];
                default "Static";
                userflags %upc;
            }

            /**
             * IPv6 address prefix.
             *
             * @version 1.0
             */
            %persistent string Prefix {
                userflags %upc;
                on action validate call matches_regexp "^Device\.IP\.Interface\.[0-9]+\.IPv6Prefix\.";
                on action write call set_object_ref_simple;
            }

            /**
             * The time at which this address will cease to be preferred (i.e. will become deprecated),
             * or 0001-01-01T00:00:00Z if not known.
             * For an infinite lifetime, the parameter value MUST be 9999-12-31T23:59:59Z.
             *
             * @version 1.0
             */
            %read-only %persistent datetime PreferredLifetime = "${infinite_lifetime}" {
                userflags %upc;
            }

            /**
             * The time at which this address will cease to be valid (i.e. will become invalid),
             * or 0001-01-01T00:00:00Z if not known.
             * For an infinite lifetime, the parameter value MUST be 9999-12-31T23:59:59Z.
             *
             * @version 1.0
             */
            %read-only %persistent datetime ValidLifetime = "${infinite_lifetime}" {
                userflags %upc;
            }

            /**
             * The relative valid lifetime in seconds
             *
             * @version 1.0
             */
            %read-only %persistent %protected uint32 RelativeValidLifetime = "${uint32_max}" {
                userflags %upc;
            }

            /**
             * The relative preferred lifetime in seconds
             *
             * @version 1.0
             */
            %read-only %persistent %protected uint32 RelativePreferredLifetime = "${uint32_max}" {
                userflags %upc;
            }

            /**
             * Indicates whether this is an anycast address.
             *
             * @version 1.0
             */
            %persistent bool Anycast = false;

            /**
             * When generating an IPAddress the Interface ID can either be:
             * * an autogenerated EUI64 value from the interfaces MAC Address
             * * a fixed value provided in the InterfaceId parameter
             *
             * @version 1.0
             */
            %persistent %protected string InterfaceIDType = "EUI64" {
                on action validate call check_enum
                    ["None", "EUI64", "Fixed"];
                    userflags %upc;
            }

            /**
             * When InterfaceIDType is set to "Fixed" the value in this parameter will be used as
             * the interface ID part of the IPAddress.
             * For the other types this parameter will be ignored.
             *
             * @version 1.0
             */
            %persistent %protected string InterfaceID = "::0:0:0:0/128" {
                on action validate call is_valid_ipv6_prefix;
                userflags %upc;
            }

            /**
             * Address scope
             * Some possible values:
             * global site link host nowhere | user defined from NetDev.ConversionTable.Scope
             *
             * @version 1.0
             */
            %read-only %protected string Scope;

            /**
             * Address flags.
             * Represented as a space separated list of keywords.
             * Some possible values include:
             * secondary nodad optimistic homeaddress deprecated tentative permanent dadfailed
             * @version 1.0
             */
            %read-only %protected string Flags;

            /**
             * Defines the type of address, each address type starts with an '@' sign.
             * The following types are supported for ipv6: @gua, @lla, @ula, @mc.
             * @version 1.0
             */
            %read-only %protected string TypeFlags;

            /**
             * Number of bits in the prefix
             * Expressed as an integer between 1 through 128.
             *
             * @version 1.0
             */
            %read-only %protected uint8 PrefixLen;

            /**
             * IPv6 Address of the peer, if relevant.
             *
             * @version 1.0
             */
            %read-only %protected string Peer;

            /**
             * Auto detected IP from another data model.
             *
             * @version 1.0
             */
             %read-only %protected bool AutoDetected = false;
        }
    }
}
