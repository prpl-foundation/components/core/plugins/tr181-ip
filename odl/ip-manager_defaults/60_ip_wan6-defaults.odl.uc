%populate {
{# find out if we have at least one wan interface #}{% if (BDfn.getUpstreamInterfaceIndex() != "-1"): %}
    object IP.Interface.wan6 {
        parameter Enable = false;
        parameter IPv4Enable = false;
        parameter IPv6Enable = true;
        parameter ULAEnable = false;
        parameter UCISectionNameIPv4 = "";
        parameter UCISectionNameIPv6 = "wan";
        parameter Type = "Normal";
        parameter Loopback = false;
        parameter UpLink = true;
    }

    object IP.Interface.wan6.IPv6Prefix {
        instance add ("GUA_IAPD") {
            parameter Enable = true;
            parameter Origin = "PrefixDelegation";
            parameter StaticType = "Inapplicable";
        }

        instance add ("GUA_RA") {
            parameter Enable = true;
            parameter Origin = "RouterAdvertisement";
            parameter StaticType = "Inapplicable";
        }

        instance add ("DHCP_IAPD") {
            parameter Enable = true;
            parameter Origin = "Child";
            parameter Prefix = "";
            parameter Autonomous = true;
            parameter ParentPrefix = "${ip_intf_wan6}IPv6Prefix.1";
            parameter StaticType = "Inapplicable";
            parameter ChildPrefixBits = "0:0:0:FFFF::/64";
        }

        instance add ("GUA_STATIC") {
            parameter Enable = true;
            parameter Origin = "Static";
            parameter StaticType = "Static";
            parameter Autonomous = true;
            parameter OnLink = true;
        }
    }

    object IP.Interface.wan6.IPv6Address {
        instance add ("GUA_RA") {
            parameter Enable = true;
            parameter Origin = "AutoConfigured";
            parameter Prefix = "${ip_intf_wan6}IPv6Prefix.2";
        }

        instance add("DHCP") {
            parameter Enable = false;
            parameter Origin = "DHCPv6";
            parameter InterfaceIDType = "None";
        }

        instance add("DHCP_IAPD") {
            parameter Enable = true;
            parameter IPAddress = "";
            parameter Prefix = "${ip_intf_wan6}IPv6Prefix.3";
            parameter Origin = "AutoConfigured";
        }

        instance add("GUA_STATIC") {
            parameter Enable = true;
            parameter IPAddress = "";
            parameter Prefix = "${ip_intf_wan6}IPv6Prefix.4";
            parameter Origin = "Static";
        }
    }
{% endif; %}
}
