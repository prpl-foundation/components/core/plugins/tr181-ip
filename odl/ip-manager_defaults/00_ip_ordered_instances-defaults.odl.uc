{% let has_upstream = (BDfn.getUpstreamInterfaceIndex() != "-1") %}
{% let has_failover = (BDfn.getFailoverInterfaceIndex() != "-1") %}
/*
This defaults.odl aims to initialize all instances of IP.Interface in a specific sequence.
This sequence can be modified upon request. However, it's crucial to ensure that the order
remains synchronized with odl/global_ip_interfaces.odl.
*/
%populate {
    object IP.Interface {
        instance add(0, "loopback");
{% if (has_upstream): %}
        instance add(0, "wan");
{% endif; %}
{% for (let Bridge in BD.Bridges) : %}
        instance add(0, "{{lc(Bridge)}}");
{% endfor; %}
{% if (has_upstream): %}
        instance add(0, "wan6");
        instance add(0, "DSLite-entry");
        instance add(0, "DSLite-exit");
        instance add(0, "voip");
        instance add(0, "iptv");
        instance add(0, "mgmt");
{% endif; %}
{% if (has_failover): %}
        instance add(0, "failover");
{% endif; %}
    }
}
