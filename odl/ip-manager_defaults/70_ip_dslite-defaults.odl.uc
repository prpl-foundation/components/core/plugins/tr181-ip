%populate {
{# find out if we have at least one wan interface #}{% if (BDfn.getUpstreamInterfaceIndex() != "-1"): %}
    object IP.Interface.DSLite-entry {
        parameter Enable = false;
        parameter IPv4Enable = true;
        parameter IPv6Enable = false;
        parameter ULAEnable = false;
        parameter UCISectionNameIPv4 = "";
        parameter UCISectionNameIPv6 = "";
        parameter Type = "Tunnel";
        parameter Loopback = false;
        parameter LowerLayers = "${ip_intf_dslite-exit}";
    }

    object IP.Interface.DSLite-entry.IPv4Address {
        instance add("primary") {
            parameter Enable = true;
            parameter IPAddress = "192.0.0.2";
            parameter SubnetMask = "255.255.255.0";
        }
    }

    object IP.Interface.DSLite-exit {
        parameter Enable = false;
        parameter IPv4Enable = false;
        parameter IPv6Enable = true;
        parameter ULAEnable = false;
        parameter UCISectionNameIPv4 = "";
        parameter UCISectionNameIPv6 = "";
        parameter Type = "Tunneled";
        parameter Loopback = false;
        parameter LowerLayers = "";
    }
{% endif; %}
}
