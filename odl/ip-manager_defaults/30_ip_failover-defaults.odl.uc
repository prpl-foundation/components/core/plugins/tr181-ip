{# Count non-default upstream interfaces until failover #}{% let failover_itf_index = 0; let DefaultItf = BD.Interfaces[BDfn.getUpstreamInterfaceIndex()]; for (let Itf in BD.Interfaces) : if (Itf.Upstream == "true" && Itf.Name != DefaultItf.Name) : failover_itf_index++; if (Itf.Failover == "true") : break; endif; endif; endfor %}
%populate {
{% if (BDfn.getFailoverInterfaceIndex() != "-1"): %}
    object IP.Interface.failover {
        parameter Enable = true;
        parameter IPv4Enable = true;
        parameter IPv6Enable = true;
        parameter ULAEnable = false;
        parameter UCISectionNameIPv4 = "";
        parameter UCISectionNameIPv6 = "";
        parameter MaxMTUSize = 1500;
        parameter Type = "Normal";
        parameter Loopback = false;
        parameter LowerLayers = "Device.Ethernet.Link.{{failover_itf_index + 2}}"; {# loopback + default wan + failover_itf_index #}
        parameter UpLink = true;
    }

    object IP.Interface.failover.IPv4Address {
        instance add("primary") {
            parameter Enable = true;
            parameter AddressingType = "DHCP";
        }
    }

    object IP.Interface.failover.IPv6Prefix {
        instance add ("GUA_IAPD") {
            parameter Enable = true;
            parameter Origin = "PrefixDelegation";
            parameter StaticType = "Inapplicable";
        }

        instance add ("GUA_RA") {
            parameter Enable = true;
            parameter Origin = "RouterAdvertisement";
            parameter StaticType = "Inapplicable";
        }

        instance add ("DHCP_IAPD") {
            parameter Enable = true;
            parameter Origin = "Child";
            parameter Prefix = "";
            parameter Autonomous = true;
            parameter ParentPrefix = "${ip_intf_failover}IPv6Prefix.1";
            parameter StaticType = "Inapplicable";
            parameter ChildPrefixBits = "0:0:0:FFFF::/64";
        }

        instance add ("GUA_STATIC") {
            parameter Enable = true;
            parameter Origin = "Static";
            parameter StaticType = "Static";
            parameter Autonomous = true;
            parameter OnLink = true;
        }
    }

    object IP.Interface.failover.IPv6Address {
        instance add ("GUA_RA") {
            parameter Enable = true;
            parameter Origin = "AutoConfigured";
            parameter Prefix = "${ip_intf_failover}IPv6Prefix.2";
        }

        instance add("DHCP") {
            parameter Enable = true;
            parameter Origin = "DHCPv6";
            parameter InterfaceIDType = "None";
        }

        instance add("DHCP_IAPD") {
            parameter Enable = true;
            parameter IPAddress = "";
            parameter Prefix = "${ip_intf_failover}IPv6Prefix.3";
            parameter Origin = "AutoConfigured";
        }

        instance add("GUA_STATIC") {
            parameter Enable = true;
            parameter IPAddress = "";
            parameter Prefix = "${ip_intf_failover}IPv6Prefix.4";
            parameter Origin = "Static";
        }
    }
{% endif; %}
} 