/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <amxc/amxc_lqueue.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_object.h>
#include <amxc/amxc_macros.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_interface.h"
#include "ip_manager_ip.h"
#include "ip_manager_common.h"
#include "ip_manager_controller.h"

#include "ipv4_addresses.h"

#define ME "ip-mngr"

int ipv4_toggle_persistency(amxd_object_t* ipaddr_obj, bool is_persistent) {
    SAH_TRACEZ_IN(ME);
    amxd_param_t* ip_param = amxd_object_get_param_def(ipaddr_obj, "IPAddress");
    amxd_param_t* subnetmask_param = amxd_object_get_param_def(ipaddr_obj, "SubnetMask");
    int rv = -1;

    when_null_trace(ip_param, exit, ERROR, "IPAddress could not be found");
    when_null_trace(subnetmask_param, exit, ERROR, "SubnetMask could not be found");
    rv = amxd_param_set_attr(ip_param, amxd_pattr_persistent, is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the persistent flag", is_persistent ? "add" : "remove");
    rv = amxd_param_set_attr(subnetmask_param, amxd_pattr_persistent, is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the persistent flag", is_persistent ? "add" : "remove");

    if(is_persistent) {
        amxd_param_set_flag(ip_param, "upc");
        amxd_param_set_flag(subnetmask_param, "upc");
    } else {
        amxd_param_unset_flag(ip_param, "upc");
        amxd_param_unset_flag(subnetmask_param, "upc");
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool ipv4_is_enabled(amxc_var_t* ctrlr_args) {
    SAH_TRACEZ_IN(ME);
    bool ipv4_enable = GETP_BOOL(ctrlr_args, "ip_parameters.IPv4Enable");
    bool intf_ipv4_enable = GETP_BOOL(ctrlr_args, "intf_parameters.IPv4Enable");
    bool intf_enable = GETP_BOOL(ctrlr_args, "intf_parameters.Enable");
    bool address_enable = GETP_BOOL(ctrlr_args, "parameters.Enable");
    bool ipv4_is_enabled = ipv4_enable && intf_ipv4_enable && intf_enable && address_enable;
    amxc_var_add_key(bool, ctrlr_args, "combined_enable", ipv4_is_enabled);
    SAH_TRACEZ_OUT(ME);
    return ipv4_is_enabled;
}

int ipv4address_build_ctrlr_args_from_object(amxd_object_t* ipv4address,
                                             amxc_var_t* ctrlr_args) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t* parameters = NULL;
    amxc_var_t* intf_params = NULL;
    amxc_var_t* ip_params = NULL;
    ip_intf_info_t* intf_info = NULL;
    amxd_object_t* intf_obj = amxd_object_findf(ipv4address, ".^.^.");
    amxd_object_t* ip_obj = amxd_object_findf(intf_obj, ".^.^.");

    amxc_var_set_type(ctrlr_args, AMXC_VAR_ID_HTABLE);

    when_null_trace(ipv4address, exit, ERROR, "No IPv4 object provided");
    when_null_trace(intf_obj, exit, ERROR, "Interface object could not be found");
    when_null_trace(ctrlr_args, exit, ERROR, "No variant stucture provided");
    intf_info = (ip_intf_info_t*) intf_obj->priv;
    when_null_trace(intf_info, exit, ERROR, "Interface has no private data");

    ip_params = amxc_var_add_key(amxc_htable_t, ctrlr_args, "ip_parameters", NULL);
    amxd_object_get_params(ip_obj, ip_params, amxd_dm_access_protected);

    intf_params = amxc_var_add_key(amxc_htable_t, ctrlr_args, "intf_parameters", NULL);
    amxd_object_get_params(intf_obj, intf_params, amxd_dm_access_protected);
    amxc_var_add_key(cstring_t, ctrlr_args, "UCIsectionName", GET_CHAR(intf_params, "UCISectionNameIPv4"));
    amxc_var_add_key(cstring_t, ctrlr_args, "ifname", intf_info->intf_name);
    amxc_var_add_key(uint32_t, ctrlr_args, "ipv4address-idx", amxd_object_get_index(ipv4address));
    parameters = amxc_var_add_key(amxc_htable_t, ctrlr_args, "parameters", NULL);
    amxd_object_get_params(ipv4address, parameters, amxd_dm_access_protected);

    ipv4_is_enabled(ctrlr_args);
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void _ipv4_address_added(UNUSED const char* const sig_name,
                         const amxc_var_t* const data,
                         UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_object_t* addr_tmpl_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    amxd_object_t* addr_obj = amxd_object_get_instance(addr_tmpl_obj, NULL, GET_UINT32(data, "index"));

    when_null_trace(addr_obj, exit, ERROR, "Could not find address address object with index %d", GET_UINT32(data, "index"));
    rv = init_ipv4(addr_obj);
    when_failed_trace(rv, exit, ERROR, "Failed to initialise new IPv4 address");

    ipv4_addr_apply((ipv4_addr_info_t*) addr_obj->priv);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

amxd_status_t _ipv4_addr_destroy(amxd_object_t* object,
                                 UNUSED amxd_param_t* param,
                                 amxd_action_t reason,
                                 UNUSED const amxc_var_t* const args,
                                 UNUSED amxc_var_t* const retval,
                                 UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    ipv4_addr_info_t* addr_info = NULL;
    amxd_status_t rv = amxd_status_unknown_error;
    amxc_var_t ctrlr_args;
    amxc_var_t ret;

    amxc_var_init(&ctrlr_args);
    amxc_var_init(&ret);

    if(reason != action_object_destroy) {
        SAH_TRACEZ_NOTICE(ME, "Wrong reason, expected action_object_destroy(%d) got %d", action_object_destroy, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(object, exit, ERROR, "Object can not be NULL, invalid argument");
    when_null_trace(object->priv, exit, INFO, "Object has no private data, nothing to destroy");
    addr_info = (ipv4_addr_info_t*) object->priv;

    if(ipv4_addr_delete(addr_info) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to delete IPv4 address");
    }

    rv = ipv4_addr_info_clean(&addr_info);

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&ctrlr_args);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void _ipv4_address_update(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_object_t* object = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    amxc_var_t ctrlr_args;
    amxc_var_t ret;
    amxc_var_t* parameters = NULL;
    ipv4_addr_info_t* addr_info = NULL;
    const char* addr_to = GETP_CHAR(data, "parameters.IPAddress.to");
    const char* addr_from = GETP_CHAR(data, "parameters.IPAddress.from");
    const char* subnetmask_to = GETP_CHAR(data, "parameters.SubnetMask.to");
    const char* subnetmask_from = GETP_CHAR(data, "parameters.SubnetMask.from");
    bool ipv4_delete = false;
    bool ipv4_subnetmask_mod = false;
    bool ipv4_ip_mod = false;
    amxc_var_init(&ctrlr_args);
    amxc_var_init(&ret);

    when_null_trace(object, exit, ERROR, "IPv4Address object could not be found");
    when_null_trace(object->priv, exit, ERROR, "IPv4Address object has no private data");
    addr_info = (ipv4_addr_info_t*) object->priv;

    rv = ipv4address_build_ctrlr_args_from_object(object, &ctrlr_args);
    when_failed_trace(rv, exit, ERROR, "Failed to build IPv4 arguments for '%s'", object->name);
    parameters = GET_ARG(&ctrlr_args, "parameters");
    if(((addr_to == NULL) || (*addr_to == '\0')) &&
       ((addr_from != NULL) && (*addr_from != '\0'))) {
        // If IPAddress was cleared
        amxc_var_set_key(parameters, "IPAddress",
                         GETP_ARG(data, "parameters.IPAddress.from"),
                         AMXC_VAR_FLAG_UPDATE | AMXC_VAR_FLAG_COPY);
        ipv4_delete = true;
    }
    if(((subnetmask_to == NULL) || (*subnetmask_to == '\0')) &&
       ((subnetmask_from != NULL) && (*subnetmask_from != '\0'))) {
        // If SubnetMask was cleared
        amxc_var_set_key(parameters, "SubnetMask",
                         GETP_ARG(data, "parameters.SubnetMask.from"),
                         AMXC_VAR_FLAG_UPDATE | AMXC_VAR_FLAG_COPY);
        ipv4_delete = true;
    }
    if(((addr_to == NULL)) && ((addr_from == NULL))) {
        // If only the subnetmask has been modified
        ipv4_subnetmask_mod = true;
    }
    if((subnetmask_to == NULL) && (subnetmask_from == NULL)) {
        // If only the IP address has been modified
        ipv4_ip_mod = true;
    }

    if(ipv4_delete) {
        rv = cfgctrlr_execute_function(object, "delete-ipv4address", &ctrlr_args, &ret);
        ipv4_addr_info_subscription_clean(addr_info);
        when_true_trace(rv == 1, exit, INFO, "Insufficient data provided to 'delete-ipv4address'");
        when_failed_trace(rv, exit, ERROR, "Failed to delete the IPv4 Address");
    } else {
        amxc_var_add_key(cstring_t, &ctrlr_args, "OldIPAddress", ipv4_subnetmask_mod ? GET_CHAR(parameters, "IPAddress") : addr_from);
        amxc_var_add_key(cstring_t, &ctrlr_args, "OldSubnetMask", ipv4_ip_mod ? GET_CHAR(parameters, "SubnetMask") : subnetmask_from);
        rv = cfgctrlr_execute_function(object, "update-ipv4address", &ctrlr_args, &ret);
        when_true_trace(rv == 1, exit, INFO, "Insufficient data provided to 'update-ipv4address'");
        when_failed_trace(rv, exit, ERROR, "Failed to update the IPv4 Address");
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&ctrlr_args);
    SAH_TRACEZ_OUT(ME);
}

void _ipv4_address_enable_changed(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipv4_addr_info_t* addr_info = NULL;
    amxd_object_t* addr_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);

    when_null_trace(addr_obj, exit, ERROR, "No IPv4 address object found");
    when_null_trace(addr_obj->priv, exit, ERROR, "No private data in this IPv4 address object found");
    addr_info = (ipv4_addr_info_t*) addr_obj->priv;

    if(GETP_BOOL(data, "parameters.Enable.to")) {
        rv = ipv4_addr_apply(addr_info);
        when_failed_trace(rv, exit, ERROR, "Failed to enable IPv4Address '%s'", addr_obj->name);
    } else {
        rv = ipv4_addr_disable(addr_info);
        when_failed_trace(rv, exit, ERROR, "Failed to disable IPv4Address '%s'", addr_obj->name);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ipv4_addressingtype_changed(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    bool is_persistent = false;
    amxd_object_t* addr_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    ipv4_addr_info_t* addr_info = NULL;

    when_null_trace(addr_obj, exit, ERROR, "No IPv4 address object found");
    when_null_trace(addr_obj->priv, exit, ERROR, "No private data in the IPv4 address object");
    addr_info = (ipv4_addr_info_t*) addr_obj->priv;

    // If the previous address was static, clear the address now
    if(addr_info->addr_type == IPV4_ADDR_TYPE_STATIC) {
        make_ip_address_empty(addr_obj, IP_VERSION_4);
    }

    // Make sure to update the addr_type in the address info struc before applying the address
    addr_info->addr_type = convert_to_ipv4_addr_type(GETP_CHAR(data, "parameters.AddressingType.to"));
    is_persistent = (addr_info->addr_type == IPV4_ADDR_TYPE_STATIC);
    ipv4_toggle_persistency(addr_obj, is_persistent);

    when_failed_trace(ipv4_addr_apply(addr_info), exit, ERROR, "Failed to apply change");

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

int init_ipv4(amxd_object_t* addr_obj) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipv4_addr_info_t* addr_info = NULL;
    bool is_persistent = false;

    when_null_trace(addr_obj, exit, WARNING, "No address object provided to initialise");
    when_not_null_trace(addr_obj->priv, exit, WARNING, "Address object already initialised");
    rv = ipv4_addr_info_new(&addr_info, addr_obj);
    when_failed_trace(rv, exit, ERROR, "Failed to create address info structure");

    is_persistent = (addr_info->addr_type == IPV4_ADDR_TYPE_STATIC);
    rv = ipv4_toggle_persistency(addr_obj, is_persistent);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ipv4_addr_apply(ipv4_addr_info_t* addr_info) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t ctrlr_args;

    amxc_var_init(&ctrlr_args);

    when_null_trace(addr_info, exit, ERROR, "Addr_info should not be empty");

    rv = ipv4address_build_ctrlr_args_from_object(addr_info->addr_obj, &ctrlr_args);
    when_failed_trace(rv, exit, ERROR, "Failed to build IPv4 arguments for '%s'", addr_info->addr_obj->name);
    if(GET_BOOL(&ctrlr_args, "combined_enable")) {
        nm_close_ipv4_query(addr_info);
        switch(addr_info->addr_type) {
        case IPV4_ADDR_TYPE_STATIC: {
            rv = update_ipv4_address(addr_info);
            when_failed_trace(rv, exit, ERROR, "Failed to apply static IPv4 Address");
            break;
        }
        case IPV4_ADDR_TYPE_DHCP:
            rv = nm_open_dhcpv4_query(addr_info);
            when_failed_trace(rv, exit, ERROR, "Failed to open DHCPv4 query");
            break;
        case IPV4_ADDR_TYPE_IPCP:
            rv = nm_open_ipcpv4_query(addr_info);
            when_failed_trace(rv, exit, ERROR, "Failed to open IPCP query");
            break;
        case IPV4_ADDR_TYPE_3GPP_NAS:
            rv = nm_open_3gppnasv4_query(addr_info);
            when_failed_trace(rv, exit, ERROR, "Failed to open 3GPP-NAS query");
            break;
        default:
            SAH_TRACEZ_ERROR(ME, "Unhandled IPv4 Address type");
            break;
        }
    }

exit:
    amxc_var_clean(&ctrlr_args);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ipv4_addr_disable(ipv4_addr_info_t* addr_info) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    when_null_trace(addr_info, exit, ERROR, "Addr_info should not be empty");

    rv = ipv4_addr_delete(addr_info);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to delete IPv4 address");
    }

    nm_close_ipv4_query(addr_info);
    switch(addr_info->addr_type) {
    case IPV4_ADDR_TYPE_STATIC:
        break;
    case IPV4_ADDR_TYPE_DHCP:
        make_ip_address_empty(addr_info->addr_obj, IP_VERSION_4);
        break;
    case IPV4_ADDR_TYPE_IPCP:
        make_ip_address_empty(addr_info->addr_obj, IP_VERSION_4);
        break;
    case IPV4_ADDR_TYPE_3GPP_NAS:
        make_ip_address_empty(addr_info->addr_obj, IP_VERSION_4);
        break;
    default:
        break;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}
