/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxc/amxc_macros.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_interface.h"
#include "ip_manager_ip.h"
#include "ip_manager_common.h"
#include "ip_manager_controller.h"
#include "ipv4_addresses.h"

#include "ipv4_utils.h"

#define DHCPv4_FLAG "dhcpv4"
#define PPP_FLAG "ppp"
#define NAS_3GPP_FLAG "cellular"
#define ME "ip-mngr"

static void nm_ipcpv4_address_cb(UNUSED const char* sig_name,
                                 const amxc_var_t* result,
                                 void* userdata) {
    SAH_TRACEZ_IN(ME);
    ipv4_addr_info_t* addr_info = NULL;
    amxd_trans_t* trans = NULL;
    const char* ip_addr = GET_CHAR(result, NULL);

    when_null_trace(userdata, exit, ERROR, "No userdata provided");
    addr_info = (ipv4_addr_info_t*) userdata;

    SAH_TRACEZ_INFO(ME, "IPCP address '%s' found", ip_addr);

    trans = ip_manager_trans_create(addr_info->addr_obj);
    amxd_trans_set_value(cstring_t, trans, "IPAddress", ip_addr);
    amxd_trans_set_value(cstring_t, trans, "SubnetMask", "255.255.255.255");

    if(netdev_name_known(addr_info->intf_obj) && !str_empty(ip_addr)) {
        int rv = subscribe_if_added_to_netdev(addr_info->intf_obj, addr_info->addr_obj, ip_addr, IP_VERSION_4);
        when_failed_trace(rv, exit, ERROR, "Failed to subscribe on parameter changes");
    } else {
        amxd_trans_set_value(cstring_t, trans, "Status", STATUS_ERROR);
    }

    ip_manager_trans_apply(trans);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void nm_ipaddress_cb(UNUSED const char* sig_name,
                            const amxc_var_t* result,
                            void* userdata) {
    SAH_TRACEZ_IN(ME);
    ipv4_addr_info_t* addr_info = NULL;
    amxd_trans_t* trans = NULL;
    const char* ip_addr = GET_CHAR(result, NULL);

    when_null_trace(userdata, exit, ERROR, "No userdata provided");
    addr_info = (ipv4_addr_info_t*) userdata;

    SAH_TRACEZ_INFO(ME, "IPAddress '%s' found", ip_addr);

    trans = ip_manager_trans_create(addr_info->addr_obj);
    amxd_trans_set_value(cstring_t, trans, "IPAddress", ip_addr);

    if(netdev_name_known(addr_info->intf_obj) && !str_empty(ip_addr)) {
        int rv = subscribe_if_added_to_netdev(addr_info->intf_obj, addr_info->addr_obj, ip_addr, IP_VERSION_4);
        when_failed_trace(rv, exit, ERROR, "Failed to subscribe on parameter changes");
    } else {
        amxd_trans_set_value(cstring_t, trans, "Status", STATUS_ERROR);
    }

    ip_manager_trans_apply(trans);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void nm_subnetmask_cb(UNUSED const char* sig_name,
                             const amxc_var_t* result,
                             void* userdata) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipv4_addr_info_t* addr_info = NULL;
    const char* subnetmask = GET_CHAR(result, NULL);
    amxd_trans_t* trans = NULL;
    const char* ip_addr = NULL;

    when_null_trace(userdata, exit, ERROR, "No userdata provided");
    addr_info = (ipv4_addr_info_t*) userdata;

    SAH_TRACEZ_INFO(ME, "Subnetmask '%s' found", subnetmask);

    trans = ip_manager_trans_create(addr_info->addr_obj);
    amxd_trans_set_value(cstring_t, trans, "SubnetMask", subnetmask);
    ip_addr = GET_CHAR(amxd_object_get_param_value(addr_info->addr_obj, "IPAddress"), NULL);
    if(netdev_name_known(addr_info->intf_obj) && !str_empty(ip_addr) && !str_empty(subnetmask)) {
        rv = subscribe_if_added_to_netdev(addr_info->intf_obj, addr_info->addr_obj, ip_addr, IP_VERSION_4);
        when_failed_trace(rv, exit, ERROR, "Failed to subscribe on parameter changes");
    } else {
        amxd_trans_set_value(cstring_t, trans, "Status", STATUS_ERROR);
    }

    ip_manager_trans_apply(trans);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static inline uint8_t get_octet(uint32_t val, uint32_t index) {
    int shift = (index - 1) * 8;
    int mask = 0xff << shift;
    return (val & mask) >> shift;
}

char* ipv4_addr_get_netmask_from_prefixlen(const int prefixlen) {
    SAH_TRACEZ_IN(ME);
    uint32_t netmask_hex = 0xffffffff;
    amxc_string_t netmask;
    char* ret = NULL;

    when_false((prefixlen > 0 && prefixlen < 33), exit);
    when_failed(amxc_string_init(&netmask, 0), exit);

    netmask_hex = 0xffffffff << (32 - prefixlen);

    amxc_string_setf(&netmask, "%d.%d.%d.%d",
                     get_octet(netmask_hex, 4),
                     get_octet(netmask_hex, 3),
                     get_octet(netmask_hex, 2),
                     get_octet(netmask_hex, 1));

    ret = amxc_string_take_buffer(&netmask);
    amxc_string_clean(&netmask);

exit:
    SAH_TRACEZ_OUT(ME);
    return ret;
}

amxd_status_t ipv4_addr_info_new(ipv4_addr_info_t** addr_info, amxd_object_t* addr_obj) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    const char* type = GET_CHAR(amxd_object_get_param_value(addr_obj, "AddressingType"), NULL);

    when_null_trace(addr_info, exit, ERROR, "Failed to create new IPv4 address info structure");
    when_null_trace(addr_obj, exit, ERROR, "No address object provided");

    *addr_info = (ipv4_addr_info_t*) calloc(1, sizeof(ipv4_addr_info_t));
    when_null_status(*addr_info, exit, rv = amxd_status_out_of_mem);
    (*addr_info)->addr_obj = addr_obj;
    (*addr_info)->intf_obj = amxd_object_get_parent(amxd_object_get_parent(addr_obj));
    (*addr_info)->nm_ip_query = NULL;
    (*addr_info)->nm_subnet_query = NULL;
    (*addr_info)->addr_type = convert_to_ipv4_addr_type(type);
    (*addr_info)->addr_obj->priv = *addr_info;

    rv = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t ipv4_addr_info_clean(ipv4_addr_info_t** addr_info) {
    SAH_TRACEZ_IN(ME);
    if((addr_info == NULL) || (*addr_info == NULL)) {
        goto exit;
    }

    nm_close_ipv4_query((*addr_info));
    ipv4_addr_info_subscription_clean(*addr_info);
    (*addr_info)->addr_obj->priv = NULL;
    (*addr_info)->addr_obj = NULL;
    free(*addr_info);
    *addr_info = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return amxd_status_ok;
}

int ipv4_addr_info_subscription_clean(ipv4_addr_info_t* addr_info) {
    int rv = -1;
    rv = amxb_subscription_delete(&addr_info->ipv4_param_subscr);
    addr_info->ipv4_param_subscr = NULL;

    return rv;
}

int nm_open_dhcpv4_query(ipv4_addr_info_t* addr_info) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    char* path = NULL;

    when_null_trace(addr_info, exit, ERROR, "No address info provided");

    path = amxd_object_get_path(addr_info->intf_obj, AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    addr_info->nm_ip_query = netmodel_openQuery_getFirstParameter(path, ME, "IPAddress",
                                                                  DHCPv4_FLAG,
                                                                  netmodel_traverse_down,
                                                                  nm_ipaddress_cb,
                                                                  addr_info);
    when_null_trace(addr_info->nm_ip_query, exit, ERROR, "Failed to start query for the DHCPv4 IP address");
    rv = nm_open_dhcpoption_query(addr_info->intf_obj, &addr_info->nm_subnet_query,
                                  "req", 1, nm_subnetmask_cb, addr_info);
    when_failed_trace(rv, exit, ERROR, "Failed to start query for the DHCPv4 subnetmask");

exit:
    free(path);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int nm_open_ipcpv4_query(ipv4_addr_info_t* addr_info) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    char* path = NULL;

    when_null_trace(addr_info, exit, ERROR, "No address info provided");

    path = amxd_object_get_path(addr_info->intf_obj, AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    addr_info->nm_ip_query = netmodel_openQuery_getFirstParameter(path, ME, "LocalIPAddress",
                                                                  PPP_FLAG,
                                                                  netmodel_traverse_down,
                                                                  nm_ipcpv4_address_cb,
                                                                  addr_info);
    when_null_trace(addr_info->nm_ip_query, exit, ERROR, "Failed to start query for the PPP IP address");
    rv = 0;

exit:
    free(path);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int nm_open_3gppnasv4_query(ipv4_addr_info_t* addr_info) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    char* path = NULL;

    when_null_trace(addr_info, exit, ERROR, "No address info provided");

    path = amxd_object_get_path(addr_info->intf_obj, AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    addr_info->nm_ip_query = netmodel_openQuery_getFirstParameter(path, ME, "IPAddress",
                                                                  NAS_3GPP_FLAG,
                                                                  netmodel_traverse_down,
                                                                  nm_ipaddress_cb,
                                                                  addr_info);
    when_null_trace(addr_info->nm_ip_query, exit, ERROR, "Failed to start query for the 3GPP-NAS IP address");

    addr_info->nm_subnet_query = netmodel_openQuery_getFirstParameter(path, ME, "SubnetMask",
                                                                      NAS_3GPP_FLAG,
                                                                      netmodel_traverse_down,
                                                                      nm_subnetmask_cb,
                                                                      addr_info);
    when_null_trace(addr_info->nm_subnet_query, exit, ERROR, "Failed to start query for the 3GPP-NAS SubnetMask");

    rv = 0;

exit:
    free(path);
    SAH_TRACEZ_OUT(ME);
    return rv;
}


void nm_close_ipv4_query(ipv4_addr_info_t* addr_info) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(addr_info, exit, ERROR, "No address info provided");
    nm_close_query(&addr_info->nm_ip_query);
    nm_close_query(&addr_info->nm_subnet_query);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

ipv4_addressing_type_t convert_to_ipv4_addr_type(const char* s_addr_type) {
    SAH_TRACEZ_IN(ME);
    ipv4_addressing_type_t addr_type = IPV4_ADDR_TYPE_UNKNOWN;

    when_str_empty_trace(s_addr_type, exit, ERROR, "Could not convert, empty string");

    if(strcmp(s_addr_type, "DHCP") == 0) {
        addr_type = IPV4_ADDR_TYPE_DHCP;
    } else if(strcmp(s_addr_type, "IKEv2") == 0) {
        addr_type = IPV4_ADDR_TYPE_IKEV2;
    } else if(strcmp(s_addr_type, "AutoIP") == 0) {
        addr_type = IPV4_ADDR_TYPE_AUTOIP;
    } else if(strcmp(s_addr_type, "IPCP") == 0) {
        addr_type = IPV4_ADDR_TYPE_IPCP;
    } else if(strcmp(s_addr_type, "3GPP-NAS") == 0) {
        addr_type = IPV4_ADDR_TYPE_3GPP_NAS;
    } else if(strcmp(s_addr_type, "Static") == 0) {
        addr_type = IPV4_ADDR_TYPE_STATIC;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return addr_type;
}

int update_ipv4_address(ipv4_addr_info_t* addr_info) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t ctrlr_args;
    amxc_var_t ctrlr_ret;
    amxd_object_t* addr_obj = NULL;

    amxc_var_init(&ctrlr_args);
    amxc_var_init(&ctrlr_ret);

    when_null_trace(addr_info, exit, ERROR, "No address structure provided");
    when_false_trace(netdev_name_known(addr_info->intf_obj), exit, INFO, "NetDev name not known, can't update IPv4 on interface '%s'", addr_info->intf_obj->name);
    addr_obj = addr_info->addr_obj;
    rv = ipv4address_build_ctrlr_args_from_object(addr_obj, &ctrlr_args);

    when_failed_trace(rv, exit, ERROR, "Failed to build IP address arguments for '%s'", amxd_object_get_name(addr_obj, AMXD_OBJECT_NAMED));

    if(GET_BOOL(&ctrlr_args, "combined_enable")) {
        const char* ip_addr = GETP_CHAR(&ctrlr_args, "parameters.IPAddress");
        when_str_empty_trace(ip_addr, exit, INFO, "No address to update with");
        rv = subscribe_if_added_to_netdev(addr_info->intf_obj, addr_obj, ip_addr, IP_VERSION_4);
        when_failed_trace(rv, exit, ERROR, "Failed to subscribe on parameter changes");
        rv = cfgctrlr_execute_function(addr_obj, "update-ipv4address", &ctrlr_args, &ctrlr_ret);
        when_true_trace(rv == 1, exit, INFO, "Insufficient data provided to 'update-ipv4address'");
        when_failed_trace(rv, exit, ERROR, "Error returned by 'update-ipv4address'");
    }

exit:
    amxc_var_clean(&ctrlr_args);
    amxc_var_clean(&ctrlr_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ipv4_addr_delete(ipv4_addr_info_t* addr_info) {

    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* ip_addr = NULL;
    amxc_var_t ctrlr_args;
    amxc_var_t ctrlr_ret;

    amxc_var_init(&ctrlr_args);
    amxc_var_init(&ctrlr_ret);

    when_null_trace(addr_info, exit, ERROR, "No address info provided");
    rv = ipv4address_build_ctrlr_args_from_object(addr_info->addr_obj, &ctrlr_args);

    ip_manager_set_status(addr_info->addr_obj, GET_BOOL(&ctrlr_args, "combined_enable") ? STATUS_ERROR : STATUS_DISABLED);
    ipv4_addr_info_subscription_clean(addr_info);

    ip_addr = GETP_CHAR(&ctrlr_args, "parameters.IPAddress");
    if(!str_empty(ip_addr)) {
        rv = cfgctrlr_execute_function(addr_info->addr_obj, "delete-ipv4address", &ctrlr_args, &ctrlr_ret);
    }

    when_true_trace(rv == 1, exit, INFO, "Insufficient data provided to 'delete-ipv4address'");
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to delete-ipv4address '%s'", addr_info->addr_obj->name);
    }
exit:
    amxc_var_clean(&ctrlr_args);
    amxc_var_clean(&ctrlr_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

