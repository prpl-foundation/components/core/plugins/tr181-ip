/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ipv6_utils.h"
#include "ip_manager_controller.h"
#include "ip_manager_netdev.h"
#include "ip_manager_common.h"
#include "ip_manager_interface.h"

#include "ip_manager_ip.h"

#define ME "ip-mngr"

static amxd_status_t ip_manager_ip_set_status(const char* const param, const char* val) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* dm_ip = NULL;
    amxd_dm_t* dm = ip_manager_get_dm();
    amxd_trans_t* trans = NULL;
    amxd_status_t rv = amxd_status_unknown_error;

    dm_ip = amxd_dm_findf(dm, "IP.");
    when_null_trace(dm_ip, exit, ERROR, "Unable to find IP. object");
    trans = ip_manager_trans_create(dm_ip);
    when_null_trace(trans, exit, ERROR, "Failed to create transaction");
    amxd_trans_set_value(cstring_t, trans, param, val);
    rv = ip_manager_trans_apply(trans);
    when_failed_trace(rv, exit, ERROR, "Failed to apply transaction");


exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Sets the status on a IP interface to NotPresent
 * Sets the status for the enabled IPv4/6 address to Error
 * Resets the LastChanged parameter
 * @param intf_obj pointer to the IP interface object
 * @return amxd_status_ok when the status is applied, otherwise an other error code and no changes in the data model are done
 */
amxd_status_t ip_manager_status_notpresent(amxd_object_t* intf_obj) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_trans_t* trans = NULL;
    amxd_object_t* ipv4_obj = NULL;
    amxd_object_t* ipv6_obj = NULL;
    ip_intf_info_t* intf_priv = NULL;

    when_null_trace(intf_obj, exit, ERROR, "No interface object provided");
    trans = ip_manager_trans_create(intf_obj);
    when_null_trace(trans, exit, ERROR, "Failed to create transaction");
    amxd_trans_set_value(cstring_t, trans, "Status", STATUS_NOTPRESENT);

    // Set IPv4 Address status to ERROR if interface is not present
    ipv4_obj = amxd_object_get_child(intf_obj, "IPv4Address");
    amxd_object_for_each(instance, it, ipv4_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        bool enabled = amxd_object_get_bool(instance, "Enable", NULL);
        amxd_trans_select_object(trans, instance);
        amxd_trans_set_value(cstring_t, trans, "Status", enabled ? STATUS_ERROR : STATUS_DISABLED);
    }

    // Set IPv6 Address status to ERROR if interface is not present
    ipv6_obj = amxd_object_get_child(intf_obj, "IPv6Address");
    amxd_object_for_each(instance, it, ipv6_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        bool enabled = amxd_object_get_bool(instance, "Enable", NULL);
        amxd_trans_select_object(trans, instance);
        amxd_trans_set_value(cstring_t, trans, "Status", enabled ? STATUS_ERROR : STATUS_DISABLED);
    }

    rv = ip_manager_trans_apply(trans);
    when_failed_trace(rv, exit, ERROR, "Failed to set Link '%s' to NotPresent", intf_obj->name);
    intf_priv = (ip_intf_info_t*) intf_obj->priv;
    when_null(intf_priv, exit);
    intf_priv->last_changed_timestamp = get_system_uptime();

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Sets the status on a IP instance
 * @param instance pointer to the IP instance object
 * @param status the new status to be set
 * @return amxd_status_ok when the status is applied, otherwise an other error code and no changes in the data model are done
 */
amxd_status_t ip_manager_set_status(const amxd_object_t* instance, const char* status) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_trans_t* trans = NULL;

    when_null_trace(instance, exit, ERROR, "No object provided to set status for");
    trans = ip_manager_trans_create(instance);
    when_null(trans, exit);

    amxd_trans_set_value(cstring_t, trans, "Status", status);
    rv = ip_manager_trans_apply(trans);
    when_failed_trace(rv, exit, ERROR, "Failed to set status '%s' on '%s'", status, instance->name);
    SAH_TRACEZ_INFO(ME, "Status '%s' set on '%s'", status, instance->name);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}


/**
 * @brief Sets the status on a IP interface and also resets the LastChanged parameter
 * @param intf_obj pointer to the IP interface object
 * @param status the new status to be set
 * @return amxd_status_ok when the status is applied, otherwise an other error code and no changes in the data model are done
 */
amxd_status_t ip_manager_intf_set_status(amxd_object_t* intf_obj, const char* status) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    ip_intf_info_t* intf_priv = NULL;

    when_null_trace(intf_obj, exit, ERROR, "No object provided to set status for");
    rv = ip_manager_set_status(intf_obj, status);
    when_failed_trace(rv, exit, ERROR, "Failed to set status on Link")

    intf_priv = (ip_intf_info_t*) intf_obj->priv;
    when_null(intf_priv, exit);
    intf_priv->last_changed_timestamp = get_system_uptime();

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void update_status(ip_manager_ip_status_t* query) {
    const char* status = STATUS_DISABLED;
    const char* status_param = (query->ip_version == IP_VERSION_4) ? IPV4_STATUS_PARAM : IPV6_STATUS_PARAM;

    if(query->ip_wan && query->ip_lan) {
        status = STATUS_ENABLED;
    }

    ip_manager_ip_set_status(status_param, status);

}


static void nm_query_ip_active_wan_cb(UNUSED const char* sig_name,
                                      const amxc_var_t* data,
                                      void* priv) {
    SAH_TRACEZ_IN(ME);
    ip_manager_ip_status_t* ip_query = (ip_manager_ip_status_t*) priv;
    ip_query->ip_wan = GET_BOOL(data, NULL);
    update_status(ip_query);
    SAH_TRACEZ_OUT(ME);

}

static void nm_query_ip_active_lan_cb(UNUSED const char* sig_name,
                                      const amxc_var_t* data,
                                      void* priv) {
    SAH_TRACEZ_IN(ME);
    ip_manager_ip_status_t* ip_query = (ip_manager_ip_status_t*) priv;
    ip_query->ip_lan = GET_BOOL(data, NULL);
    update_status(ip_query);
    SAH_TRACEZ_OUT(ME);

}

/**
 * @brief Checks if an IP address is already added to NetDev, if it exists it creates a parameter-changed subscription
 * @param intf_obj pointer to the IP interface object
 * @param addr_obj pointer to the IP address object
 * @param ip_addr the IP address in text format
 * @param ip_family the IP version of IP address the function need to check
 * @return 0 is returned if the address was not found in netdev or if it was found and it was able to subscribe successful, otherwise an error value is returned
 */
int subscribe_if_added_to_netdev(amxd_object_t* intf_obj,
                                 amxd_object_t* addr_obj,
                                 const char* ip_addr,
                                 uint8_t ip_family) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t netdev_link;
    amxc_string_t search_path;
    const char* intf_name = NULL;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("NetDev");

    amxc_var_init(&netdev_link);
    amxc_string_init(&search_path, 0);

    when_str_empty_trace(ip_addr, exit, ERROR, "No IP address provided to search for");

    intf_name = GET_CHAR(amxd_object_get_param_value(intf_obj, "Name"), NULL);
    when_str_empty_trace(intf_name, exit, ERROR, "No interface name provided");
    rv = amxc_string_setf(&search_path, "NetDev.Link.[%s].IPv%dAddr.[Address == '%s'].", intf_name, ip_family, ip_addr);
    when_failed_trace(rv, exit, ERROR, "Failed to set search path");

    amxb_get(ctx, amxc_string_get(&search_path, 0), 0, &netdev_link, 3);

    if(GETP_CHAR(&netdev_link, "0.0.Address") != NULL) {
        const char* path = amxc_var_key(GETP_ARG(&netdev_link, "0.0"));
        SAH_TRACEZ_INFO(ME, "Matching IPv%dAddress '%s' found in NetDev", ip_family, ip_addr);
        if(ip_family == IP_VERSION_4) {
            rv = netdev_ipv4_addr_param_change_subscribe(addr_obj, path);
        } else {
            rv = netdev_ipv6_addr_param_change_subscribe(addr_obj, path);
        }
    } else {
        SAH_TRACEZ_INFO(ME, "IPv%dAddress '%s' could not be found in NetDev", ip_family, ip_addr);
        rv = 0;
    }

exit:
    amxc_var_clean(&netdev_link);
    amxc_string_clean(&search_path);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int nm_open_dhcpoption_query(amxd_object_t* intf_obj,
                             netmodel_query_t** query,
                             const char* type,
                             int option,
                             netmodel_callback_t nm_cb,
                             void* userdata) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    char* intf = NULL;
    amxc_string_t intf_path;
    amxc_string_init(&intf_path, 0);

    when_null_trace(intf_obj, exit, ERROR, "No object provided");
    intf = amxd_object_get_path(intf_obj, AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);

    when_str_empty_trace(intf, exit, ERROR, "Could not open DHCP option query, no interface path");
    amxc_string_setf(&intf_path, "Device.%s", intf);
    if(*query != NULL) {
        SAH_TRACEZ_WARNING(ME, "A query is already open, closing this one first");
        nm_close_query(query);
    }
    *query = netmodel_openQuery_getDHCPOption(amxc_string_get(&intf_path, 0),
                                              ME, type, option, "this", nm_cb, userdata);
    when_null_trace(*query, exit, ERROR, "Failed to open DHCP option %d query on '%s'",
                    option, amxc_string_get(&intf_path, 0));

    rv = 0;

exit:
    amxc_string_clean(&intf_path);
    free(intf);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void nm_close_query(netmodel_query_t** query) {
    SAH_TRACEZ_IN(ME);
    if((query != NULL) && (*query != NULL)) {
        netmodel_closeQuery(*query);
        *query = NULL;
    }
    SAH_TRACEZ_OUT(ME);
}

amxd_status_t make_ip_address_empty(amxd_object_t* addr_obj, uint8_t ip_family) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_trans_t* trans = NULL;
    trans = ip_manager_trans_create(addr_obj);
    amxd_trans_set_value(cstring_t, trans, "IPAddress", "");
    amxd_trans_set_value(cstring_t, trans, "Flags", "");
    amxd_trans_set_value(cstring_t, trans, "TypeFlags", "");
    amxd_trans_set_value(cstring_t, trans, "Peer", "");
    amxd_trans_set_value(cstring_t, trans, "Scope", "");
    amxd_trans_set_value(uint8_t, trans, "PrefixLen", 0);
    switch(ip_family) {
    case IP_VERSION_4:
        amxd_trans_set_value(cstring_t, trans, "SubnetMask", "");
        break;
    case IP_VERSION_6:
        amxd_trans_set_value(cstring_t, trans, "ValidLifetime", INFINITE_LIFETIME);
        amxd_trans_set_value(cstring_t, trans, "PreferredLifetime", INFINITE_LIFETIME);
        amxd_trans_set_value(uint32_t, trans, "RelativeValidLifetime", UINT32_MAX);
        amxd_trans_set_value(uint32_t, trans, "RelativePreferredLifetime", UINT32_MAX);
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "Unkown IP version %d", ip_family);
        break;
    }
    rv = ip_manager_trans_apply(trans);

    SAH_TRACEZ_OUT(ME);
    return rv;
}

int update_ipv6_address(ipv6_addr_info_t* addr_info) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t ctrlr_args;
    amxc_var_t ctrlr_ret;
    amxd_object_t* addr_obj = NULL;

    amxc_var_init(&ctrlr_args);
    amxc_var_init(&ctrlr_ret);

    when_null_trace(addr_info, exit, ERROR, "No address structure provided");
    when_false_trace(netdev_name_known(addr_info->intf_obj), exit, INFO, "NetDev name not known, can't update IPv6 on interface '%s'", addr_info->intf_obj->name);
    addr_obj = addr_info->addr_obj;
    when_true_status(addr_info->auto_detected, exit, rv = 0);
    rv = ipv6address_build_ctrlr_args_from_object(addr_obj, &ctrlr_args);

    when_failed_trace(rv, exit, ERROR, "Failed to build IP address arguments for '%s'", amxd_object_get_name(addr_obj, AMXD_OBJECT_NAMED));

    if(GET_BOOL(&ctrlr_args, "combined_enable")) {
        const char* ip_addr = GETP_CHAR(&ctrlr_args, "parameters.IPAddress");
        when_str_empty_trace(ip_addr, exit, INFO, "No address to update with");
        rv = subscribe_if_added_to_netdev(addr_info->intf_obj, addr_obj, ip_addr, IP_VERSION_6);
        when_failed_trace(rv, exit, ERROR, "Failed to subscribe on parameter changes");
        rv = cfgctrlr_execute_function(addr_obj, "update-ipv6address", &ctrlr_args, &ctrlr_ret);

        when_true_trace(rv == 1, exit, INFO, "Insufficient data provided to 'update-ipv6address'");
        when_failed_trace(rv, exit, ERROR, "Error returned by 'update-ipv6address'");
    }

exit:
    amxc_var_clean(&ctrlr_args);
    amxc_var_clean(&ctrlr_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

ip_manager_ip_status_t* ip_manager_populate_ip_status(const char* flag, uint8_t IP_version) {
    SAH_TRACEZ_IN(ME);
    const char* intf = NULL;

    ip_manager_ip_status_t* ip_query = (ip_manager_ip_status_t*) calloc(1, sizeof(ip_manager_ip_status_t));
    when_null_trace(ip_query, exit, ERROR, "Error on calloc");
    ip_query->ip_lan = false;
    ip_query->ip_wan = false;
    ip_query->ip_version = IP_version;
    intf = ip_manager_get_setting("ip_intf_lan");
    if(!str_empty(intf)) {
        ip_query->lan_query = netmodel_openQuery_isUp(intf,
                                                      ME,
                                                      flag,
                                                      netmodel_traverse_this,
                                                      nm_query_ip_active_lan_cb,
                                                      ip_query);
    }
    intf = ip_manager_get_setting("ip_intf_wan");
    if(!str_empty(intf)) {
        ip_query->wan_query = netmodel_openQuery_isUp(intf,
                                                      ME,
                                                      flag,
                                                      netmodel_traverse_this,
                                                      nm_query_ip_active_wan_cb,
                                                      ip_query);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return ip_query;
}

int intf_name_close_query(ip_intf_info_t* intf, bool free_name) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_object_t* intf_obj = NULL;

    when_null_trace(intf, exit, ERROR, "No address IP interface structure provided");
    intf_obj = intf->obj;

    nm_close_query(&intf->name_query);
    rv = ip_interface_set_name(intf_obj, "");
    when_failed_trace(rv, exit, ERROR, "Could not clear the Name of the IP Interface");

    if(free_name) {
        free(intf->intf_name);
        intf->intf_name = NULL;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _ipv6_going_down(amxd_object_t* object,
                               amxd_param_t* param,
                               amxd_action_t reason,
                               const amxc_var_t* const args,
                               amxc_var_t* const retval,
                               void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t data;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    when_true_status(reason != action_param_write, exit, status = amxd_status_function_not_implemented);

    amxc_var_add_key(cstring_t, &data, "Interface", "");

    if(!GET_BOOL(args, NULL)) {
        amxd_object_trigger_signal(object, "ip:ipv6_going_down", &data);
    }

    status = amxd_action_object_write(object, param, reason, args, retval, priv);
exit:
    amxc_var_clean(&data);
    return status;
}
