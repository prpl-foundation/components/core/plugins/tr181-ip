/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_ip.h"
#include "ip_manager_common.h"
#include "ip_manager_interface.h"

#include "ip_manager_interface_ipaddress.h"

#define ME "ip-mngr"

/**********************************************************
* Macro definitions
**********************************************************/

#define IP_INTERFACE_IPV4ADDRESS_OBJECT "IPv4Address"
#define IP_INTERFACE_ENABLE_PARAM "Enable"
#define IP_INTERFACE_IPADDRESS_PARAM "IPAddress"
#define IP_INTERFACE_SUBNETMASK_PARAM "SubnetMask"
#define IP_INTERFACE_ADDRESSINGTYPE_PARAM "AddressingType"
#define IP_INTERFACE_ALIAS_PARAM "Alias"
#define IP_INTERFACE_ALLOW_CREATE_PARAM "AllowCreate"
#define IP_INTERFACE_DELETE_INSTANCE_PARAM "deleteInstance"

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

static amxd_status_t set_ipaddress_instance(const amxd_object_t* object, bool enabled, const cstring_t ipAddress, const cstring_t subnetMask, const cstring_t addressingType, const cstring_t alias);
static amxd_status_t delete_ipaddress_instance(amxd_object_t* object, const cstring_t alias);
static amxd_status_t add_ipaddress_instance(const amxd_object_t* object, bool enabled, const cstring_t ipAddress, const cstring_t subnetMask, const cstring_t addressingType, const cstring_t alias);

static amxd_status_t find_ipaddress_by_ip_and_sub(amxd_object_t* object, const cstring_t ip_to_check, const cstring_t subnet_to_check, amxc_var_t* const ret);
static amxd_status_t find_ipaddress_by_alias(amxd_object_t* object, const cstring_t alias_to_check, amxc_var_t* const ret);
static amxd_status_t find_ipaddress_by_addressing_type(amxd_object_t* object, const cstring_t addressingType, amxc_var_t* const ret);
static amxd_status_t find_ipaddress(amxd_object_t* object, amxc_string_t* const expression, amxc_var_t* const ret);

static amxd_status_t delete_using_alias(amxd_object_t* object, const cstring_t alias, bool delete_instance);
static amxd_status_t handle_deleteIPv4Address(amxd_object_t* object, amxc_var_t* ret, const cstring_t alias, const cstring_t ipAddress, const cstring_t subnetMask, const cstring_t addressingType, bool delete_instance);
static amxd_status_t handle_setIPv4Address(amxd_object_t* object, amxc_var_t* ret, const cstring_t alias, const cstring_t ipAddress, const cstring_t subnetMask, const cstring_t addressingType, bool enable, bool allow_create);

/**********************************************************
* Functions
**********************************************************/

static amxd_status_t set_ipaddress_instance(const amxd_object_t* object,
                                            bool enabled,
                                            const cstring_t ipAddress,
                                            const cstring_t subnetMask,
                                            const cstring_t addressingType,
                                            const cstring_t alias) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* object_IPv4Address = NULL;
    amxd_object_t* instance = NULL;
    amxd_trans_t trans;

    object_IPv4Address = amxd_object_get_child(object, IP_INTERFACE_IPV4ADDRESS_OBJECT);
    when_null_status(object_IPv4Address, exit, status = amxd_status_object_not_found);

    instance = amxd_object_get_instance(object_IPv4Address, alias, 0);
    when_null_status(instance, exit, status = amxd_status_object_not_found);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, instance);

    amxd_trans_set_value(cstring_t, &trans, IP_INTERFACE_IPADDRESS_PARAM, ipAddress);
    amxd_trans_set_value(bool, &trans, IP_INTERFACE_ENABLE_PARAM, enabled);
    amxd_trans_set_value(cstring_t, &trans, IP_INTERFACE_SUBNETMASK_PARAM, subnetMask);
    if(addressingType != NULL) {
        amxd_trans_set_value(cstring_t, &trans, IP_INTERFACE_ADDRESSINGTYPE_PARAM, addressingType);
    }

    status = amxd_trans_apply(&trans, ip_manager_get_dm());
    amxd_trans_clean(&trans);
    when_failed_trace(status, exit, ERROR, "Failed to update the parameters of an ipaddress instance with alias: '%s'", alias);
    SAH_TRACEZ_INFO(ME, "Successfully updated the ipaddress with alias: '%s'", alias);
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t delete_ipaddress_instance(amxd_object_t* object, const cstring_t alias) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* object_IPv4Address = NULL;
    amxd_trans_t trans;

    object_IPv4Address = amxd_object_get_child(object, IP_INTERFACE_IPV4ADDRESS_OBJECT);
    when_null_status(object_IPv4Address, exit, status = amxd_status_object_not_found);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, object_IPv4Address);
    amxd_trans_del_inst(&trans, 0, alias);

    status = amxd_trans_apply(&trans, ip_manager_get_dm());
    amxd_trans_clean(&trans);
    when_failed_trace(status, exit, ERROR, "Failed to remove an ipaddress instance");
    SAH_TRACEZ_INFO(ME, "Successfully deleted the ipaddress");
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t add_ipaddress_instance(const amxd_object_t* object,
                                            bool enabled,
                                            const cstring_t ipAddress,
                                            const cstring_t subnetMask,
                                            const cstring_t addressingType,
                                            const cstring_t alias) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* object_IPv4Address = NULL;
    amxd_trans_t trans;

    object_IPv4Address = amxd_object_get_child(object, IP_INTERFACE_IPV4ADDRESS_OBJECT);
    when_null_status(object_IPv4Address, exit, status = amxd_status_object_not_found);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, object_IPv4Address);
    amxd_trans_add_inst(&trans, 0, alias);

    amxd_trans_set_value(bool, &trans, IP_INTERFACE_ENABLE_PARAM, enabled);
    amxd_trans_set_value(cstring_t, &trans, IP_INTERFACE_IPADDRESS_PARAM, ipAddress);
    amxd_trans_set_value(cstring_t, &trans, IP_INTERFACE_SUBNETMASK_PARAM, subnetMask);
    amxd_trans_set_value(cstring_t, &trans, IP_INTERFACE_ADDRESSINGTYPE_PARAM, addressingType);

    status = amxd_trans_apply(&trans, ip_manager_get_dm());

    amxd_trans_clean(&trans);
    when_failed_trace(status, exit, ERROR, "Failed to add an ipaddress instance");
    SAH_TRACEZ_INFO(ME, "Successfully added an ipaddress");
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t find_ipaddress_by_ip_and_sub(amxd_object_t* object,
                                                  const cstring_t ip_to_check,
                                                  const cstring_t subnet_to_check,
                                                  amxc_var_t* const ret) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t expression;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_string_init(&expression, 0);

    //add the ipaddress and subnet to the expression
    status = (amxd_status_t) amxc_string_appendf(&expression, ".[%s == '%s' && %s == '%s'].", IP_INTERFACE_IPADDRESS_PARAM, ip_to_check, IP_INTERFACE_SUBNETMASK_PARAM, subnet_to_check);
    when_failed(status, exit);
    status = find_ipaddress(object, &expression, ret);
exit:
    amxc_string_clean(&expression);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t find_ipaddress_by_alias(amxd_object_t* object,
                                             const cstring_t alias_to_check,
                                             amxc_var_t* const ret) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t expression;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_string_init(&expression, 0);

    //add the alias to the expression
    status = (amxd_status_t) amxc_string_appendf(&expression, ".[%s == '%s'].", IP_INTERFACE_ALIAS_PARAM, alias_to_check);
    when_failed(status, exit);
    status = find_ipaddress(object, &expression, ret);
exit:
    amxc_string_clean(&expression);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t find_ipaddress_by_addressing_type(amxd_object_t* object,
                                                       const cstring_t addressingType_to_check,
                                                       amxc_var_t* const ret) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t expression;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_string_init(&expression, 0);

    //add the addressingtype to the expression
    status = (amxd_status_t) amxc_string_appendf(&expression, ".[%s == '%s'].", IP_INTERFACE_ADDRESSINGTYPE_PARAM, addressingType_to_check);
    when_failed(status, exit);
    status = find_ipaddress(object, &expression, ret);
exit:
    amxc_string_clean(&expression);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t find_ipaddress(amxd_object_t* object,
                                    amxc_string_t* const expression,
                                    amxc_var_t* const ret) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* object_IPv4Address = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* inst = NULL;
    amxc_llist_t possible_ipaddresses;
    const cstring_t first_instance_alias = NULL;

    amxc_llist_init(&possible_ipaddresses);

    //get the ipv4address object
    object_IPv4Address = amxd_object_get_child(object, IP_INTERFACE_IPV4ADDRESS_OBJECT);
    when_null_status(object_IPv4Address, exit, status = amxd_status_object_not_found);

    //get the ipv4address instance
    status = amxd_object_resolve_pathf(object_IPv4Address, &possible_ipaddresses, "%s", amxc_string_get(expression, 0));
    when_failed_trace(status, exit, WARNING, "amxd_object_resolve_pathf failed, using the expression: '%s'", amxc_string_get(expression, 0));
    if(amxc_llist_size(&possible_ipaddresses) > 0) {
        // get the alias+path of the first instance
        first_instance_alias = amxc_string_get(amxc_string_from_llist_it(amxc_llist_get_first(&possible_ipaddresses)), 0);

        // get the instance
        inst = amxd_dm_findf(ip_manager_get_dm(), "%s", first_instance_alias);
        if(inst != NULL) {
            status = amxd_object_get_params(inst, ret, amxd_dm_access_public);
            when_failed(status, exit);
        } else {
            status = amxd_status_object_not_found;
            when_null_trace(inst, exit, ERROR, "amxd_dm_findf returns NULL, but this should always be a valid object");
        }
    }

exit:
    amxc_llist_clean(&possible_ipaddresses, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t handle_setIPv4Address(amxd_object_t* object,
                                           amxc_var_t* ret,
                                           const cstring_t alias,
                                           const cstring_t ipAddress,
                                           const cstring_t subnetMask,
                                           const cstring_t addressingType,
                                           bool enable, bool allow_create) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_parameter_not_found;

    if((alias == NULL) || (alias[0] == '\0')) {
        //find the object using the addressingtype
        status = find_ipaddress_by_addressing_type(object, addressingType, ret);
        when_failed_trace(status, exit, ERROR, "Failed to find ipaddress with type '%s' on '%s', return %d", addressingType, object->name, status);

        if(amxc_var_is_null(ret)) {// no ipaddress with the same addressingType was found -> create it
            if(allow_create) {
                status = add_ipaddress_instance(object, enable, ipAddress, subnetMask, addressingType, alias);
                when_failed_trace(status, exit, ERROR, "Failed to add new IP Address: '%s', '%s' return %d", ipAddress, subnetMask, status);

                status = find_ipaddress_by_addressing_type(object, addressingType, ret);
                when_failed_trace(status, exit, ERROR, "Failed to find ipaddress with type '%s' on '%s', return %d", addressingType, object->name, status);
            }
        } else {
            // ipaddress with the same addressingType was found -> fill in the corresponding alias, to update it
            alias = GET_CHAR(ret, IP_INTERFACE_ALIAS_PARAM); // get the alias from the found ipaddress instance

            status = set_ipaddress_instance(object, enable, ipAddress, subnetMask, addressingType, alias);
            when_failed_trace(status, exit, ERROR, "Failed to set new parameters on IP Address '%s', return %d", alias, status);

            status = find_ipaddress_by_alias(object, alias, ret);
            when_failed_trace(status, exit, ERROR, "Failed to find IP Address '%s', return %d", alias, status);
        }
    } else {
        status = find_ipaddress_by_alias(object, alias, ret);
        when_failed_trace(status, exit, ERROR, "Failed to find IP Address '%s', return %d", alias, status);

        if(amxc_var_is_null(ret)) {// the given alias does not exist -> create it
            if(allow_create) {
                status = add_ipaddress_instance(object, enable, ipAddress, subnetMask, addressingType, alias);
                when_failed_trace(status, exit, ERROR, "Failed to add IP Address '%s', '%s', '%s', return %d", alias, ipAddress, subnetMask, status);

                status = find_ipaddress_by_alias(object, alias, ret);
                when_failed_trace(status, exit, ERROR, "Failed to find IP Address '%s', return %d", alias, status);
            }
        } else { // the given alias does exist -> update the parameters
            status = set_ipaddress_instance(object, enable, ipAddress, subnetMask, addressingType, alias);
            when_failed_trace(status, exit, ERROR, "Failed to set new parameters on existing IP Address '%s', return %d", alias, status);

            status = find_ipaddress_by_alias(object, alias, ret);
            when_failed_trace(status, exit, ERROR, "Failed to find IP Address '%s', return %d", alias, status);
        }
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t handle_deleteIPv4Address(amxd_object_t* object,
                                              amxc_var_t* ret,
                                              const cstring_t alias,
                                              const cstring_t ipAddress,
                                              const cstring_t subnetMask,
                                              const cstring_t addressingType,
                                              bool delete_instance) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    bool ipAddress_is_given = (ipAddress != NULL) && (ipAddress[0] != '\0');
    bool subnetMask_is_given = (subnetMask != NULL) && (subnetMask[0] != '\0');
    bool addressingType_is_given = (addressingType != NULL) && (addressingType[0] != '\0');

    //Search for an alias with the given parameters if no alias is given
    if((alias == NULL) || (alias[0] == '\0')) {
        //check if an ipaddress with subnet is given OR addressingType
        if(ipAddress_is_given && subnetMask_is_given) {
            // get alias name using ipaddress and subnet
            status = find_ipaddress_by_ip_and_sub(object, ipAddress, subnetMask, ret);
        } else if(addressingType_is_given) {
            // get alias name using the addressingtype
            status = find_ipaddress_by_addressing_type(object, addressingType, ret);
        } else {
            //no alias can be found
            status = amxd_status_parameter_not_found;
            when_failed_trace(status, exit, WARNING, "No param is given that can be used to delete/clear something");
        }
        when_failed(status, exit);
        alias = GET_CHAR(ret, IP_INTERFACE_ALIAS_PARAM); // get the alias from the found ipaddress instance, and use it to delete it
    } else {
        // alias is given, only fill in the return value
        status = find_ipaddress_by_alias(object, alias, ret);
        when_failed(status, exit);
    }

    status = delete_using_alias(object, alias, delete_instance);
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t delete_using_alias(amxd_object_t* object,
                                        const cstring_t alias,
                                        bool delete_instance) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;

    when_str_empty_trace(alias, exit, WARNING, "No ipaddress instance could be found with the given parameters");

    //delete/clear the ipaddress with a given alias
    if(delete_instance) {
        status = delete_ipaddress_instance(object, alias);
    } else {
        status = set_ipaddress_instance(object, false, "", "", NULL, alias);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _setIPv4Address(amxd_object_t* object,
                              UNUSED amxd_function_t* func,
                              amxc_var_t* args,
                              amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_parameter_not_found;
    bool enable_param = false;
    bool allow_create_param = true;
    const cstring_t ipAddress_param = NULL;
    const cstring_t subnetMask_param = NULL;
    const cstring_t addressingType_param = NULL;
    const cstring_t alias_param = NULL;

    when_null_status(ret, exit, status = amxd_status_parameter_not_found);
    when_null_status(object, exit, status = amxd_status_parameter_not_found);
    when_null_status(args, exit, status = amxd_status_parameter_not_found);

    allow_create_param = GET_BOOL(args, IP_INTERFACE_ALLOW_CREATE_PARAM);
    enable_param = GET_BOOL(args, IP_INTERFACE_ENABLE_PARAM);
    alias_param = GET_CHAR(args, IP_INTERFACE_ALIAS_PARAM);
    subnetMask_param = GET_CHAR(args, IP_INTERFACE_SUBNETMASK_PARAM);
    addressingType_param = GET_CHAR(args, IP_INTERFACE_ADDRESSINGTYPE_PARAM);
    ipAddress_param = GET_CHAR(args, IP_INTERFACE_IPADDRESS_PARAM);

    when_str_empty_trace(ipAddress_param, exit, ERROR, "Param '%s' is missing", IP_INTERFACE_IPADDRESS_PARAM);
    when_str_empty_trace(subnetMask_param, exit, ERROR, "Param '%s' is missing", IP_INTERFACE_SUBNETMASK_PARAM);
    when_str_empty_trace(addressingType_param, exit, ERROR, "Param '%s' is missing", IP_INTERFACE_ADDRESSINGTYPE_PARAM);

    status = handle_setIPv4Address(object, ret, alias_param, ipAddress_param, subnetMask_param, addressingType_param, enable_param, allow_create_param);
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _deleteIPv4Address(amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 UNUSED amxc_var_t* args,
                                 amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    bool delete_instance_param = true;
    const cstring_t ipAddress_param = NULL;
    const cstring_t subnetMask_param = NULL;
    const cstring_t addressingType_param = NULL;
    const cstring_t alias_param = NULL;

    when_null_status(object, exit, status = amxd_status_parameter_not_found);
    when_null_status(args, exit, status = amxd_status_parameter_not_found);
    when_null_status(ret, exit, status = amxd_status_parameter_not_found);

    delete_instance_param = GET_BOOL(args, IP_INTERFACE_DELETE_INSTANCE_PARAM);
    alias_param = GET_CHAR(args, IP_INTERFACE_ALIAS_PARAM);
    subnetMask_param = GET_CHAR(args, IP_INTERFACE_SUBNETMASK_PARAM);
    addressingType_param = GET_CHAR(args, IP_INTERFACE_ADDRESSINGTYPE_PARAM);
    ipAddress_param = GET_CHAR(args, IP_INTERFACE_IPADDRESS_PARAM);

    status = handle_deleteIPv4Address(object, ret, alias_param, ipAddress_param, subnetMask_param, addressingType_param, delete_instance_param);
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}
