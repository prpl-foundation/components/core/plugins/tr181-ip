/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ipv4_utils.h"
#include "ip_manager_interface.h"
#include "ipv4_addresses.h"
#include "ipv6_prefixes.h"
#include "ipv6_addresses.h"
#include "ip_manager_common.h"

#include "ip_manager_entrypoint.h"

#define ME "ip-mngr"

int _ip_manager_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    int retval = 0;
    switch(reason) {
    case AMXO_START:
        retval = ip_manager_init(dm, parser);
        SAH_TRACEZ_INFO(ME, "IP manager started");
        break;
    case AMXO_STOP:
        ip_manager_shutdown();
        SAH_TRACEZ_INFO(ME, "IP manager stopped");
        break;
    }

    return retval;
}

static int init_interface(UNUSED amxd_object_t* templ,
                          amxd_object_t* intf_obj,
                          UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ip_intf_info_t* intf = NULL;
    amxd_object_t* ipv4_tmpl_obj = amxd_object_get_child(intf_obj, "IPv4Address");
    amxd_object_t* prefix_tmpl_obj = amxd_object_get_child(intf_obj, "IPv6Prefix");
    amxd_object_t* ipv6_tmpl_obj = amxd_object_get_child(intf_obj, "IPv6Address");

    when_null_trace(intf_obj, exit, ERROR, "No interface provided to NULL check");

    rv = ip_intf_info_new(&intf, intf_obj);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to initialise interface '%s'", intf_obj->name);
    }

    amxd_object_for_each(instance, it, ipv4_tmpl_obj) {
        amxd_object_t* addr_obj = amxc_container_of(it, amxd_object_t, it);
        rv = init_ipv4(addr_obj);
        if(rv != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to initialise IPv4 address on interface '%s'", intf_obj->name);
        }
    }

    amxd_object_for_each(instance, it, prefix_tmpl_obj) {
        amxd_object_t* prefix_obj = amxc_container_of(it, amxd_object_t, it);
        rv = init_prefix(prefix_obj);
        if(rv != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to initialise IPv6 prefix on interface '%s'", intf_obj->name);
        }
    }

    amxd_object_for_each(instance, it, ipv6_tmpl_obj) {
        amxd_object_t* addr_obj = amxc_container_of(it, amxd_object_t, it);
        rv = init_ipv6(addr_obj);
        if(rv != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to initialise IPv6 Address on interface '%s'", intf_obj->name);
        }
    }

    rv = nm_open_netdevname_query(intf_obj);
    when_failed_trace(rv, exit, ERROR, "Failed to open NetDevName query on '%s'", intf_obj->name);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void _app_start(UNUSED const char* const sig_name,
                UNUSED const amxc_var_t* const data,
                UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* ip_obj = amxd_dm_get_object(ip_manager_get_dm(), "IP");
    SAH_TRACEZ_INFO(ME, "Initialising IP datamodel");
    amxd_object_for_all(ip_obj, ".Interface.*", init_interface, NULL);

    SAH_TRACEZ_OUT(ME);
}