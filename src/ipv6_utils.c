/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <ipat/ipat.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_transaction.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_interface.h"
#include "ip_manager_ip.h"
#include "ip_manager_common.h"
#include "ip_manager_controller.h"
#include "ipv6_addresses.h"
#include "ipv6_prefixes.h"

#include "ipv6_utils.h"

#define ME "ipv6"

static void prefix_preferred_timer_expired(UNUSED amxp_timer_t* timer, void* userdata) {
    SAH_TRACEZ_IN(ME);
    ipv6_prefix_info_t* prefix_info = (ipv6_prefix_info_t*) userdata;
    amxd_object_t* prefix_obj = NULL;
    when_null_trace(prefix_info, exit, ERROR, "No prefix info provided");
    when_null_trace(prefix_info->prefix_obj, exit, ERROR, "No prefix object provided");
    prefix_obj = prefix_info->prefix_obj;

    SAH_TRACEZ_INFO(ME, "Prefix '%s' no longer preferred", prefix_obj->name);

    if(prefix_info->valid_timer->state == amxp_timer_running) {
        set_status(PREFIX_STATUS_PARAM, prefix_obj, PREFIX_STATUS_DEPRECATED);
    } else {
        set_status(PREFIX_STATUS_PARAM, prefix_obj, PREFIX_STATUS_INVALID);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void prefix_valid_timer_expired(UNUSED amxp_timer_t* timer, void* userdata) {
    SAH_TRACEZ_IN(ME);
    ipv6_prefix_info_t* prefix_info = (ipv6_prefix_info_t*) userdata;
    amxd_object_t* prefix_obj = NULL;
    when_null_trace(prefix_info, exit, ERROR, "No prefix info provided");
    when_null_trace(prefix_info->prefix_obj, exit, ERROR, "No prefix object provided");
    prefix_obj = prefix_info->prefix_obj;

    SAH_TRACEZ_INFO(ME, "Prefix '%s' no longer valid", prefix_obj->name);
    set_status(PREFIX_STATUS_PARAM, prefix_obj, PREFIX_STATUS_INVALID);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void addr_preferred_timer_expired(UNUSED amxp_timer_t* timer, void* userdata) {
    SAH_TRACEZ_IN(ME);
    ipv6_addr_info_t* addr_info = (ipv6_addr_info_t*) userdata;
    amxd_object_t* addr_obj = NULL;
    when_null_trace(addr_info, exit, ERROR, "No addr info provided");
    when_null_trace(addr_info->addr_obj, exit, ERROR, "No prefix object provided");
    addr_obj = addr_info->addr_obj;

    SAH_TRACEZ_INFO(ME, "Address '%s' no longer preferred", addr_obj->name);

    if(addr_info->valid_timer->state == amxp_timer_running) {
        set_status(ADDR_STATUS_PARAM, addr_obj, ADDR_STATUS_DEPRECATED);
    } else {
        set_status(ADDR_STATUS_PARAM, addr_obj, ADDR_STATUS_INVALID);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void addr_valid_timer_expired(UNUSED amxp_timer_t* timer, void* userdata) {
    SAH_TRACEZ_IN(ME);
    ipv6_addr_info_t* addr_info = (ipv6_addr_info_t*) userdata;
    amxd_object_t* addr_obj = NULL;
    when_null_trace(addr_info, exit, ERROR, "No addr info provided");
    when_null_trace(addr_info->addr_obj, exit, ERROR, "No prefix object provided");
    addr_obj = addr_info->addr_obj;

    SAH_TRACEZ_INFO(ME, "Address '%s' no longer valid", addr_obj->name);
    set_status(ADDR_STATUS_PARAM, addr_obj, ADDR_STATUS_INVALID);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static int ipv6_convert_to_bin(const char* ip, int lower, int upper, unsigned int* buf) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    char* ip_lower_masked = NULL;
    char* ip_upper_masked = NULL;

    when_str_empty_trace(ip, exit, ERROR, "No IP provided");
    when_false_trace((lower >= 0 && lower <= 128), exit, ERROR, "'%d' is an invalid lower prefix", lower);
    when_false_trace((upper >= 0 && upper <= 128), exit, ERROR, "'%d' is an invalid upper prefix", upper);

    ip_lower_masked = ipat_text_mask_apply_direct(ip, lower, ipat_bits_lower, ipat_oper_and, false);
    ip_upper_masked = ipat_text_mask_apply_direct(ip_lower_masked, upper, ipat_bits_upper, ipat_oper_and, false);
    rv = inet_pton(AF_INET6, ip_upper_masked, buf);
    when_false_trace(rv > 0, exit, ERROR, "Failed to convert '%s' to binary, return %d", ip_upper_masked, rv);
    rv = 0;

exit:
    free(ip_lower_masked);
    free(ip_upper_masked);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool ipv6_is_enabled(amxc_var_t* ctrlr_args) {
    SAH_TRACEZ_IN(ME);
    bool ipv6_enable = GETP_BOOL(ctrlr_args, "ip_parameters.IPv6Enable");
    bool intf_ipv6_enable = GETP_BOOL(ctrlr_args, "intf_parameters.IPv6Enable");
    bool intf_enable = GETP_BOOL(ctrlr_args, "intf_parameters.Enable");
    bool address_enable = GETP_BOOL(ctrlr_args, "parameters.Enable");
    bool ipv6_is_enabled = ipv6_enable && intf_ipv6_enable && intf_enable && address_enable;
    amxc_var_add_key(bool, ctrlr_args, "combined_enable", ipv6_is_enabled);
    SAH_TRACEZ_OUT(ME);
    return ipv6_is_enabled;
}

/**
 * @brief Sets the PrefixStatus on a prefix instance
 * @param param_name the name of the status parameter that should be set
 * @param obj pointer to the object for which the status should be set
 * @param status the new status to be set
 * @return amxd_status_ok when the status is applied, otherwise an other error code and no changes in the data model are done
 */
amxd_status_t set_status(const char* param_name, amxd_object_t* obj, const char* status) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_trans_t* trans = NULL;

    when_str_empty(param_name, exit);
    when_null_trace(obj, exit, ERROR, "No prefix object provided for which to set the status");
    trans = ip_manager_trans_create(obj);
    when_null_trace(trans, exit, ERROR, "Failed to create transaction");

    amxd_trans_set_value(cstring_t, trans, param_name, status);
    rv = ip_manager_trans_apply(trans);
    when_failed_trace(rv, exit, ERROR, "Failed to set status '%s' on '%s'", status, obj->name);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t ipv6_prefix_info_new(ipv6_prefix_info_t** prefix_info,
                                   amxd_object_t* prefix_obj) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    const char* origin = GET_CHAR(amxd_object_get_param_value(prefix_obj, "Origin"), NULL);
    const char* type = GET_CHAR(amxd_object_get_param_value(prefix_obj, "StaticType"), NULL);

    when_null_trace(prefix_info, exit, ERROR, "Prefix can not be null, invalid argument");
    when_null_trace(prefix_obj, exit, ERROR, "No prefix object provided");

    *prefix_info = (ipv6_prefix_info_t*) calloc(1, sizeof(ipv6_prefix_info_t));
    when_null_status(*prefix_info, exit, rv = amxd_status_out_of_mem);
    (*prefix_info)->prefix_obj = prefix_obj;
    (*prefix_info)->nm_query = NULL;
    (*prefix_info)->intf_obj = amxd_object_get_parent(amxd_object_get_parent(prefix_obj));
    (*prefix_info)->prefix_origin = prefix_origin_to_enum(origin);
    (*prefix_info)->prefix_type = prefix_static_type_to_enum(type);
    (*prefix_info)->prefix_obj->priv = *prefix_info;
    amxp_timer_new(&(*prefix_info)->preferred_timer, prefix_preferred_timer_expired, (*prefix_info));
    amxp_timer_new(&(*prefix_info)->valid_timer, prefix_valid_timer_expired, (*prefix_info));

    rv = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t ipv6_addr_info_new(ipv6_addr_info_t** addr_info,
                                 amxd_object_t* addr_obj,
                                 bool auto_detected) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_function_argument;
    const char* origin = GET_CHAR(amxd_object_get_param_value(addr_obj, "Origin"), NULL);
    const char* intf_id = GET_CHAR(amxd_object_get_param_value(addr_obj, "InterfaceIDType"), NULL);

    when_null_trace(addr_info, exit, ERROR, "Failed to create new IPv6 address info structure");
    when_null_trace(addr_obj, exit, ERROR, "No address object provided");

    *addr_info = (ipv6_addr_info_t*) calloc(1, sizeof(ipv6_addr_info_t));
    when_null_status(*addr_info, exit, rv = amxd_status_out_of_mem);
    (*addr_info)->nm_query = NULL;
    (*addr_info)->addr_obj = addr_obj;
    (*addr_info)->intf_obj = amxd_object_get_parent(amxd_object_get_parent(addr_obj));
    (*addr_info)->intf_id_type = convert_to_intf_id(intf_id);
    (*addr_info)->origin = addr_origin_to_enum(origin);
    (*addr_info)->auto_detected = auto_detected;
    (*addr_info)->addr_obj->priv = *addr_info;
    amxp_timer_new(&(*addr_info)->preferred_timer, addr_preferred_timer_expired, (*addr_info));
    amxp_timer_new(&(*addr_info)->valid_timer, addr_valid_timer_expired, (*addr_info));

    rv = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t ipv6_prefix_info_clean(ipv6_prefix_info_t** prefix_info) {
    SAH_TRACEZ_IN(ME);
    if((prefix_info == NULL) || (*prefix_info == NULL)) {
        goto exit;
    }

    amxp_timer_delete(&(*prefix_info)->preferred_timer);
    amxp_timer_delete(&(*prefix_info)->valid_timer);
    nm_close_query(&(*prefix_info)->nm_query);
    (*prefix_info)->prefix_obj->priv = NULL;
    (*prefix_info)->prefix_obj = NULL;
    free(*prefix_info);
    *prefix_info = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return amxd_status_ok;
}

amxd_status_t ipv6_addr_info_clean(ipv6_addr_info_t** addr_info) {
    SAH_TRACEZ_IN(ME);
    if((addr_info == NULL) || (*addr_info == NULL)) {
        goto exit;
    }

    amxp_timer_delete(&(*addr_info)->preferred_timer);
    amxp_timer_delete(&(*addr_info)->valid_timer);
    ipv6_addr_info_subscription_clean(*addr_info);
    nm_close_query(&(*addr_info)->nm_query);
    (*addr_info)->addr_obj->priv = NULL;
    (*addr_info)->addr_obj = NULL;
    free(*addr_info);
    *addr_info = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return amxd_status_ok;
}

int ipv6_addr_info_subscription_clean(ipv6_addr_info_t* addr_info) {
    int rv = -1;
    rv = amxb_subscription_delete(&addr_info->ipv6_param_subscr);
    addr_info->ipv6_param_subscr = NULL;

    return rv;
}

static int check_duplicate_prefix(ipv6_prefix_info_t* prefix_info,
                                  amxc_string_t* combined, bool primary_prefix) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_llist_t entries;
    const char* parent_prefix = NULL;

    amxc_llist_init(&entries);

    when_null_status(prefix_info, exit, rv = 0); // Not a prefix, no need to check (can happen when generating addresses)
    parent_prefix = GET_CHAR(amxd_object_get_param_value(prefix_info->prefix_obj, "ParentPrefix"), NULL);

    amxd_object_resolve_pathf(prefix_info->intf_obj, &entries,
                              "^.*.IPv6Prefix.[Prefix == '%s'].",
                              amxc_string_get(combined, 0));

    rv = amxc_llist_size(&entries);

    amxc_llist_for_each(it, (&entries)) {
        const char* path = amxc_string_get(amxc_string_from_llist_it(it), 0);
        amxd_object_t* obj = amxd_dm_findf(ip_manager_get_dm(), "%s", path);
        ipv6_prefix_info_t* matching_prefix_info = NULL;

        when_null_trace(obj, exit, ERROR, "Could not find object matching with '%s'", path);
        when_null_trace(obj->priv, exit, ERROR, "Object does not contain prefix info");
        matching_prefix_info = (ipv6_prefix_info_t*) obj->priv;

        amxc_string_prependf(amxc_string_from_llist_it(it), "Device."); // Changes the value in path variable, only do after fetching the obj
        amxc_string_shrink(amxc_string_from_llist_it(it), 1);
        path = amxc_string_get(amxc_string_from_llist_it(it), 0);
        if(obj == prefix_info->prefix_obj) {
            SAH_TRACEZ_INFO(ME, "Prefix found itself, ignoring");
            rv--;
            continue;
        }
        if(!str_empty(parent_prefix) && (strcmp(path, parent_prefix) == 0)) { // parent_prefix allowed to be empty in case of ULA prefix
            SAH_TRACEZ_INFO(ME, "Found parent, ignoring");
            rv--;
            continue;
        }
        if(primary_prefix || (prefix_info->intf_obj->index < matching_prefix_info->intf_obj->index)) {
            rv = dm_ipv6_prefix_set(NULL, matching_prefix_info, NULL, NULL, 0, 0);
            ip_manager_set_status(matching_prefix_info->prefix_obj, STATUS_ERROR);
        }
    }

exit:
    amxc_llist_clean(&entries, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Combines a child and parent prefix into a single prefix/address
 * @param prefix_info pointer to prefix info struct when combining prefixes, when generating an address (combining prefix and interfaceID, this should be NULL)
 * @param child string representation of the child prefix (or interfaceID for addresses), should be in cidr notation
 * @param parent string representation of the parent prefix, should be in cidr notation
 * @param combined pointer to a amxc_string, will be used to return the result
 * @param cidr controls if the results is returned in cidr notation or not. If true, largest prefix length will be added to the result.
 * @return 0 when address was generated successful, an error value otherwise. The generated address will be in the combined parameter. If failed, the result in the combined parameter will be an empty string.
 */
int ipv6_child_parent_combine(ipv6_prefix_info_t* prefix_info,
                              const char* child,
                              const char* parent,
                              amxc_string_t* combined,
                              bool cidr) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    int child_length = -1;
    int parent_length = -1;
    long unsigned int i = 0;
    unsigned int parent_buf[sizeof(struct in6_addr)];
    unsigned int child_buf[sizeof(struct in6_addr)];
    unsigned int ip_buf[sizeof(struct in6_addr)];
    char str[INET6_ADDRSTRLEN];
    bool primary_prefix = false;

    when_str_empty_trace(child, exit, ERROR, "Child prefix can not be empty");
    when_str_empty_trace(parent, exit, ERROR, "Parent prefix can not be empty");
    when_null_trace(combined, exit, ERROR, "Destination string can not be NULL");

    child_length = ipat_text_prefix_length(child);
    parent_length = ipat_text_prefix_length(parent);
    amxc_string_set(combined, "");

    when_true_trace(child_length < parent_length, exit, ERROR, "Parent prefix longer than child prefix, can't generate prefix");

    if((prefix_info != NULL) && (strcmp(prefix_info->intf_obj->name, "lan") == 0) && (strcmp(prefix_info->prefix_obj->name, "GUA") == 0)) {
        primary_prefix = true; // We want to give priority to the LAN GUA prefix, if only one prefix is possible it should be set on this one.
    }
    when_true_trace(parent_length == 64 &&
                    prefix_info != NULL &&      // When combining the prefix and interfaceID to generate an address, prefix_info will be NULL
                    !primary_prefix,
                    exit, WARNING, "Not generating prefix. If we can only generate one prefix it should be LAN GUA");

    when_failed_trace(ipv6_convert_to_bin(parent, 0, parent_length, parent_buf), exit, ERROR, "Failed to convert parent prefix");
    when_failed_trace(ipv6_convert_to_bin(child, parent_length, child_length, child_buf), exit, ERROR, "Failed to convert child prefix");

    for(i = 0; i < sizeof(struct in6_addr); i++) {
        ip_buf[i] = parent_buf[i] | child_buf[i];
    }
    when_null_trace(inet_ntop(AF_INET6, ip_buf, str, INET6_ADDRSTRLEN), exit, ERROR, "Failed to convert back to string");
    if(cidr) {
        char* ipv6 = ipat_text_set_prefix_length(str, (child_length >= parent_length) ? child_length : parent_length, true);
        amxc_string_set(combined, ipv6);
        free(ipv6);
    } else {
        amxc_string_set(combined, str);
    }

    if(check_duplicate_prefix(prefix_info, combined, primary_prefix) != 0) { // Check if generated prefix already exists
        SAH_TRACEZ_WARNING(ME, "Interface '%s' prefix '%s' is a duplicate", prefix_info->intf_obj->name, prefix_info->prefix_obj->name);
        amxc_string_set(combined, "");
        goto exit;
    }

    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

ipv6_prefix_origin_t prefix_origin_to_enum(const char* s_origin) {
    SAH_TRACEZ_IN(ME);
    ipv6_prefix_origin_t origin = PREFIX_ORIGIN_UNKNOWN;

    when_str_empty_trace(s_origin, exit, ERROR, "Could not convert, empty string");

    if(strcmp(s_origin, "AutoConfigured") == 0) {
        origin = PREFIX_ORIGIN_AUTOCONFIGURED;
    } else if(strcmp(s_origin, "PrefixDelegation") == 0) {
        origin = PREFIX_ORIGIN_PREFIXDELEGATION;
    } else if(strcmp(s_origin, "RouterAdvertisement") == 0) {
        origin = PREFIX_ORIGIN_ROUTERADVERTISEMENT;
    } else if(strcmp(s_origin, "WellKnown") == 0) {
        origin = PREFIX_ORIGIN_WELLKNOWN;
    } else if(strcmp(s_origin, "Static") == 0) {
        origin = PREFIX_ORIGIN_STATIC;
    } else if(strcmp(s_origin, "Child") == 0) {
        origin = PREFIX_ORIGIN_CHILD;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return origin;
}

ipv6_prefix_static_type_t prefix_static_type_to_enum(const char* s_type) {
    SAH_TRACEZ_IN(ME);
    ipv6_prefix_static_type_t static_type = PREFIX_STATIC_TYPE_UNKNOWN;

    when_str_empty_trace(s_type, exit, ERROR, "Could not convert, empty string");

    if(strcmp(s_type, "Static") == 0) {
        static_type = PREFIX_STATIC_TYPE_STATIC;
    } else if(strcmp(s_type, "Inapplicable") == 0) {
        static_type = PREFIX_STATIC_TYPE_INAPPLICABLE;
    } else if(strcmp(s_type, "PrefixDelegation") == 0) {
        static_type = PREFIX_STATIC_TYPE_PREFIXDELEGATION;
    } else if(strcmp(s_type, "Child") == 0) {
        static_type = PREFIX_STATIC_TYPE_CHILD;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return static_type;
}

intf_id_t convert_to_intf_id(const char* s_intf_id_type) {
    SAH_TRACEZ_IN(ME);
    intf_id_t intf_id_type = UNKNOWN_INTF_ID;

    when_str_empty_trace(s_intf_id_type, exit, ERROR, "Could not convert, empty string");

    if(strcmp(s_intf_id_type, "EUI64") == 0) {
        intf_id_type = EUI64_INTF_ID;
    } else if(strcmp(s_intf_id_type, "Fixed") == 0) {
        intf_id_type = FIXED_INTF_ID;
    } else if(strcmp(s_intf_id_type, "None") == 0) {
        intf_id_type = NONE_INTF_ID;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return intf_id_type;
}

amxd_object_t* get_object_from_device_path(const char* device_path) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t path;
    amxd_object_t* object = NULL;

    amxc_string_init(&path, 0);
    when_str_empty(device_path, exit);

    amxc_string_set(&path, device_path);
    amxc_string_replace(&path, "Device.", "", 1);

    object = amxd_dm_findf(ip_manager_get_dm(), "%s", amxc_string_get(&path, 0));

exit:
    amxc_string_clean(&path);
    SAH_TRACEZ_OUT(ME);
    return object;
}

amxd_object_t* get_prefix_object_from_addr(amxd_object_t* addr_obj) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* prefix_obj = NULL;
    const char* prefix_path = NULL;

    when_null_trace(addr_obj, exit, ERROR, "IPv6Address object is NULL");

    prefix_path = GET_CHAR(amxd_object_get_param_value(addr_obj, "Prefix"), NULL);
    when_str_empty_trace(prefix_path, exit, ERROR, "Prefix could not be found");

    prefix_obj = get_object_from_device_path(prefix_path);

exit:
    SAH_TRACEZ_OUT(ME);
    return prefix_obj;
}

int dm_ipv6_set(const char* param, const char* new_value,
                const char* status_param_name,
                amxd_object_t* obj, uint32_t plt, uint32_t vlt,
                const amxc_ts_t* preferred_lt, const amxc_ts_t* valid_lt,
                amxp_timer_t* preferred_timer, amxp_timer_t* valid_timer) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_trans_t* trans = NULL;
    bool enabled = true;
    ipv6_prefix_info_t* prefix_info = NULL;
    bool clearing_dm = false;

    when_str_empty_trace(param, exit, ERROR, "No parameter name provided");
    when_str_empty_trace(status_param_name, exit, ERROR, "No status parameter name provided");
    when_null_trace(obj, exit, ERROR, "No object provided");
    when_null_trace(preferred_timer, exit, ERROR, "No preferred timer provided");
    when_null_trace(valid_timer, exit, ERROR, "No valid timer provided");

    if(strcmp(param, "Prefix") == 0) {
        prefix_info = (ipv6_prefix_info_t*) obj->priv;
    }

    if(str_empty(new_value)) {
        ip_manager_set_status(obj, STATUS_ERROR);
        enabled = false;
        new_value = "";
    }

    trans = ip_manager_trans_create(obj);
    // Some prefix will be user configurable ((StaticType and Origin) = Static).
    // Those prefixes should not be cleared when disabled as the user configuration should be persistent.
    // "clearing_dm" makes sure that the conditions for keeping the dm filled in are met.
    clearing_dm = (prefix_info == NULL) || !((prefix_info->prefix_origin == PREFIX_ORIGIN_STATIC) && (prefix_info->prefix_type == PREFIX_STATIC_TYPE_STATIC));
    if(clearing_dm) {
        amxd_trans_set_value(cstring_t, trans, param, new_value);
    }

    if(enabled && amxc_ts_is_valid(preferred_lt) && amxc_ts_is_valid(valid_lt)) {
        char preferred[36];
        char valid[36];
        amxc_ts_format_precision(preferred_lt, preferred, sizeof(preferred), 0);
        amxd_trans_set_value(cstring_t, trans, "PreferredLifetime", preferred);
        amxd_trans_set_value(uint32_t, trans, "RelativePreferredLifetime", plt);
        amxc_ts_format_precision(valid_lt, valid, sizeof(valid), 0);
        amxd_trans_set_value(cstring_t, trans, "ValidLifetime", valid);
        amxd_trans_set_value(uint32_t, trans, "RelativeValidLifetime", vlt);
    } else if(enabled) {
        amxd_trans_set_value(cstring_t, trans, "PreferredLifetime", INFINITE_LIFETIME);
        amxd_trans_set_value(uint32_t, trans, "RelativePreferredLifetime", UINT32_MAX);
        amxd_trans_set_value(cstring_t, trans, "ValidLifetime", INFINITE_LIFETIME);
        amxd_trans_set_value(uint32_t, trans, "RelativeValidLifetime", UINT32_MAX);
        amxd_trans_set_value(cstring_t, trans, status_param_name, LIFETIME_STATUS_INVALID);
    } else {
        if(clearing_dm) {
            amxd_trans_set_value(cstring_t, trans, "PreferredLifetime", UNKNOWN_LIFETIME);
            amxd_trans_set_value(uint32_t, trans, "RelativePreferredLifetime", 0);
            amxd_trans_set_value(cstring_t, trans, "ValidLifetime", UNKNOWN_LIFETIME);
            amxd_trans_set_value(uint32_t, trans, "RelativeValidLifetime", 0);
        }
        amxd_trans_set_value(cstring_t, trans, status_param_name, LIFETIME_STATUS_INVALID);
        amxp_timer_stop(preferred_timer);
        amxp_timer_stop(valid_timer);
    }
    rv = ip_manager_trans_apply(trans);

    lifetime_timers_start(status_param_name,
                          obj,
                          preferred_timer,
                          valid_timer);

    if(rv != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to set %s '%s' for '%s', return '%d'", param, new_value, amxd_object_get_name(obj, AMXD_OBJECT_NAMED), rv);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "'%s' set successful for '%s'", new_value, amxd_object_get_name(obj, AMXD_OBJECT_NAMED));

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ipv6_add_eui(amxd_object_t* intf_obj, amxc_string_t* ipv6_address) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("NetDev");
    const char* mac_address_s = NULL;
    amxc_string_t tmp_str;
    amxc_var_t data;
    const char* name = GET_CHAR(amxd_object_get_param_value(intf_obj, "Name"), NULL);
    const char* llayer = GET_CHAR(amxd_object_get_param_value(intf_obj, "LowerLayers"), NULL);
    char* ipv6 = NULL;
    intf_type_t type = intf_get_lowerlayer_type(llayer);

    amxc_var_init(&data);
    amxc_string_init(&tmp_str, 0);

    when_str_empty_trace(name, exit, ERROR, "IPv6 address not calculated, no interface name");
    when_false_trace(!amxc_string_is_empty(ipv6_address), exit, ERROR, "No prefix provided");

    if(type == INTF_TYPE_PPP) {
        /* IPv6 Address should be set by the kernel when /proc/sys/net/ipv6/conf/[INTERFACE]/autoconf is set to 1.
           If not, we are either in unnumbered mode and don't need a GUA address or we rely on DHCPv6 IA_NA. */
        ipv6 = strdup("");
        amxc_string_set(ipv6_address, ipv6);
    } else {
        amxc_string_setf(&tmp_str, "NetDev.Link.[Name==\"%s\"].LLAddress", name);
        amxb_get(ctx, amxc_string_get(&tmp_str, 0), 0, &data, 1);
        mac_address_s = GETP_CHAR(&data, "0.0.LLAddress");
        when_str_empty_trace(mac_address_s, exit, ERROR,
                             "IPv6 address not calculated, MACAddress not filled in for '%s'", name);

        ipv6 = ipat_text_make_EUI64(amxc_string_get(ipv6_address, 0), mac_address_s, false);
        when_false_trace(amxc_string_set(ipv6_address, ipv6) > 0, exit, ERROR, "Failed to generate EUI64 address");
    }

    rv = 0;

exit:
    if(rv != 0) {
        amxc_string_set(ipv6_address, "");
    }
    amxc_string_clean(&tmp_str);
    amxc_var_clean(&data);
    free(ipv6);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ipv6_add_fixed(ipv6_addr_info_t* addr_info, amxc_string_t* ipv6_address) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* intf_id = NULL;
    amxc_string_t combined;

    amxc_string_init(&combined, 0);

    when_null_trace(addr_info, exit, ERROR, "No address info provided");
    intf_id = GET_CHAR(amxd_object_get_param_value(addr_info->addr_obj, "InterfaceID"), NULL);
    when_str_empty_trace(intf_id, exit, ERROR, "No interface ID set");
    rv = ipv6_child_parent_combine(NULL, intf_id, amxc_string_get(ipv6_address, 0), &combined, false);
    if(rv != 0) {
        amxc_string_set(ipv6_address, "");
        goto exit;
    }

    amxc_string_copy(ipv6_address, &combined);

exit:
    amxc_string_clean(&combined);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void start_listening_for_changes(const char* prefix_path,
                                 const char* parameter,
                                 amxp_slot_fn_t fn,
                                 void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_dm_t* dm = ip_manager_get_dm();
    amxc_string_t expr;
    amxc_string_init(&expr, 0);

    amxc_string_setf(&expr, "path matches \"^%s.$\" && (contains(\"parameters.%s\") || " \
                     "contains(\"parameters.PreferredLifetime\") || " \
                     "contains(\"parameters.ValidLifetime\") || " \
                     "contains(\"parameters.Enable\"))", prefix_path, parameter);
    amxc_string_replace(&expr, "Device.", "", 1);

    rv = amxp_slot_connect(&dm->sigmngr,
                           "dm:object-changed",
                           amxc_string_get(&expr, 0),
                           fn,
                           priv);

    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to start listening for changes '%s'", amxc_string_get(&expr, 0));
    }

    amxc_string_clean(&expr);
    SAH_TRACEZ_OUT(ME);
    return;
}

void stop_listening_for_prefix_changes(amxp_slot_fn_t fn, void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_dm_t* dm = ip_manager_get_dm();

    amxp_slot_disconnect_with_priv(&dm->sigmngr, fn, priv);
    SAH_TRACEZ_OUT(ME);
}

bool is_dynamic_ip(const char* s_origin) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    when_str_empty_trace(s_origin, exit, ERROR, "Could not convert, empty string");

    if(strcmp(s_origin, "AutoConfigured") == 0) {
        rv = true;
    } else if(strcmp(s_origin, "DHCPv6") == 0) {
        rv = true;
    } else if(strcmp(s_origin, "IKEv2") == 0) {
        rv = true;
    } else if(strcmp(s_origin, "MAP") == 0) {
        rv = true;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ipv6address_build_ctrlr_args_from_object(amxd_object_t* obj,
                                             amxc_var_t* ctrlr_args) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t* params = NULL;
    amxc_var_t* ip_params = NULL;
    amxc_var_t* intf_params = NULL;
    ip_intf_info_t* intf_info = NULL;
    amxd_object_t* intf_obj = amxd_object_findf(obj, ".^.^.");
    amxd_object_t* ip_obj = amxd_object_findf(intf_obj, ".^.^.");

    amxc_var_set_type(ctrlr_args, AMXC_VAR_ID_HTABLE);

    when_null_trace(obj, exit, ERROR, "No IPv6 object provided");
    when_null_trace(intf_obj, exit, ERROR, "Interface object could not be found");
    when_null_trace(ctrlr_args, exit, ERROR, "No variant stucture provided");
    intf_info = (ip_intf_info_t*) intf_obj->priv;
    when_null_trace(intf_info, exit, ERROR, "Interface has no private data");

    ip_params = amxc_var_add_key(amxc_htable_t, ctrlr_args, "ip_parameters", NULL);
    amxd_object_get_params(ip_obj, ip_params, amxd_dm_access_protected);

    intf_params = amxc_var_add_key(amxc_htable_t, ctrlr_args, "intf_parameters", NULL);
    rv = 1;
    when_false_trace(netdev_name_known(intf_obj), exit, INFO, "Can't build IPv6 arguments, NetDev name not known");
    amxd_object_get_params(intf_obj, intf_params, amxd_dm_access_protected);
    amxc_var_add_key(cstring_t, ctrlr_args, "UCIsectionName", GET_CHAR(intf_params, "UCISectionNameIPv6"));
    amxc_var_add_key(cstring_t, ctrlr_args, "ifname", intf_info->intf_name);
    amxc_var_add_key(cstring_t, ctrlr_args, "PrefixLength", "64");
    params = amxc_var_add_key(amxc_htable_t, ctrlr_args, "parameters", NULL);
    amxd_object_get_params(obj, params, amxd_dm_access_protected);

    ipv6_is_enabled(ctrlr_args);
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Calculate the end of the lease time.
 * @param time time in seconds till end of lease time
 * @param lt timestamp for when the time runs out
 */
static int set_lifetime(uint32_t time, amxc_ts_t* lt) {
    int rv = -1;
    if((time == UINT32_MAX) || (time == 0)) {
        const char* lifetime = (time == 0) ? UNKNOWN_LIFETIME : INFINITE_LIFETIME;
        rv = amxc_ts_parse(lt, lifetime, strlen(lifetime));
        when_failed_trace(rv, exit, ERROR, "Failed to set Valid lifetime to '%s'", lifetime);
    } else {
        when_failed_trace(amxc_ts_now(lt), exit, ERROR, "Failed to get current time");
        lt->sec += time;
    }
    rv = 0;

exit:
    return rv;
}

int calculate_lifetimes(uint32_t plt,
                        uint32_t vlt,
                        amxc_ts_t* preferred_lt,
                        amxc_ts_t* valid_lt) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    if(plt > vlt) {
        SAH_TRACEZ_ERROR(ME, "Preferred lifetime greater than the valid lifetime, not using address");
        goto exit;
    }

    rv = set_lifetime(plt, preferred_lt);
    when_failed_trace(rv, exit, ERROR, "Failed to set preferred lifetime");
    rv = set_lifetime(vlt, valid_lt);
    when_failed_trace(rv, exit, ERROR, "Failed to set valid lifetime");

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ipv6_addr_disable(ipv6_addr_info_t* addr_info) {

    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* ip_addr = NULL;
    bool has_netdev_name = false;
    amxc_var_t ctrlr_args;
    amxc_var_t ctrlr_ret;
    amxd_object_t* intf_obj;

    amxc_var_init(&ctrlr_args);
    amxc_var_init(&ctrlr_ret);

    when_null_trace(addr_info, exit, ERROR, "No address info provided");
    when_true_status(addr_info->auto_detected, exit, rv = 0);
    intf_obj = addr_info->intf_obj;
    has_netdev_name = netdev_name_known(intf_obj);
    rv = ipv6address_build_ctrlr_args_from_object(addr_info->addr_obj, &ctrlr_args);

    ip_manager_set_status(addr_info->addr_obj, (GET_BOOL(&ctrlr_args, "combined_enable") && (rv == 0)) ? STATUS_ERROR : STATUS_DISABLED);
    ipv6_addr_info_subscription_clean(addr_info);

    ip_addr = GETP_CHAR(&ctrlr_args, "parameters.IPAddress");
    if(!str_empty(ip_addr) && has_netdev_name) {
        rv = cfgctrlr_execute_function(addr_info->addr_obj, "delete-ipv6address", &ctrlr_args, &ctrlr_ret);
    }

    ipv6_address_stop_subscription(addr_info);

    if((rv != 0) && has_netdev_name) {
        SAH_TRACEZ_ERROR(ME, "Failed to delete-ipv6address '%s'", addr_info->addr_obj->name);
    } else if(!has_netdev_name) {
        SAH_TRACEZ_INFO(ME, "NetDev Name not known");
        rv = 1;
    }
exit:
    amxc_var_clean(&ctrlr_args);
    amxc_var_clean(&ctrlr_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

ipv6_addr_origin_t addr_origin_to_enum(const char* s_origin) {
    SAH_TRACEZ_IN(ME);
    ipv6_addr_origin_t rv = ADDR_ORIGIN_UNKNOWN;
    when_str_empty_trace(s_origin, exit, ERROR, "Could not convert, empty string");

    if(strcmp(s_origin, "AutoConfigured") == 0) {
        rv = ADDR_ORIGIN_AUTOCONFIGURED;
    } else if(strcmp(s_origin, "DHCPv6") == 0) {
        rv = ADDR_ORIGIN_DHCPV6;
    } else if(strcmp(s_origin, "IKEv2") == 0) {
        rv = ADDR_ORIGIN_IKEV2;
    } else if(strcmp(s_origin, "MAP") == 0) {
        rv = ADDR_ORIGIN_MAP;
    } else if(strcmp(s_origin, "WellKnown") == 0) {
        rv = ADDR_ORIGIN_WELLKNOWN;
    } else if(strcmp(s_origin, "Static") == 0) {
        rv = ADDR_ORIGIN_STATIC;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int preferred_timer_start(const char* status_param_name,
                          amxd_object_t* obj,
                          amxp_timer_t* preferred_timer) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    uint32_t plt = 0;
    uint32_t vlt = 0;
    when_null_trace(obj, exit, ERROR, "No object provided");
    when_null_trace(preferred_timer, exit, ERROR, "No preferred timer provided");

    plt = amxd_object_get_value(uint32_t, obj, "RelativePreferredLifetime", NULL);
    vlt = amxd_object_get_value(uint32_t, obj, "RelativeValidLifetime", NULL);
    if(plt > 0) {
        if(plt != UINT32_MAX) {
            uint32_t plt_ms = plt * 1000;
            if(plt_ms < plt) {
                plt_ms = UINT32_MAX;
            }
            rv = amxp_timer_start(preferred_timer, plt_ms);
            if(rv != 0) {
                SAH_TRACEZ_ERROR(ME, "Failed to start the preferred timer for '%s', error '%d'", amxd_object_get_name(obj, AMXD_OBJECT_NAMED), rv);
                set_status(status_param_name, obj, STATUS_UNKNOWN);
                goto exit;
            }
        }
        if(vlt != 0) {
            set_status(status_param_name, obj, LIFETIME_STATUS_PREFERRED);
        } else {
            set_status(status_param_name, obj, LIFETIME_STATUS_INVALID);
        }
    } else if(vlt > 0) {
        set_status(status_param_name, obj, LIFETIME_STATUS_DEPRECATED);
    } else {
        set_status(status_param_name, obj, LIFETIME_STATUS_INVALID);
    }
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int valid_timer_start(const char* status_param_name,
                      amxd_object_t* obj,
                      amxp_timer_t* valid_timer) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    uint32_t vlt = 0;
    when_null_trace(obj, exit, ERROR, "No object provided");
    when_null_trace(valid_timer, exit, ERROR, "No valid timer provided");

    vlt = amxd_object_get_value(uint32_t, obj, "RelativeValidLifetime", NULL);
    if(vlt > 0) {
        uint32_t plt = amxd_object_get_value(uint32_t, obj, "RelativePreferredLifetime", NULL);
        if(vlt != UINT32_MAX) {
            uint32_t vlt_ms = vlt * 1000;
            if(vlt_ms < vlt) {
                vlt_ms = UINT32_MAX;
            }
            rv = amxp_timer_start(valid_timer, vlt_ms);
            if(rv != 0) {
                SAH_TRACEZ_ERROR(ME, "Failed to start the valid timer for '%s', error '%d'", amxd_object_get_name(obj, AMXD_OBJECT_NAMED), rv);
                set_status(status_param_name, obj, STATUS_UNKNOWN);
                goto exit;
            }
        }
        if(plt != 0) {
            set_status(status_param_name, obj, LIFETIME_STATUS_PREFERRED);
        } else {
            set_status(status_param_name, obj, LIFETIME_STATUS_DEPRECATED);
        }
    } else {
        set_status(status_param_name, obj, LIFETIME_STATUS_INVALID);
    }
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}


/** @brief This function toggles the read-only flag of the given object lifetime parameters
 * @param obj Pointer to the datamodel object
 * @param is_read_only If set to false all lifetime parameters for this object will be made read-only,
 *  when true the parameters will be able to be written
 * @return 0 if flag was toggled successful, otherwise a different value is returned
 */
int toggle_lifetime_read_only(amxd_object_t* obj, bool is_read_only) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    amxd_param_t* preferredlifetime = amxd_object_get_param_def(obj, "PreferredLifetime");
    amxd_param_t* validlifetime = amxd_object_get_param_def(obj, "ValidLifetime");

    when_null_trace(preferredlifetime, exit, ERROR, "PreferredLifetime could not be found");
    when_null_trace(validlifetime, exit, ERROR, "ValidLifetime could not be found");
    rv = amxd_param_set_attr(preferredlifetime, amxd_pattr_read_only, is_read_only);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the read-only flag", is_read_only ? "add" : "remove");
    rv = amxd_param_set_attr(validlifetime, amxd_pattr_read_only, is_read_only);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the read-only flag", is_read_only ? "add" : "remove");
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int calculate_and_set_relative_lifetimes(const amxc_ts_t* lifetime,
                                         amxd_object_t* obj,
                                         const char* lifetime_name) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_ts_t now;
    amxd_trans_t* trans = NULL;
    int64_t relative_lifetime = 0;

    when_null_trace(lifetime, exit, ERROR, "No lifetime timestamp provided");
    when_null_trace(obj, exit, ERROR, "No object provided");
    when_str_empty_trace(lifetime_name, exit, ERROR, "No parameter name provided");

    amxc_ts_now(&now);
    relative_lifetime = lifetime->sec - now.sec;
    if(relative_lifetime < 0) {
        relative_lifetime = 0;
    } else if(relative_lifetime > UINT32_MAX) {
        relative_lifetime = UINT32_MAX;
    }

    trans = ip_manager_trans_create(obj);
    amxd_trans_set_value(uint32_t, trans, lifetime_name, relative_lifetime);
    rv = ip_manager_trans_apply(trans);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void init_relative_lifetimes(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t params;
    const amxc_ts_t* preferred_lt = NULL;
    const amxc_ts_t* valid_lt = NULL;

    amxc_var_init(&params);
    amxd_object_get_params(obj, &params, amxd_dm_access_protected);
    preferred_lt = amxc_var_constcast(amxc_ts_t, GET_ARG(&params, "PreferredLifetime"));
    valid_lt = amxc_var_constcast(amxc_ts_t, GET_ARG(&params, "ValidLifetime"));

    calculate_and_set_relative_lifetimes(preferred_lt, obj, "RelativePreferredLifetime");
    calculate_and_set_relative_lifetimes(valid_lt, obj, "RelativeValidLifetime");

    amxc_var_clean(&params);

    SAH_TRACEZ_OUT(ME);
}

void lifetime_timers_start(const char* status_param_name,
                           amxd_object_t* obj,
                           amxp_timer_t* preferred_timer,
                           amxp_timer_t* valid_timer) {
    preferred_timer_start(status_param_name, obj, preferred_timer);
    valid_timer_start(status_param_name, obj, valid_timer);
}

/**
 * @brief Sets the address and its timestamps in the datamodel
 * @param ipv6_address char string containing the address without any prefix length information
 * @param addr_info info structure for which the address should be set
 * @param preferred_lt amxc timestamp containing the absolute time until the address is no longer preferred
 * @param valid_lt amxc timestamp containing the absolute time until the address is no longer valid
 * @param plt the time in seconds until the address is no longer preferred
 * @param vlt the time in seconds until the address is no longer valid
 * @return amxd_status_ok when the status is applied, otherwise an other error code and no changes in the data model are done
 */
int dm_ipv6_address_set(const char* ipv6_address,
                        ipv6_addr_info_t* addr_info,
                        const amxc_ts_t* preferred_lt,
                        const amxc_ts_t* valid_lt,
                        const uint32_t plt,
                        const uint32_t vlt) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    when_null_trace(addr_info, exit, ERROR, "No address info provided");

    rv = dm_ipv6_set("IPAddress", ipv6_address, ADDR_STATUS_PARAM,
                     addr_info->addr_obj, plt, vlt,
                     preferred_lt, valid_lt,
                     addr_info->preferred_timer, addr_info->valid_timer);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Sets the a prefix and its timestamps in the datamodel
 * @param prefix amxc string containing the full prefix including the prefix length in cidr notation.
 *               When NULL is provided the prefix will be cleared.
 * @param prefix_info info structure for which the prefix should be set
 * @param preferred_lt amxc timestamp containing the absolute time until the prefix is no longer preferred
 * @param valid_lt amxc timestamp containing the absolute time until the prefix is no longer valid
 * @param plt the time in seconds until the prefix is no longer preferred
 * @param vlt the time in seconds until the prefix is no longer valid
 * @return amxd_status_ok when the status is applied, otherwise an other error code and no changes in the data model are done
 */
int dm_ipv6_prefix_set(const amxc_string_t* prefix,
                       ipv6_prefix_info_t* prefix_info,
                       const amxc_ts_t* preferred_lt,
                       const amxc_ts_t* valid_lt,
                       uint32_t plt,
                       uint32_t vlt) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* new_prefix = NULL;

    when_null_trace(prefix_info, exit, ERROR, "No prefix info provided");

    if(prefix != NULL) {
        new_prefix = amxc_string_get(prefix, 0);
    }

    rv = dm_ipv6_set("Prefix", new_prefix, PREFIX_STATUS_PARAM,
                     prefix_info->prefix_obj, plt, vlt,
                     preferred_lt, valid_lt,
                     prefix_info->preferred_timer, prefix_info->valid_timer);

    ip_manager_set_status(prefix_info->prefix_obj, (rv == 0 && !str_empty(new_prefix)) ? STATUS_ENABLED : STATUS_ERROR);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Will take the provided parent address and completes it with an interfaces ID as configured in the address instance.
 * @param addr_info address info structure for the address that should be generated
 * @param ipv6_address string IPv6 address, should contain the parent value and will contain the full address after returning
 * @return 0 if successful (ipv6_address will contain the new address), error value otherwise (ipv6_address will be empty string)
 */
int ipv6_intf_id_set(ipv6_addr_info_t* addr_info,
                     amxc_string_t* ipv6_address) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    int pos = -1;

    when_null_trace(addr_info, exit, ERROR, "No address info provided");
    pos = amxc_string_search(ipv6_address, ":/", 0);
    if(pos > 0) {
        switch(addr_info->intf_id_type) {
        case EUI64_INTF_ID:
            rv = ipv6_add_eui(addr_info->intf_obj, ipv6_address);
            break;
        case FIXED_INTF_ID:
            rv = ipv6_add_fixed(addr_info, ipv6_address);
            break;
        case NONE_INTF_ID:
            break;
        default:
            SAH_TRACEZ_ERROR(ME, "Unhandled interface ID type");
            break;
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ipv6_generate_and_set_in_dm(ipv6_addr_info_t* addr_info,
                                const char* prefix_path) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipv6_prefix_info_t* prefix_info = NULL;
    amxc_string_t ipv6_address;
    amxd_object_t* prefix_obj = get_object_from_device_path(prefix_path);
    amxc_ts_t* preferred_lt = NULL;
    amxc_ts_t* valid_lt = NULL;
    uint32_t plt = 0;
    uint32_t vlt = 0;
    const char* prefix = NULL;
    bool enable = false;
    bool skip = false;
    amxc_string_init(&ipv6_address, 0);

    when_null_trace(prefix_obj, exit, INFO, "Prefix object '%s' does not exist", prefix_path);
    prefix_info = (ipv6_prefix_info_t*) prefix_obj->priv;
    when_null_trace(prefix_info, exit, ERROR, "Could not get prefix info structure");

    prefix = GET_CHAR(amxd_object_get_param_value(prefix_obj, "Prefix"), NULL);
    enable = GET_BOOL(amxd_object_get_param_value(prefix_obj, "Enable"), NULL);
    //Skipping ipv6 generation if the prefix is static (origin and type) while being disabled
    //This prevents creating ipv6 addresses that are related to a disabled user defined prefix ((StaticType and Origin) = Static)
    skip = !enable && (prefix_info->prefix_origin == PREFIX_ORIGIN_STATIC) && (prefix_info->prefix_type == PREFIX_STATIC_TYPE_STATIC);
    when_str_empty_trace(prefix, exit, INFO, "Prefix not yet filled in for '%s'", amxd_object_get_name(prefix_obj, AMXD_OBJECT_NAMED));
    when_true_trace(skip, exit, INFO, "Static prefix not enabled, skipping ipv6 address generation");

    amxc_string_set(&ipv6_address, prefix);
    ipv6_intf_id_set(addr_info, &ipv6_address); // In case of failure, still set ipv6_address in DM to clear value

    preferred_lt = amxd_object_get_value(amxc_ts_t, prefix_obj, "PreferredLifetime", NULL);
    valid_lt = amxd_object_get_value(amxc_ts_t, prefix_obj, "ValidLifetime", NULL);
    plt = amxd_object_get_value(uint32_t, prefix_obj, "RelativePreferredLifetime", NULL);
    vlt = amxd_object_get_value(uint32_t, prefix_obj, "RelativeValidLifetime", NULL);
    rv = dm_ipv6_address_set(amxc_string_get(&ipv6_address, 0), addr_info, preferred_lt, valid_lt, plt, vlt);

exit:
    free(preferred_lt);
    free(valid_lt);
    amxc_string_clean(&ipv6_address);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ipv6_prefix_autoconfigured(ipv6_prefix_info_t* prefix_info) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_dm_t* dm = ip_manager_get_dm();
    amxc_string_t combined;
    amxc_string_t prefix;
    const char* ula_prefix = NULL;
    const char* child_prefix_bits = NULL;

    amxc_string_init(&combined, 0);
    amxc_string_init(&prefix, 0);

    when_null(prefix_info, exit);
    when_null(prefix_info->prefix_obj, exit);

    // Start listening for when the prefix changes
    start_listening_for_changes("IP", "ULAPrefix", new_ulaprefix_cb, prefix_info);

    ula_prefix = GET_CHAR(amxd_object_get_param_value(amxd_dm_get_object(dm, "IP"), "ULAPrefix"), NULL);
    when_str_empty_trace(ula_prefix, exit, INFO, "ULAPrefix not yet filled in");

    child_prefix_bits = GET_CHAR(amxd_object_get_param_value(prefix_info->prefix_obj, "ChildPrefixBits"), NULL);

    ipv6_child_parent_combine(prefix_info, child_prefix_bits, ula_prefix, &combined, true); // In case of failure, still set combined in DM to clear value

    rv = dm_ipv6_prefix_set(&combined, prefix_info, NULL, NULL, 0, 0);

exit:
    amxc_string_clean(&prefix);
    amxc_string_clean(&combined);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ipv6_prefix_child(ipv6_prefix_info_t* prefix_info) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_string_t combined;
    amxd_object_t* parent_prefix_obj = NULL;
    const char* parent_prefix_path = NULL;
    const char* parent_prefix = NULL;
    const char* child_prefix_bits = NULL;
    const amxc_ts_t* preferred_lt = NULL;
    const amxc_ts_t* valid_lt = NULL;
    uint32_t plt = 0;
    uint32_t vlt = 0;
    amxc_var_t params;

    amxc_string_init(&combined, 0);
    amxc_var_init(&params);

    when_null(prefix_info, exit);
    when_null(prefix_info->prefix_obj, exit);

    parent_prefix_path = GET_CHAR(amxd_object_get_param_value(prefix_info->prefix_obj, "ParentPrefix"), NULL);
    parent_prefix_obj = get_object_from_device_path(parent_prefix_path);
    when_null_trace(parent_prefix_obj, exit, ERROR, "Could not find object '%s'", parent_prefix_path);
    parent_prefix = GET_CHAR(amxd_object_get_param_value(parent_prefix_obj, "Prefix"), NULL);
    child_prefix_bits = GET_CHAR(amxd_object_get_param_value(prefix_info->prefix_obj, "ChildPrefixBits"), NULL);

    // Start listening for when the prefix changes
    start_listening_for_changes(parent_prefix_path, "Prefix", new_prefix_cb, prefix_info);

    when_str_empty_trace(parent_prefix, exit, INFO, "Prefix not yet filled in for '%s'", parent_prefix_path);
    ipv6_child_parent_combine(prefix_info, child_prefix_bits, parent_prefix, &combined, true); // In case of failure, still set combined in DM to clear value

    amxd_object_get_params(parent_prefix_obj, &params, amxd_dm_access_protected);
    preferred_lt = amxc_var_constcast(amxc_ts_t, GET_ARG(&params, "PreferredLifetime"));
    valid_lt = amxc_var_constcast(amxc_ts_t, GET_ARG(&params, "ValidLifetime"));
    vlt = GET_UINT32(&params, "RelativeValidLifetime");
    plt = GET_UINT32(&params, "RelativePreferredLifetime");

    rv = dm_ipv6_prefix_set(&combined, prefix_info, preferred_lt, valid_lt, plt, vlt);

exit:
    amxc_string_clean(&combined);
    amxc_var_clean(&params);
    SAH_TRACEZ_OUT(ME);
    return rv;
}
