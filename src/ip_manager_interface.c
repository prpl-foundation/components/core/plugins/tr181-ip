/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_controller.h"
#include "ipv4_addresses.h"
#include "ipv6_utils.h"
#include "ipv6_addresses.h"
#include "ipv6_prefixes.h"
#include "ip_manager_common.h"
#include "ip_manager_netdev.h"

#include "ip_manager_interface.h"

#define ME "ip-mngr"

typedef enum _ip_version {
    IPv4 = 4,
    IPv6 = 6
} ip_version_t;

static void interface_enable(amxd_object_t* intf_obj, bool enable);
static int toggle_ipv4_on_interface(amxd_object_t* templ, amxd_object_t* intf_obj, void* priv);
static int toggle_ipv6_on_interface(amxd_object_t* templ, amxd_object_t* intf_obj, void* priv);

/*
 * Constructs and initializes ip_intf_info_t structure for a given IP.Interface DM instance
 */
int ip_intf_info_new(ip_intf_info_t** intf_info, amxd_object_t* intf_obj) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    when_null_trace(intf_info, exit, ERROR, "Failed to create interface info, NULL pointer given");
    when_null_trace(intf_obj, exit, ERROR, "No interface object provided");

    *intf_info = (ip_intf_info_t*) calloc(1, sizeof(ip_intf_info_t));
    when_null_trace(*intf_info, exit, ERROR, "Failed to create interface info, out of memory");

    (*intf_info)->last_changed_timestamp = get_system_uptime();
    (*intf_info)->obj = intf_obj;
    (*intf_info)->ndl_ipv4addr_subscr = NULL;
    (*intf_info)->ndl_ipv6addr_subscr = NULL;
    (*intf_info)->name_query = NULL;
    (*intf_info)->status_query = NULL;
    (*intf_info)->mtu_name_query = NULL;
    (*intf_info)->obj->priv = *intf_info;

    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ip_intf_info_clean(ip_intf_info_t** intf_info) {
    SAH_TRACEZ_IN(ME);
    if((intf_info == NULL) || (*intf_info == NULL)) {
        goto exit;
    }

    ip_interface_subscriptions_clean(*intf_info);
    nm_close_query(&(*intf_info)->name_query);
    nm_close_query(&(*intf_info)->mtu_name_query);
    nm_close_query(&(*intf_info)->status_query);
    free((*intf_info)->intf_name);
    free((*intf_info)->nm_intf_path);
    free((*intf_info)->mtu_intf_name);
    (*intf_info)->obj->priv = NULL;
    free(*intf_info);
    *intf_info = NULL;

exit:
    SAH_TRACEZ_OUT(ME);
    return 0;
}

int ip_interface_subscriptions_clean(ip_intf_info_t* ip_intf) {
    int rv = -1;

    when_null_trace(ip_intf, exit, ERROR, "No interface structure provided");

    rv = amxb_subscription_delete(&ip_intf->ndl_ipv4addr_subscr);
    ip_intf->ndl_ipv4addr_subscr = NULL;
    rv |= amxb_subscription_delete(&ip_intf->ndl_ipv6addr_subscr);
    ip_intf->ndl_ipv6addr_subscr = NULL;

exit:
    return rv;
}

amxd_status_t _Reset(amxd_object_t* intf,
                     UNUSED amxd_function_t* func,
                     UNUSED amxc_var_t* args,
                     UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_object_not_found;
    bool enable = false;

    when_null_trace(intf, exit, ERROR, "Interface object not found");

    status = amxd_status_ok;

    enable = amxd_object_get_value(bool, intf, "Enable", NULL);
    when_false_trace(enable, exit, INFO, "Interface '%s' is disabled, do nothing", amxd_object_get_name(intf, AMXD_OBJECT_NAMED));

    interface_enable(intf, false);
    interface_enable(intf, true);

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _interface_destroy(amxd_object_t* intf,
                                 UNUSED amxd_param_t* param,
                                 amxd_action_t reason,
                                 UNUSED const amxc_var_t* const args,
                                 UNUSED amxc_var_t* const retval,
                                 UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    ip_intf_info_t* intf_info = NULL;

    when_false_trace(reason == action_object_destroy, exit, NOTICE, "Wrong reason, expected action_object_destroy(%d) got %d", action_object_destroy, reason);

    when_null_trace(intf, exit, ERROR, "Interface can not be NULL");
    intf_info = (ip_intf_info_t*) intf->priv;
    SAH_TRACEZ_INFO(ME, "Deleting interface %s", intf->name);
    ip_intf_info_clean(&intf_info);
    status = amxd_status_ok;

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _lastchange_read(amxd_object_t* intf,
                               UNUSED amxd_param_t* param,
                               amxd_action_t reason,
                               UNUSED const amxc_var_t* const args,
                               amxc_var_t* const retval,
                               UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    ip_intf_info_t* intf_priv = NULL;
    time_t current_time = get_system_uptime();

    if(reason != action_param_read) {
        SAH_TRACEZ_WARNING(ME, "custom read handler for LastChange called with reason [%d]", reason);
        status = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(intf, exit, ERROR, "Interface object can not be NULL");
    when_null_trace(retval, exit, ERROR, "Interface object can not be NULL");

    intf_priv = (ip_intf_info_t*) intf->priv;
    if(intf_priv != NULL) {
        amxc_var_set(uint32_t, retval, (current_time - intf_priv->last_changed_timestamp));
    } else {
        amxc_var_set(uint32_t, retval, 0);
    }

    status = amxd_status_ok;

exit:
    return status;
}

amxd_status_t _reset_read(UNUSED amxd_object_t* intf,
                          UNUSED amxd_param_t* param,
                          amxd_action_t reason,
                          UNUSED const amxc_var_t* const args,
                          amxc_var_t* const retval,
                          UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    if(reason != action_param_read) {
        SAH_TRACEZ_WARNING(ME, "custom read handler for Reset called with reason [%d]", reason);
        status = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(retval, exit, ERROR, "Return variable cannot be NULL");

    amxc_var_set(bool, retval, false);

    status = amxd_status_ok;

exit:
    return status;
}

static int interface_toggle_build_ctrlr_args_from_object(amxd_object_t* intf_obj,
                                                         amxc_var_t* ctrlr_args,
                                                         bool enable) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t* params = NULL;
    amxc_var_t* parent_params = NULL;
    amxd_object_t* parent_obj = amxd_object_findf(intf_obj, ".^.^");
    ip_intf_info_t* intf_info = NULL;
    bool ipv6_enable = false;
    bool intf_enable = false;
    bool intf_ipv6_enable = false;

    amxc_var_set_type(ctrlr_args, AMXC_VAR_ID_HTABLE);

    when_null_trace(intf_obj, exit, ERROR, "Interface object could not be found");
    when_null_trace(parent_obj, exit, ERROR, "Parent object for '%s' could not be found", intf_obj->name);

    intf_info = (ip_intf_info_t*) intf_obj->priv;
    when_null_trace(intf_info, exit, ERROR, "Interface has no private data");

    params = amxc_var_add_key(amxc_htable_t, ctrlr_args, "parameters", NULL);
    amxd_object_get_params(intf_obj, params, amxd_dm_access_protected);
    amxc_var_add_key(cstring_t, ctrlr_args, "UCIsectionName", GET_CHAR(params, "UCISectionNameIPv6"));
    amxc_var_add_key(cstring_t, ctrlr_args, "ifname", intf_info->intf_name);
    amxc_var_add_key(cstring_t, ctrlr_args, "Type", intf_get_lowerlayer_type_str(GET_CHAR(params, "LowerLayers")));
    parent_params = amxc_var_add_key(amxc_htable_t, ctrlr_args, "parent_parameters", NULL);
    amxd_object_get_params(parent_obj, parent_params, amxd_dm_access_protected);

    ipv6_enable = GETP_BOOL(ctrlr_args, "parent_parameters.IPv6Enable");
    intf_enable = GETP_BOOL(ctrlr_args, "parameters.Enable");
    intf_ipv6_enable = GETP_BOOL(ctrlr_args, "parameters.IPv6Enable");
    amxc_var_add_key(bool, ctrlr_args, "combined_enable", enable && ipv6_enable && intf_enable && intf_ipv6_enable);
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int toggle_ipv4_address(UNUSED amxd_object_t* templ,
                               amxd_object_t* instance,
                               UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipv4_addr_handler_t handler = NULL;

    when_null_trace(priv, exit, ERROR, "No handler provided");
    when_null_trace(instance, exit, ERROR, "No instance provided");
    when_null_trace(instance->priv, exit, INFO, "No instance->priv provided");
    handler = (ipv4_addr_handler_t) priv;
    rv = handler((ipv4_addr_info_t*) instance->priv);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

intf_type_t intf_get_lowerlayer_type(const char* llayer) {
    amxc_string_t ll;
    intf_type_t type = INTF_TYPE_ETHERNET;

    amxc_string_init(&ll, 0);
    amxc_string_set(&ll, llayer);

    if(amxc_string_search(&ll, "Device.Ethernet.VLANTermination.", 0) == 0) {
        type = INTF_TYPE_VLAN;
    } else if(amxc_string_search(&ll, "Device.PPP.Interface.", 0) == 0) {
        type = INTF_TYPE_PPP;
    } else if(amxc_string_search(&ll, "Device.Cellular.Interface.", 0) == 0) {
        type = INTF_TYPE_CELLULAR;
    }

    amxc_string_clean(&ll);
    return type;
}

const char* intf_get_lowerlayer_type_str(const char* llayer) {
    const char* type_str = INTF_TYPE_STR_ETHERNET;
    intf_type_t type = intf_get_lowerlayer_type(llayer);

    switch(type) {
    case INTF_TYPE_VLAN:
        type_str = INTF_TYPE_STR_VLAN;
        break;
    case INTF_TYPE_PPP:
        type_str = INTF_TYPE_STR_PPP;
        break;
    case INTF_TYPE_CELLULAR:
        type_str = "cellular";
        break;
    default:
        break;
    }

    return type_str;
}

static uint32_t get_default_mtu(void) {
    amxc_var_t* default_mtu = amxo_parser_get_config(ip_manager_get_parser(), "default_mtu");
    return amxc_var_dyncast(uint32_t, default_mtu);
}

static void intf_option_build_ctrlr_args_from_object(amxd_object_t* intf_obj,
                                                     amxc_var_t* ctrlr_args) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* intf_params = NULL;

    amxc_var_set_type(ctrlr_args, AMXC_VAR_ID_HTABLE);

    when_null_trace(intf_obj, exit, ERROR, "Interface object not provided");
    when_null_trace(ctrlr_args, exit, ERROR, "No variant stucture provided");

    intf_params = amxc_var_add_key(amxc_htable_t, ctrlr_args, "intf_parameters", NULL);
    amxd_object_get_params(intf_obj, intf_params, amxd_dm_access_protected);
    amxc_var_add_key(cstring_t, ctrlr_args, "UCIsectionName4", GET_CHAR(intf_params, "UCISectionNameIPv4"));
    amxc_var_add_key(cstring_t, ctrlr_args, "UCIsectionName6", GET_CHAR(intf_params, "UCISectionNameIPv6"));
    amxc_var_add_key(cstring_t, ctrlr_args, "ifname", GET_CHAR(intf_params, "Name"));
    amxc_var_add_key(cstring_t, ctrlr_args, "Type", intf_get_lowerlayer_type_str(GET_CHAR(intf_params, "LowerLayers")));

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static int mod_set_mtu(const char* name, uint32_t mtu) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t ctrlr_args;
    amxc_var_t ctrlr_ret;
    amxc_var_init(&ctrlr_args);
    amxc_var_init(&ctrlr_ret);

    amxc_var_set_type(&ctrlr_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &ctrlr_args, "ifname", name);
    amxc_var_add_key(uint32_t, &ctrlr_args, "MaxMTUSize", mtu);

    rv = cfgctrlr_execute_function(NULL, "set-mtu", &ctrlr_args, &ctrlr_ret);
    when_failed_trace(rv, exit, ERROR, "Error returned by 'set-mtu'");

exit:
    amxc_var_clean(&ctrlr_args);
    amxc_var_clean(&ctrlr_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static uint32_t mod_get_mtu(const char* name) {
    SAH_TRACEZ_IN(ME);
    uint32_t rv = 0;
    amxc_var_t ctrlr_args;
    amxc_var_t ctrlr_ret;
    amxc_var_init(&ctrlr_args);
    amxc_var_init(&ctrlr_ret);

    amxc_var_set_type(&ctrlr_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &ctrlr_args, "ifname", name);

    when_failed_trace(cfgctrlr_execute_function(NULL, "get-mtu", &ctrlr_args, &ctrlr_ret),
                      exit, ERROR, "Error returned by 'get-mtu'");
    rv = GET_UINT32(&ctrlr_ret, NULL);

exit:
    amxc_var_clean(&ctrlr_args);
    amxc_var_clean(&ctrlr_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int set_neigh_config(amxd_object_t* if_obj, ip_version_t ip_version) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t ctrlr_args;
    amxc_var_t ctrlr_ret;
    ip_intf_info_t* intf_priv = NULL;
    amxd_object_t* config = NULL;
    const char* prefix = ip_manager_get_setting("prefix_");
    amxc_var_init(&ctrlr_args);
    amxc_var_init(&ctrlr_ret);

    when_null_trace(if_obj, exit, ERROR, "Interface object is NULL");
    intf_priv = (ip_intf_info_t*) if_obj->priv;
    when_null_trace(if_obj, exit, ERROR, "Interface object private data is NULL");
    config = amxd_object_findf(if_obj, "%sIPv%uConfig.", prefix, ip_version);
    when_null_trace(config, exit, ERROR, "Failed to find object %sIPv%uConfig", prefix, ip_version);

    amxc_var_set_type(&ctrlr_args, AMXC_VAR_ID_HTABLE);
    amxd_object_get_params(config, &ctrlr_args, amxd_dm_access_protected);
    amxc_var_add_key(cstring_t, &ctrlr_args, "ifname", intf_priv->intf_name);
    amxc_var_add_key(uint32_t, &ctrlr_args, "IPVersion", ip_version);

    rv = cfgctrlr_execute_function(NULL, "set-neigh-config", &ctrlr_args, &ctrlr_ret);
    when_failed_trace(rv, exit, ERROR, "Error returned by 'set-neigh-config'");

exit:
    amxc_var_clean(&ctrlr_args);
    amxc_var_clean(&ctrlr_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int set_uci_name(amxd_object_t* instance) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t ctrlr_args;
    amxc_var_t ctrlr_ret;
    amxc_var_init(&ctrlr_args);
    amxc_var_init(&ctrlr_ret);

    intf_option_build_ctrlr_args_from_object(instance, &ctrlr_args);
    rv = cfgctrlr_execute_function(NULL, "set-ifname", &ctrlr_args, &ctrlr_ret);
    when_failed_trace(rv, exit, ERROR, "Error returned by 'set-ifname'");

exit:
    amxc_var_clean(&ctrlr_args);
    amxc_var_clean(&ctrlr_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void update_mtu(ip_intf_info_t* intf, const char* new_name) {
    amxd_object_t* intf_obj = NULL;
    const char* intf_type = NULL;

    when_null_trace(new_name, exit, ERROR, "New NetDev interface is NULL, can't set MTU");
    when_null_trace(intf, exit, ERROR, "Interface private data is NULL");
    intf_obj = intf->obj;
    when_null_trace(intf_obj, exit, ERROR, "Interface object is NULL");

    intf_type = GET_CHAR(amxd_object_get_param_value(intf_obj, "Type"), NULL);
    when_true_trace((intf_type != NULL) && (strcmp(intf_type, "Tunneled") == 0), exit, INFO, "Interface is of type Tunneled, not setting MTU");

    if(!str_empty(intf->mtu_intf_name) && (strcmp(intf->mtu_intf_name, new_name) != 0)) {
        SAH_TRACEZ_INFO(ME, "Set MTU of interface %s back to %u", intf->mtu_intf_name, intf->old_mtu);
        mod_set_mtu(intf->mtu_intf_name, intf->old_mtu);
        free(intf->mtu_intf_name);
        intf->mtu_intf_name = NULL;
    }

    if(*new_name != '\0') {
        uint32_t dm_mtu = amxd_object_get_value(uint32_t, intf_obj, "MaxMTUSize", NULL);
        intf->old_mtu = mod_get_mtu(new_name);
        if(intf->old_mtu != 0) {
            intf->mtu_intf_name = strdup(new_name);
        }

        if(dm_mtu == intf->old_mtu) {
            SAH_TRACEZ_INFO(ME, "MTU is already correctly set, doing nothing");
        } else if((dm_mtu == get_default_mtu()) && (dm_mtu > intf->old_mtu)) {
            SAH_TRACEZ_WARNING(ME, "Driver set more optimal MTU of %u, not updating MTU value", intf->old_mtu);
        } else {
            SAH_TRACEZ_INFO(ME, "Change MTU from %u to %u", dm_mtu, intf->old_mtu);
            mod_set_mtu(new_name, dm_mtu);
        }
    }
exit:
    return;
}

static void mtu_intf_name_updated_cb(UNUSED const char* sig_name,
                                     const amxc_var_t* data,
                                     void* userdata) {
    SAH_TRACEZ_IN(ME);
    ip_intf_info_t* intf = (ip_intf_info_t*) userdata;
    const char* new_name = NULL;
    when_null_trace(intf, exit, ERROR, "No userdata provided");

    new_name = GET_CHAR(data, NULL);
    when_null_trace(new_name, exit, INFO, "No name known to configure MTU");
    SAH_TRACEZ_INFO(ME, "IP interface '%s' netdev name changing from '%s' to '%s'",
                    intf->obj->name, intf->intf_name != NULL ? intf->intf_name : "", new_name);

    update_mtu(intf, new_name);

exit:
    SAH_TRACEZ_OUT(ME);
    return;

}

static void netdevname_updated_cb(UNUSED const char* sig_name,
                                  const amxc_var_t* data,
                                  void* userdata) {
    SAH_TRACEZ_IN(ME);
    ip_intf_info_t* intf = (ip_intf_info_t*) userdata;
    const char* new_name = NULL;
    when_null_trace(intf, exit, ERROR, "No userdata provided");

    new_name = GET_CHAR(data, NULL);
    when_null_trace(new_name, exit, WARNING, "Received NULL instead of a string");
    SAH_TRACEZ_INFO(ME, "IP interface '%s' netdev name changing from '%s' to '%s'",
                    intf->obj->name, intf->intf_name != NULL ? intf->intf_name : "", new_name);

    handle_new_netdevname(intf, new_name);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief This function will call the module update-ipv6address function on all enabled IPv6 address on an interface.
 * This will set the current value in IPAddress on the interface.
 * This expects all needed queries have been opened and addresses have been generated.
 * If this is not yet the case use ipv6_toggle_addresses
 * @param intf interface info structure for the interface for which the IPv6 addresses should be set
 */
static void ipv6_set_addresses(ip_intf_info_t* intf) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t intf_args;
    const char* type = NULL;
    amxc_var_init(&intf_args);

    rv = interface_toggle_build_ctrlr_args_from_object(intf->obj, &intf_args, true);
    when_failed_trace(rv, exit, ERROR, "Failed to build arguments on interface '%s'", intf->obj->name);

    type = GET_CHAR(&intf_args, "Type");
    when_str_empty_trace(type, exit, ERROR, "No interface type known for interface '%s'", intf->obj->name);
    when_true_trace(strcmp(type, INTF_TYPE_STR_PPP) == 0,
                    exit,
                    INFO,
                    "Skipping setting IPv6 addresses on ppp interface '%s'",
                    intf->obj->name);

    if(GET_BOOL(&intf_args, "combined_enable")) {
        amxd_object_t* ipv6_obj = amxd_object_get_child(intf->obj, "IPv6Address");
        amxd_object_for_each(instance, it, ipv6_obj) {
            amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
            ipv6_addr_info_t* addr_info = (ipv6_addr_info_t*) instance->priv;

            if(!addr_info->auto_detected) {
                amxc_var_t ctrlr_args;
                amxc_var_t ret;

                amxc_var_init(&ctrlr_args);
                amxc_var_init(&ret);

                rv = ipv6address_build_ctrlr_args_from_object(instance, &ctrlr_args);
                if(rv != 0) {
                    SAH_TRACEZ_ERROR(ME, "Failed to build IPv6 arguments");
                } else if(GET_BOOL(&ctrlr_args, "combined_enable") &&
                          !str_empty(GETP_CHAR(&ctrlr_args, "parameters.IPAddress"))) {
                    rv = cfgctrlr_execute_function(instance, "update-ipv6address", &ctrlr_args, &ret);
                    if(rv != 0) {
                        SAH_TRACEZ_ERROR(ME, "Failed to set IPv6 Address '%s'",
                                         GETP_CHAR(&ctrlr_args, "parameters.IPAddress"));
                    }
                }
                amxc_var_clean(&ret);
                amxc_var_clean(&ctrlr_args);
            }
        }
    }

exit:
    amxc_var_clean(&intf_args);
    SAH_TRACEZ_OUT(ME);
    return;
}

static void status_ext_updated_cb(UNUSED const char* sig_name,
                                  const amxc_var_t* data,
                                  void* userdata) {
    SAH_TRACEZ_IN(ME);
    ip_intf_info_t* intf = (ip_intf_info_t*) userdata;
    const char* status = NULL;

    when_null_trace(intf, exit, ERROR, "No userdata provided");
    when_null_trace(intf->obj, exit, ERROR, "Interface object could not be found");

    status = GET_CHAR(data, NULL);
    when_str_empty_trace(status, exit, WARNING, "Received empty external status for %s", intf->intf_name);
    SAH_TRACEZ_INFO(ME, "IP interface '%s' Status_ext changing to '%s'",
                    intf->obj->name, status);

    when_false_trace(netdev_name_known(intf->obj), exit, INFO, "NetDev name not known, can't set IPv6 addresses on interface '%s'", intf->obj->name);
    if(strcmp(status, "Up") == 0) {
        ipv6_set_addresses(intf);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

int ip_interface_set_name(amxd_object_t* intf_obj, const char* name) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_trans_t* trans = NULL;
    trans = ip_manager_trans_create(intf_obj);
    amxd_trans_set_value(cstring_t, trans, "Name", name);
    rv = ip_manager_trans_apply(trans);
    when_failed_trace(rv, exit, ERROR, "Failed to set new interface name '%s' in datamodel, return '%d'", name, rv);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void handle_new_netdevname(ip_intf_info_t* intf, const char* new_name) {
    SAH_TRACEZ_IN(ME);

    when_null_trace(intf, exit, ERROR, "No interface info provided for which to handle the new netdevname");

    if(!str_empty(intf->intf_name) || !str_empty(intf->nm_intf_path)) {
        // We have an interface name
        if(str_empty(intf->intf_name) || str_empty(new_name) || (strcmp(intf->intf_name, new_name) != 0)) {
            // The new name is empty or different than the current one -> remove current IP addresses
            toggle_ip_flags_nm(intf, NULL, false);
            ip_manager_intf_set_status(intf->obj, STATUS_DOWN);
            SAH_TRACEZ_INFO(ME, "Removing IP addresses from NetDev interface '%s'", intf->intf_name);

            ipv4_toggle_addresses(intf->obj, false);
            ipv6_toggle_addresses(intf->obj, false);
            ipv6_toggle_prefixes(intf->obj, false); // Disable the IPv6Addresses before the prefixes
            ip_interface_subscriptions_clean(intf);

            free(intf->intf_name);
            free(intf->nm_intf_path);
            intf->intf_name = NULL;
            intf->nm_intf_path = NULL;
        } else {
            // The new name is identical to the current one -> just make sure status is UP
            ip_manager_intf_set_status(intf->obj, STATUS_UP);
            // Also make sure that the correct flags are set on the netdev interfaces in netmodel
            toggle_ip_flags_nm(intf, NULL, true);
            goto exit;
        }
    }

    // set name in DM
    ip_interface_set_name(intf->obj, new_name);

    // The current name will always be empty at this point (was empty or we just cleared it)
    // If the new name is not empty -> set it and add the IPs
    if(!str_empty(new_name)) {
        intf->intf_name = strdup(new_name);
        SAH_TRACEZ_INFO(ME, "Adding IP addresses to NetDev interface '%s'", intf->intf_name);
        // set name in UCI
        // If empty names would be set in UCI, Interface would not come back up
        set_uci_name(intf->obj);
        // A name is returned if the 'netdev-bound' flag is set, IP interface is considered up in this case
        ip_manager_intf_set_status(intf->obj, STATUS_UP);
        // Do not set the IP addresses when status_down
        ip_address_open_subscriptions(intf->obj, new_name);
        // set IP addresses
        ipv4_toggle_addresses(intf->obj, true);
        ipv6_toggle_prefixes(intf->obj, true); // Enable the prefixes before the IPv6Addresses
        ipv6_toggle_addresses(intf->obj, true);
        intf->nm_intf_path = get_nm_intf_by_netdev_name(intf->intf_name);
        toggle_ip_flags_nm(intf, NULL, true);
        set_neigh_config(intf->obj, IPv4);
        set_neigh_config(intf->obj, IPv6);
    } else {
        ip_manager_intf_set_status(intf->obj, STATUS_DOWN);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

int nm_open_netdevname_query(amxd_object_t* intf_obj) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_string_t ll;
    amxc_string_t flags;
    ip_intf_info_t* intf = NULL;
    bool enable = false;
    const char* intf_path = NULL;
    const char* intf_type = NULL;

    amxc_string_init(&ll, 0);
    amxc_string_init(&flags, 0);

    when_null_trace(intf_obj, exit, ERROR, "could not get interface object");
    when_null_trace(intf_obj->priv, exit, ERROR, "object has no private data");
    intf = (ip_intf_info_t*) intf_obj->priv;
    enable = amxd_object_get_value(bool, intf_obj, "Enable", NULL);
    if(!enable) {
        SAH_TRACEZ_INFO(ME, "Interface '%s' is not enabled, not opening a netmodel query", amxd_object_get_name(intf_obj, AMXD_OBJECT_NAMED));
        rv = 0;
        goto exit;
    }

    if(intf->name_query != NULL) {
        ip_manager_intf_set_status(intf_obj, STATUS_DOWN);
        intf_name_close_query(intf, false);
        toggle_ip_flags_nm(intf, NULL, false);
    }

    if(intf->status_query != NULL) {
        nm_close_query(&intf->status_query);
    }

    if(intf->mtu_name_query != NULL) {
        nm_close_query(&intf->mtu_name_query);
    }

    intf_path = GET_CHAR(amxd_object_get_param_value(intf_obj, "LowerLayers"), NULL);
    if(str_empty(intf_path)) {
        handle_new_netdevname(intf, "");
        rv = 0;
        goto exit;
    }

    intf_type = GET_CHAR(amxd_object_get_param_value(intf_obj, "Type"), NULL);
    if((intf_type != NULL) && (strcmp(intf_type, "Tunneled") == 0)) {
        SAH_TRACEZ_INFO(ME, "Interface is of type Tunneled, Not creating name queries");
        rv = 0;
        goto exit;
    }

    amxc_string_set(&ll, intf_path);
    amxc_string_set(&flags, "netdev-bound");

    if(amxc_string_search(&ll, "Device.Ethernet.VLANTermination.", 0) == 0) {
        amxc_string_appendf(&flags, " %s", "vlan");
    } else if(amxc_string_search(&ll, "Device.PPP.Interface.", 0) == 0) {
        amxc_string_appendf(&flags, " %s", "ppp");
    } else if(amxc_string_search(&ll, "Device.Cellular.Interface.", 0) == 0) {
        amxc_string_appendf(&flags, " %s", "cellular");
    }

    intf->name_query = netmodel_openQuery_getFirstParameter(intf_path,
                                                            ME,
                                                            "NetDevName",
                                                            amxc_string_get(&flags, 0),
                                                            netmodel_traverse_down,
                                                            netdevname_updated_cb,
                                                            intf);

    when_null_trace(intf->name_query, exit, ERROR,
                    "Failed to create netmodel query for NetDevName on interface '%s'", intf_path);

    intf->status_query = netmodel_openQuery_getFirstParameter(intf_path,
                                                              ME,
                                                              "Status_ext",
                                                              amxc_string_get(&flags, 0),
                                                              netmodel_traverse_down,
                                                              status_ext_updated_cb,
                                                              intf);

    when_null_trace(intf->status_query, exit, ERROR,
                    "Failed to create netmodel query for Status_ext on interface '%s'", intf_path);

    intf->mtu_name_query = netmodel_openQuery_getFirstParameter(intf_path,
                                                                ME,
                                                                "NetDevName",
                                                                "netdev-up && !ppp",
                                                                netmodel_traverse_down,
                                                                mtu_intf_name_updated_cb,
                                                                intf);
    when_null_trace(intf->mtu_name_query, exit, ERROR,
                    "Failed to create netmodel query for MTU NetDevName on interface '%s'", intf_path);
    rv = 0;
exit:
    amxc_string_clean(&flags);
    amxc_string_clean(&ll);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void ipv4_toggle_addresses(amxd_object_t* intf_obj, bool enable) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* dm_ipv4addr = NULL;

    when_null_trace(intf_obj, exit, ERROR, "Interface object can not be NULL");
    // Process all IPv4Addresses currently defined on this IP.Interface
    dm_ipv4addr = amxd_object_get_child(intf_obj, "IPv4Address");
    if(enable) {
        amxd_object_for_all(dm_ipv4addr, "*", toggle_ipv4_address, (void*) ipv4_addr_apply);
    } else {
        amxd_object_for_all(dm_ipv4addr, "*", toggle_ipv4_address, (void*) ipv4_addr_disable);
    }

exit:
    SAH_TRACEZ_OUT(ME);
}

bool ipv6_toggle_addresses(amxd_object_t* intf_obj, bool enable) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_object_t* ipv6_obj = NULL;
    amxc_var_t ctrlr_args;
    amxc_var_t ret;
    bool combined_enable = false;
    const char* type = NULL;

    amxc_var_init(&ctrlr_args);
    amxc_var_init(&ret);

    when_null_trace(intf_obj, exit, ERROR, "Interface object could not be found");
    rv = interface_toggle_build_ctrlr_args_from_object(intf_obj, &ctrlr_args, enable);
    when_failed_trace(rv, exit, ERROR, "Failed to build arguments to 'toggle-ipv6' on interface '%s'", intf_obj->name);

    // IPv6 needs to be enabled before we can set (static) IPv6 addresses.
    // If we disable IPv6, we don't need to individually remove all IP addresses
    when_false_trace(netdev_name_known(intf_obj), exit, INFO, "NetDev name not known, can't toggle IPv6 on interface '%s'", intf_obj->name);

    combined_enable = GET_BOOL(&ctrlr_args, "combined_enable");
    ipv6_obj = amxd_object_get_child(intf_obj, "IPv6Address");
    if(!combined_enable) { // Disabling IPv6, set all IPv6Addresses to disabled
        amxd_object_for_each(instance, it, ipv6_obj) {
            amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
            ipv6_addr_info_t* addr_info = (ipv6_addr_info_t*) instance->priv;
            ipv6_addr_disable(addr_info);
        }
    }

    if(enable) {
        type = GET_CHAR(&ctrlr_args, "Type");
        when_str_empty_trace(type, exit, ERROR, "No interface type known for interface '%s'", intf_obj->name);
        when_true_trace(strcmp(type, INTF_TYPE_STR_PPP) == 0, exit, INFO, "Skipping toggle IPv6 on ppp interface '%s'", intf_obj->name);
    }

    rv = cfgctrlr_execute_function(NULL, "toggle-ipv6", &ctrlr_args, &ret);
    when_true_trace(rv == 1, exit, INFO, "Insufficient data provided to 'toggle-ipv6'");
    when_failed_trace(rv, exit, ERROR, "Error returned by 'toggle-ipv6'");

    if(combined_enable) { // Enabling IPv6, enable the addresses who are enabled themselves
        amxd_object_for_each(instance, it, ipv6_obj) {
            amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
            ipv6_addr_info_t* addr_info = (ipv6_addr_info_t*) instance->priv;
            ipv6_addr_apply(addr_info);
        }
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&ctrlr_args);
    SAH_TRACEZ_OUT(ME);
    return combined_enable;
}

void ipv6_toggle_prefixes(amxd_object_t* intf_obj, bool enable) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* prefix_tmpl_obj = NULL;

    when_null_trace(intf_obj, exit, ERROR, "Interface object can not be NULL");
    // Process all IPv6Prefixes currently defined on this IP.Interface
    prefix_tmpl_obj = amxd_object_get_child(intf_obj, "IPv6Prefix");
    amxd_object_for_each(instance, it, prefix_tmpl_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        ipv6_prefix_info_t* prefix_info = (ipv6_prefix_info_t*) instance->priv;
        enable ? ipv6_prefix_apply(prefix_info) : ipv6_prefix_disable(prefix_info);
    }

exit:
    SAH_TRACEZ_OUT(ME);
}

bool netdev_name_known(amxd_object_t* intf_obj) {
    SAH_TRACEZ_IN(ME);
    bool is_known = false;
    ip_intf_info_t* intf = NULL;

    when_null_trace(intf_obj, exit, ERROR, "No interface object provided");
    when_null_trace(intf_obj->priv, exit, INFO, "No private data in interface object");
    intf = (ip_intf_info_t*) intf_obj->priv;
    when_str_empty(intf->intf_name, exit);
    is_known = true;

exit:
    SAH_TRACEZ_OUT(ME);
    return is_known;
}

char* get_nm_intf_by_netdev_name(const char* netdev_name) {
    SAH_TRACEZ_IN(ME);
    char* intf = NULL;
    amxc_string_t str;
    int ret = -1;
    amxc_var_t data;
    const char* alias = NULL;

    amxc_string_init(&str, 0);
    amxc_var_init(&data);
    when_str_empty_trace(netdev_name, exit, ERROR, "NetDev name is empty");

    amxc_string_setf(&str, "NetModel.Intf.[NetDevName == '%s'].", netdev_name);
    ret = amxb_get(netmodel_get_amxb_bus(), amxc_string_get(&str, 0), 0, &data, 5);
    when_failed_trace(ret, exit, ERROR, "Failed to get NetModel interface with NetDevName == %s", netdev_name);

    alias = GETP_CHAR(&data, "0.0.Alias");
    when_str_empty_trace(alias, exit, ERROR, "Failed to get NetModel interface with NetDevName == %s", netdev_name);

    amxc_string_setf(&str, "NetModel.Intf.[%s].", alias);
    intf = amxc_string_take_buffer(&str);

exit:
    amxc_string_clean(&str);
    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
    return intf;
}

int toggle_ip_flags_nm(ip_intf_info_t* intf, const char* flags, bool set) {
    SAH_TRACEZ_IN(ME);
    int ret = -1;
    amxc_string_t str;
    amxc_var_t params;
    amxc_var_init(&params);

    amxc_string_init(&str, 0);
    when_null_trace(intf, exit, ERROR, "Interface can not be NULL");
    when_str_empty_trace(intf->nm_intf_path, exit, ERROR, "No NetModel Intf path stored");

    amxd_object_get_params(intf->obj, &params, amxd_dm_access_private);
    if(strcmp(GET_CHAR(&params, "Type"), "Tunneled") == 0) {
        char* path = amxd_object_get_path(intf->obj, AMXD_OBJECT_INDEXED);
        SAH_TRACEZ_INFO(ME, "Don't set IP flags because interface %s is of type Tunneled", path);
        free(path);
        goto exit;
    }

    if(flags == NULL) {
        if(GET_BOOL(&params, "IPv4Enable")) {
            amxc_string_appendf(&str, " %s", "ipv4");
        }
        if(GET_BOOL(&params, "IPv6Enable")) {
            amxc_string_appendf(&str, " %s", "ipv6");
        }
        flags = amxc_string_get(&str, 0);
    }

    SAH_TRACEZ_INFO(ME, "%s flags \"%s\" for interface \"%s\"", set ? "Setting" : "Clearing", flags, intf->nm_intf_path);

    if(set) {
        netmodel_setFlag(intf->nm_intf_path, flags, NULL, netmodel_traverse_this);
    } else {
        netmodel_clearFlag(intf->nm_intf_path, flags, NULL, netmodel_traverse_this);
    }
    ret = 0;

exit:
    amxc_var_clean(&params);
    amxc_string_clean(&str);
    SAH_TRACEZ_OUT(ME);
    return ret;
}

void _ip_interface_added(UNUSED const char* const sig_name,
                         const amxc_var_t* const data,
                         UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ip_intf_info_t* intf_info = NULL;
    amxd_object_t* intf_tmpl_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    amxd_object_t* intf_obj = amxd_object_get_instance(intf_tmpl_obj, NULL, GET_UINT32(data, "index"));

    when_null_trace(intf_obj, exit, ERROR, "Unable to find interface object to add");
    when_not_null_trace(intf_obj->priv, exit, WARNING, "Interface already initialised");

    rv = ip_intf_info_new(&intf_info, intf_obj);
    when_failed_trace(rv, exit, ERROR, "Failed to create interface");

    nm_open_netdevname_query(intf_obj);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ip_interface_lower_layers_changed(UNUSED const char* const sig_name,
                                        const amxc_var_t* const data,
                                        UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* intf_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    when_null_trace(intf_obj, exit, ERROR, "could not get interface object");
    SAH_TRACEZ_INFO(ME, "Changing LowerLayer from '%s' to '%s'",
                    GETP_CHAR(data, "parameters.LowerLayers.from"),
                    GETP_CHAR(data, "parameters.LowerLayers.to"));
    nm_open_netdevname_query(intf_obj);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _set_max_mtu_size(UNUSED const char* const sig_name,
                       const amxc_var_t* const data,
                       UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* intf_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    ip_intf_info_t* intf = NULL;

    when_null_trace(intf_obj, exit, ERROR, "Interface object is NULL");
    intf = (ip_intf_info_t*) intf_obj->priv;
    when_null_trace(intf, exit, ERROR, "Interface object has no private data");

    when_false_trace(netdev_name_known(intf_obj), exit, INFO, "NetDev name not known, can't set MTUSize");
    SAH_TRACEZ_INFO(ME, "Changing MTU from '%d' to '%d' on '%s'",
                    GETP_UINT32(data, "parameters.MaxMTUSize.from"),
                    GETP_UINT32(data, "parameters.MaxMTUSize.to"),
                    amxd_object_get_name(intf_obj, AMXD_OBJECT_NAMED));

    when_failed_trace(mod_set_mtu(intf->mtu_intf_name, GETP_UINT32(data, "parameters.MaxMTUSize.to")),
                      exit, ERROR, "Failed to update MTU for interface '%s'", amxd_object_get_name(intf_obj, AMXD_OBJECT_NAMED));

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _neigh_reachable_time_changed(UNUSED const char* const sig_name,
                                   const amxc_var_t* const data,
                                   UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* intf_obj = amxd_object_get_parent(amxd_dm_signal_get_object(ip_manager_get_dm(), data));

    if(strstr(GET_CHAR(data, "path"), "IPv4Config") != NULL) {
        set_neigh_config(intf_obj, IPv4);
    } else if(strstr(GET_CHAR(data, "path"), "IPv6Config") != NULL) {
        set_neigh_config(intf_obj, IPv6);
    }
    SAH_TRACEZ_OUT(ME);
}

static void interface_enable(amxd_object_t* intf_obj, bool enable) {
    SAH_TRACEZ_IN(ME);
    ip_intf_info_t* intf = NULL;
    amxc_var_t params;

    amxc_var_init(&params);
    when_null_trace(intf_obj, exit, ERROR, "Interface object not found, failed to toggle");
    when_null_trace(intf_obj->priv, exit, ERROR, "Interface object has no private data");

    intf = (ip_intf_info_t*) intf_obj->priv;

    amxd_object_get_params(intf_obj, &params, amxd_dm_access_private);

    if(enable) {
        nm_open_netdevname_query(intf_obj);
    } else {
        toggle_ip_flags_nm(intf, NULL, false);
        ip_manager_intf_set_status(intf_obj, STATUS_DOWN);
        nm_close_query(&intf->mtu_name_query);
        nm_close_query(&intf->status_query);
        handle_new_netdevname(intf, "");
        intf_name_close_query(intf, true);
    }

exit:
    amxc_var_clean(&params);
    SAH_TRACEZ_OUT(ME);
}

void _enable_toggle_interface(UNUSED const char* const sig_name,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* intf_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    bool enable = GETP_BOOL(data, "parameters.Enable.to");
    interface_enable(intf_obj, enable);
    SAH_TRACEZ_OUT(ME);
}

void _ipv4_toggle_interface(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* intf_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    bool enable = GETP_BOOL(data, "parameters.IPv4Enable.to");
    toggle_ipv4_on_interface(amxd_object_get_parent(intf_obj), intf_obj, &enable);

    SAH_TRACEZ_OUT(ME);
}

void _ipv6_toggle_interface(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* intf_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    bool enable = GETP_BOOL(data, "parameters.IPv6Enable.to");
    toggle_ipv6_on_interface(amxd_object_get_parent(intf_obj), intf_obj, &enable);

    SAH_TRACEZ_OUT(ME);
}

static int toggle_ipv4_on_interface(UNUSED amxd_object_t* templ,
                                    amxd_object_t* intf_obj,
                                    void* priv) {
    SAH_TRACEZ_IN(ME);
    bool enable = *(bool*) priv;
    bool intf_enable = amxd_object_get_value(bool, intf_obj, "Enable", NULL);

    when_false_trace(intf_enable, exit, INFO, "Interface '%s' is not enabled, not toggling ipv4", amxd_object_get_name(intf_obj, AMXD_OBJECT_NAMED));

    if(netdev_name_known(intf_obj)) {
        toggle_ip_flags_nm((ip_intf_info_t*) intf_obj->priv, "ipv4", enable);
    }
    ipv4_toggle_addresses(intf_obj, enable);

exit:
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int toggle_ipv6_on_interface(UNUSED amxd_object_t* templ,
                                    amxd_object_t* instance,
                                    void* priv) {
    SAH_TRACEZ_IN(ME);
    bool enable = *(bool*) priv;
    bool intf_enable = amxd_object_get_value(bool, instance, "Enable", NULL);

    when_false_trace(intf_enable, exit, INFO, "Interface '%s' is not enabled, not toggling ipv6", amxd_object_get_name(instance, AMXD_OBJECT_NAMED));

    if(netdev_name_known(instance)) {
        toggle_ip_flags_nm((ip_intf_info_t*) instance->priv, "ipv6", enable);
    }
    ipv6_toggle_addresses(instance, true);

exit:
    SAH_TRACEZ_OUT(ME);
    return 0;
}

void _ipv4_toggle_all(UNUSED const char* const sig_name,
                      const amxc_var_t* const data,
                      UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    bool enable = GETP_BOOL(data, "parameters.IPv4Enable.to");
    amxd_object_t* ip_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    amxd_object_for_all(ip_obj, ".Interface.*", toggle_ipv4_on_interface, &enable);
    SAH_TRACEZ_OUT(ME);
}

void _ipv6_toggle_all(UNUSED const char* const sig_name,
                      const amxc_var_t* const data,
                      UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    bool enable = GETP_BOOL(data, "parameters.IPv6Enable.to");
    amxd_object_t* ip_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    amxd_object_for_all(ip_obj, ".Interface.[Loopback == false].", toggle_ipv6_on_interface, &enable);
    SAH_TRACEZ_OUT(ME);
}

static int ip_interface_set_reset(amxd_object_t* intf_obj, bool value) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_trans_t* trans = NULL;
    trans = ip_manager_trans_create(intf_obj);
    amxd_trans_set_value(bool, trans, "Reset", value);
    rv = ip_manager_trans_apply(trans);
    when_failed_trace(rv, exit, ERROR, "Failed to set interface '%s' Reset=%d in datamodel, return '%d'", amxd_object_get_name(intf_obj, AMXD_OBJECT_NAMED), value, rv);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void _reset_interface(UNUSED const char* const sig_name,
                      const amxc_var_t* const data,
                      UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* intf_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    bool reset = GETP_BOOL(data, "parameters.Reset.to");
    bool enable = false;

    when_false(reset, exit);

    when_null_trace(intf_obj, exit, ERROR, "Interface object not found");

    ip_interface_set_reset(intf_obj, false);

    enable = amxd_object_get_value(bool, intf_obj, "Enable", NULL);
    when_false_trace(enable, exit, INFO, "Interface '%s' is disabled, do nothing", amxd_object_get_name(intf_obj, AMXD_OBJECT_NAMED));

    interface_enable(intf_obj, false);
    interface_enable(intf_obj, true);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

amxd_status_t _ipv6_interface_going_down(amxd_object_t* object,
                                         amxd_param_t* param,
                                         amxd_action_t reason,
                                         const amxc_var_t* const args,
                                         amxc_var_t* const retval,
                                         void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t data;
    char* path = NULL;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    when_true_status(reason != action_param_write, exit, status = amxd_status_function_not_implemented);

    path = amxd_object_get_path(object, AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    amxc_var_add_key(cstring_t, &data, "Interface", path);

    if(!GET_BOOL(args, NULL)) {
        amxd_object_trigger_signal(object, "ip:ipv6_going_down", &data);
    }

    status = amxd_action_object_write(object, param, reason, args, retval, priv);
exit:
    free(path);
    amxc_var_clean(&data);
    return status;
}
