/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxc/amxc_macros.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_manager_interface.h"
#include "ip_manager_ip.h"
#include "ip_manager_common.h"
#include "ip_manager_controller.h"

#include "ipv6_addresses.h"

#define ME "ipv6"

/** @brief This function toggles the persistency of the given IPv6 address object.
 * For auto detected addresses, all parameters will be made read-only
 * @param ipaddr_obj Pointer to the datamodel object pointing to the IPv6 address
 * @param is_persistent If set to true, the address object will be made persistent. Otherwise persistency will be removed
 */
int ipv6_toggle_persistency(amxd_object_t* ipaddr_obj, bool is_persistent) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_param_t* ipaddress = amxd_object_get_param_def(ipaddr_obj, "IPAddress");
    amxd_param_t* preferredlifetime = amxd_object_get_param_def(ipaddr_obj, "PreferredLifetime");
    amxd_param_t* validlifetime = amxd_object_get_param_def(ipaddr_obj, "ValidLifetime");
    amxd_param_t* relativePreferredLifetime = amxd_object_get_param_def(ipaddr_obj, "RelativeValidLifetime");
    amxd_param_t* relativeValidLifetime = amxd_object_get_param_def(ipaddr_obj, "RelativePreferredLifetime");
    amxd_param_t* enable = amxd_object_get_param_def(ipaddr_obj, "Enable");

    when_null_trace(ipaddr_obj, exit, ERROR, "No IP Address object provided");
    when_null_trace(ipaddress, exit, ERROR, "IPAddress could not be found");
    when_null_trace(preferredlifetime, exit, ERROR, "PreferredLifetime could not be found");
    when_null_trace(validlifetime, exit, ERROR, "ValidLifetime could not be found");
    when_null_trace(relativeValidLifetime, exit, ERROR, "RelativeValidLifetime could not be found");
    when_null_trace(relativePreferredLifetime, exit, ERROR, "RelativePreferredLifetime could not be found");
    when_null_trace(enable, exit, ERROR, "Enable could not be found");

    rv = amxd_param_set_attr(ipaddress, amxd_pattr_persistent, is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the persistent flag", is_persistent ? "add" : "remove");
    rv = amxd_param_set_attr(preferredlifetime, amxd_pattr_persistent, is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the persistent flag", is_persistent ? "add" : "remove");
    rv = amxd_param_set_attr(validlifetime, amxd_pattr_persistent, is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the persistent flag", is_persistent ? "add" : "remove");
    rv = amxd_param_set_attr(relativeValidLifetime, amxd_pattr_persistent, is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the persistent flag", is_persistent ? "add" : "remove");
    rv = amxd_param_set_attr(relativePreferredLifetime, amxd_pattr_persistent, is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the persistent flag", is_persistent ? "add" : "remove");

    if(is_persistent) {
        amxd_param_set_flag(ipaddress, "upc");
        amxd_param_set_flag(preferredlifetime, "upc");
        amxd_param_set_flag(validlifetime, "upc");
        amxd_param_set_flag(relativeValidLifetime, "upc");
        amxd_param_set_flag(relativePreferredLifetime, "upc");
    } else {
        amxd_param_unset_flag(ipaddress, "upc");
        amxd_param_unset_flag(preferredlifetime, "upc");
        amxd_param_unset_flag(validlifetime, "upc");
        amxd_param_unset_flag(relativeValidLifetime, "upc");
        amxd_param_unset_flag(relativePreferredLifetime, "upc");
    }

    if(amxd_object_get_value(bool, ipaddr_obj, "AutoDetected", NULL)) {
        rv = amxd_object_set_attr(ipaddr_obj, amxd_oattr_persistent, false);
        when_failed_trace(rv, exit, ERROR, "Failed to remove the persistent flag to the AutoDetected IP");
        // Auto detected addresses should not be user modifiable nor upgrade persistent
        amxc_llist_for_each(it, (&ipaddr_obj->parameters)) {
            amxd_param_t* param = amxc_llist_it_get_data(it, amxd_param_t, it);
            amxd_param_set_attr(param, amxd_pattr_read_only, true);
            amxd_param_unset_flag(param, "upc");
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void ipv6_addr_prefix_changed_cb(UNUSED const char* signame,
                                        const amxc_var_t* const data,
                                        UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    ipv6_addr_info_t* addr_info = NULL;
    ipv6_prefix_info_t* prefix_info = NULL;
    const amxc_ts_t* preferred_lt = NULL;
    const amxc_ts_t* valid_lt = NULL;
    uint32_t plt = 0;
    uint32_t vlt = 0;
    amxd_object_t* prefix_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    const char* prefix = NULL;
    amxc_string_t ipv6_address;
    amxc_var_t params;
    bool enable = false;

    amxc_string_init(&ipv6_address, 0);
    amxc_var_init(&params);

    when_null(priv, exit);
    addr_info = (ipv6_addr_info_t*) priv;
    prefix_info = (ipv6_prefix_info_t*) prefix_obj->priv;
    when_null_trace(prefix_info, exit, ERROR, "Could not get prefix info structure");

    amxd_object_get_params(prefix_obj, &params, amxd_dm_access_protected);
    prefix = GET_CHAR(&params, "Prefix");
    enable = GET_BOOL(&params, "Enable");

    if((prefix == NULL)
       || (*prefix == '\0')) {
        SAH_TRACEZ_INFO(ME, "Parent prefix was cleared");
    } else if((!enable
               && (prefix_info->prefix_origin == PREFIX_ORIGIN_STATIC)
               && (prefix_info->prefix_type == PREFIX_STATIC_TYPE_STATIC))) {
        SAH_TRACEZ_INFO(ME, "Parent prefix has been disabled while Origin and Type is Static");
    } else {
        amxc_string_set(&ipv6_address, prefix);
        ipv6_intf_id_set(addr_info, &ipv6_address); // In case of failure, still set ipv6_address in DM to clear value
    }

    preferred_lt = amxc_var_constcast(amxc_ts_t, GET_ARG(&params, "PreferredLifetime"));
    valid_lt = amxc_var_constcast(amxc_ts_t, GET_ARG(&params, "ValidLifetime"));
    vlt = GET_UINT32(&params, "RelativeValidLifetime");
    plt = GET_UINT32(&params, "RelativePreferredLifetime");
    dm_ipv6_address_set(amxc_string_get(&ipv6_address, 0), addr_info, preferred_lt, valid_lt, plt, vlt);

exit:
    amxc_string_clean(&ipv6_address);
    amxc_var_clean(&params);
    SAH_TRACEZ_OUT(ME);
    return;
}

static void nm_option3_cb(UNUSED const char* sig_name,
                          const amxc_var_t* result,
                          void* userdata) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipv6_addr_info_t* addr_info = NULL;
    amxc_var_t* option = NULL;
    amxc_ts_t preferred_lt;
    amxc_ts_t valid_lt;

    option = GETP_ARG(result, "0.Options.0");
    when_null_trace(userdata, exit, ERROR, "No userdata provided");
    addr_info = (ipv6_addr_info_t*) userdata;

    SAH_TRACEZ_INFO(ME, "Received an update to IPv6 Address '%s'",
                    amxd_object_get_name(addr_info->addr_obj, AMXD_OBJECT_NAMED));

    if(option == NULL) {
        when_failed_trace(make_ip_address_empty(addr_info->addr_obj, IP_VERSION_6), exit, ERROR, "Failed to clear DHCPv6 address in the datamodel");
    } else {
        uint32_t plt = GET_UINT32(option, "Value.PreferredLifetime");
        uint32_t vlt = GET_UINT32(option, "Value.ValidLifetime");
        calculate_lifetimes(plt, vlt, &preferred_lt, &valid_lt);
        rv = dm_ipv6_address_set(GETP_CHAR(option, "Value.Address"), addr_info, &preferred_lt, &valid_lt, plt, vlt);
        when_failed_trace(rv, exit, ERROR, "Failed to set DHCPv6 address in the datamodel");
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

int ipv6_addr_apply(ipv6_addr_info_t* addr_info) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t ctrlr_args;
    amxc_var_t ctrlr_ret;

    amxc_var_init(&ctrlr_args);
    amxc_var_init(&ctrlr_ret);
    when_null_trace(addr_info, exit, ERROR, "No address info provided");
    when_true_status(addr_info->auto_detected, exit, rv = 0);
    rv = ipv6address_build_ctrlr_args_from_object(addr_info->addr_obj, &ctrlr_args);
    when_true_trace(rv == 1, exit, INFO, "Failed to build IPv6 arguments for '%s', insufficient data", addr_info->addr_obj->name);
    when_failed_trace(rv, exit, ERROR, "Failed to build IPv6 arguments for '%s'", addr_info->addr_obj->name);
    if(GET_BOOL(&ctrlr_args, "combined_enable")) {
        switch(addr_info->origin) {
        case ADDR_ORIGIN_AUTOCONFIGURED: {
            const char* prefix_path = GET_CHAR(amxd_object_get_param_value(addr_info->addr_obj, "Prefix"), NULL);
            start_listening_for_changes(prefix_path, "Prefix", ipv6_addr_prefix_changed_cb, addr_info);
            rv = ipv6_generate_and_set_in_dm(addr_info, prefix_path);
            break;
        }
        case ADDR_ORIGIN_DHCPV6:
            // Start a NetModel query on DHCPv6 option 3
            rv = nm_open_dhcpoption_query(addr_info->intf_obj, &addr_info->nm_query, "req6", 3, nm_option3_cb, addr_info);
            break;
        case ADDR_ORIGIN_WELLKNOWN:
            // WellKnown addresses needs to be applied if already set
            rv = update_ipv6_address(addr_info);
            break;
        case ADDR_ORIGIN_STATIC:
            // Static addresses needs to be applied if already set
            rv = update_ipv6_address(addr_info);
            break;
        default:
            SAH_TRACEZ_ERROR(ME, "Unsupported origin");
            break;
        }
    }

exit:
    if((rv != 0) && (addr_info != NULL)) {
        ip_manager_set_status(addr_info->addr_obj, STATUS_ERROR);
    }
    amxc_var_clean(&ctrlr_args);
    amxc_var_clean(&ctrlr_ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void ipv6_address_stop_subscription(ipv6_addr_info_t* addr_info) {
    SAH_TRACEZ_IN(ME);

    when_null_trace(addr_info, exit, ERROR, "No address info structure provided");

    switch(addr_info->origin) {
    case ADDR_ORIGIN_AUTOCONFIGURED:
        stop_listening_for_prefix_changes(ipv6_addr_prefix_changed_cb, addr_info);
        make_ip_address_empty(addr_info->addr_obj, IP_VERSION_6);
        break;
    case ADDR_ORIGIN_DHCPV6:
        nm_close_query(&addr_info->nm_query);
        make_ip_address_empty(addr_info->addr_obj, IP_VERSION_6);
        break;
    case ADDR_ORIGIN_WELLKNOWN:
        break;
    case ADDR_ORIGIN_STATIC:
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "Unsupported origin");
        break;
    }

    ipv6_addr_info_subscription_clean(addr_info);

exit:
    SAH_TRACEZ_OUT(ME);
}

void _ipv6_address_added(UNUSED const char* const sig_name,
                         const amxc_var_t* const data,
                         UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_object_t* addr_tmpl_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    amxd_object_t* addr_obj = amxd_object_get_instance(addr_tmpl_obj, NULL, GET_UINT32(data, "index"));

    when_null_trace(addr_obj, exit, ERROR, "Could not find address address object with index %d", GET_UINT32(data, "index"));
    rv = init_ipv6(addr_obj);
    when_failed_trace(rv, exit, ERROR, "Failed to initialise new IPv6 address");

    ipv6_addr_apply((ipv6_addr_info_t*) addr_obj->priv);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ipv6_interface_id_changed(UNUSED const char* const sig_name,
                                const amxc_var_t* const data,
                                UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipv6_addr_info_t* addr_info = NULL;
    const char* prefix_path = NULL;
    const char* prefix = NULL;
    amxc_ts_t* preferred_lt = NULL;
    amxc_ts_t* valid_lt = NULL;
    uint32_t plt = 0;
    uint32_t vlt = 0;
    amxd_object_t* prefix_obj = NULL;
    amxd_object_t* addr_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    amxc_string_t ipv6_address;

    amxc_string_init(&ipv6_address, 0);

    when_null(addr_obj, exit);
    when_null(addr_obj->priv, exit);
    addr_info = (ipv6_addr_info_t*) addr_obj->priv;

    when_false(addr_info->intf_id_type == FIXED_INTF_ID, exit);

    prefix_path = GET_CHAR(amxd_object_get_param_value(addr_info->addr_obj, "Prefix"), NULL);
    prefix_obj = get_object_from_device_path(prefix_path);
    prefix = GET_CHAR(amxd_object_get_param_value(prefix_obj, "Prefix"), NULL);
    amxc_string_set(&ipv6_address, prefix);

    ipv6_intf_id_set(addr_info, &ipv6_address); // In case of failure, still set ipv6_address in DM to clear value

    preferred_lt = amxd_object_get_value(amxc_ts_t, prefix_obj, "PreferredLifetime", NULL);
    valid_lt = amxd_object_get_value(amxc_ts_t, prefix_obj, "ValidLifetime", NULL);
    plt = amxd_object_get_value(uint32_t, prefix_obj, "RelativePreferredLifetime", NULL);
    vlt = amxd_object_get_value(uint32_t, prefix_obj, "RelativeValidLifetime", NULL);
    rv = dm_ipv6_address_set(amxc_string_get(&ipv6_address, 0), addr_info, preferred_lt, valid_lt, plt, vlt);
    when_failed(rv, exit);

exit:
    free(preferred_lt);
    free(valid_lt);
    amxc_string_clean(&ipv6_address);
    SAH_TRACEZ_OUT(ME);
    return;
}

amxd_status_t _ipv6_addr_destroy(amxd_object_t* object,
                                 UNUSED amxd_param_t* param,
                                 amxd_action_t reason,
                                 UNUSED const amxc_var_t* const args,
                                 UNUSED amxc_var_t* const retval,
                                 UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    ipv6_addr_info_t* addr_info = NULL;
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxc_var_t ctrlr_args;
    amxc_var_t ret;

    amxc_var_init(&ctrlr_args);
    amxc_var_init(&ret);

    if(reason != action_object_destroy) {
        SAH_TRACEZ_NOTICE(ME, "Wrong reason, expected action_object_destroy(%d) got %d", action_object_destroy, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(object, exit, ERROR, "Object can not be NULL, invalid argument");
    when_null_trace(object->priv, exit, INFO, "Object has no private data, nothing to destroy");
    addr_info = (ipv6_addr_info_t*) object->priv;

    if(!addr_info->auto_detected && (ipv6address_build_ctrlr_args_from_object(object, &ctrlr_args) == 0)) {
        cfgctrlr_execute_function(object, "delete-ipv6address", &ctrlr_args, &ret);
    }

    object->priv = NULL;
    ipv6_address_stop_subscription(addr_info);
    rv = ipv6_addr_info_clean(&addr_info);

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&ctrlr_args);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void _ipv6_address_update(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t ctrlr_args;
    amxc_var_t ret;
    ipv6_addr_info_t* addr_info = NULL;
    amxd_object_t* addr_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    amxd_object_t* intf_obj = amxd_object_findf(addr_obj, "^.^.");
    const char* addr_to = GETP_CHAR(data, "parameters.IPAddress.to");

    amxc_var_init(&ctrlr_args);
    amxc_var_init(&ret);

    when_null_trace(addr_obj, exit, ERROR, "No IPv6 address object found");
    when_null_trace(addr_obj->priv, exit, ERROR, "No private data in this IPv6 address object found");
    addr_info = (ipv6_addr_info_t*) addr_obj->priv;

    when_true_status(addr_info->auto_detected, exit, rv = 0);

    rv = ipv6address_build_ctrlr_args_from_object(addr_obj, &ctrlr_args);
    when_false_trace(netdev_name_known(intf_obj), exit, WARNING, "NetDev Name not known");
    when_failed_trace(rv, exit, ERROR, "Failed to build IPv6 arguments");

    if((addr_to == NULL) || (*addr_to == '\0')) {
        amxc_var_t* parameters = GET_ARG(&ctrlr_args, "parameters");
        amxc_var_set_key(parameters, "IPAddress",
                         GETP_ARG(data, "parameters.IPAddress.from"),
                         AMXC_VAR_FLAG_UPDATE | AMXC_VAR_FLAG_COPY);

        ip_manager_set_status(addr_obj, GET_BOOL(&ctrlr_args, "combined_enable") ? STATUS_ERROR : STATUS_DISABLED);
        rv = cfgctrlr_execute_function(addr_obj, "delete-ipv6address", &ctrlr_args, &ret);
        when_true_trace(rv == 1, exit, INFO, "Insufficient data provided to 'delete-ipv6address'");
        when_failed_trace(rv, exit, ERROR, "Failed to delete IPv6 Address");
    } else {
        amxc_var_add_key(cstring_t, &ctrlr_args, "OldIPAddress", GETP_CHAR(data, "parameters.IPAddress.from"));
        rv = cfgctrlr_execute_function(addr_obj, "update-ipv6address", &ctrlr_args, &ret);
        subscribe_if_added_to_netdev(intf_obj, addr_obj, addr_to, IP_VERSION_6);
        when_true_trace(rv == 1, exit, INFO, "Insufficient data provided to 'update-ipv6address'");
        when_failed_trace(rv, exit, ERROR, "Failed to Update IPv6 Address");
    }

    SAH_TRACEZ_INFO(ME, "Update of IP '%s' successful", addr_obj->name);

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&ctrlr_args);
    SAH_TRACEZ_OUT(ME);
}

void _ipv6_address_enable_changed(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipv6_addr_info_t* addr_info = NULL;
    amxd_object_t* addr_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);

    when_null_trace(addr_obj, exit, ERROR, "No IPv6 address object found");
    when_null_trace(addr_obj->priv, exit, ERROR, "No private data in this IPv6 address object found");
    addr_info = (ipv6_addr_info_t*) addr_obj->priv;

    if(GETP_BOOL(data, "parameters.Enable.to")) {
        rv = ipv6_addr_apply(addr_info);
        when_failed_trace(rv, exit, ERROR, "Failed to enable IPv6Address '%s'", addr_obj->name);
    } else {
        rv = ipv6_addr_disable(addr_info);
        when_failed_trace(rv, exit, ERROR, "Failed to disable IPv6Address '%s'", addr_obj->name);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ipv6_address_prefix_changed(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    ipv6_addr_info_t* addr_info = NULL;
    amxd_object_t* addr_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    const char* new_prefix_path = GETP_CHAR(data, "parameters.Prefix.to");

    when_null_trace(addr_obj, exit, ERROR, "No object found");
    when_null_trace(addr_obj->priv, exit, WARNING, "Object has no private data, address can not be changed");
    addr_info = (ipv6_addr_info_t*) addr_obj->priv;

    // Ignore prefix changes to types not listening to prefix
    if(addr_info->origin == ADDR_ORIGIN_AUTOCONFIGURED) {
        ipv6_addr_disable(addr_info);
        if((new_prefix_path != NULL) && (*new_prefix_path != '\0')) {
            // new prefix path, start listing for changes and create/set a new address
            ipv6_addr_apply(addr_info);
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ipv6_address_origin_changed(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {

    SAH_TRACEZ_IN(ME);
    amxd_object_t* addr_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    ipv6_addr_origin_t origin_info = addr_origin_to_enum(GETP_CHAR(data, "parameters.Origin.to"));
    ipv6_addr_info_t* addr_info = NULL;
    bool is_persistent = false;

    when_null_trace(addr_obj, exit, ERROR, "No object found");
    when_null_trace(addr_obj->priv, exit, ERROR, "No private data in this IPv6 address object found");
    addr_info = (ipv6_addr_info_t*) addr_obj->priv;

    is_persistent = ((origin_info == ADDR_ORIGIN_WELLKNOWN) || (origin_info == ADDR_ORIGIN_STATIC));

    if(amxd_object_get_value(bool, addr_info->addr_obj, "AutoDetected", NULL)) {
        is_persistent = false;
    }

    ipv6_toggle_persistency(addr_obj, is_persistent);
    toggle_lifetime_read_only(addr_info->addr_obj, (addr_info->origin != ADDR_ORIGIN_STATIC));

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ipv6_address_preferred_lt_changed(UNUSED const char* const sig_name,
                                        const amxc_var_t* const data,
                                        UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* addr_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    ipv6_addr_info_t* addr_info = NULL;
    const amxc_ts_t* lifetime = NULL;
    char lt[36];

    when_null_trace(addr_obj, exit, ERROR, "No object found");
    when_null_trace(addr_obj->priv, exit, ERROR, "Object has no private data, address can not be changed");
    addr_info = (ipv6_addr_info_t*) addr_obj->priv;

    amxp_timer_stop(addr_info->preferred_timer);
    lifetime = amxc_var_constcast(amxc_ts_t, GETP_ARG(data, "parameters.PreferredLifetime.to"));
    when_null_trace(lifetime, exit, ERROR, "Failed to get the PreferredLifetime");

    if(!addr_info->auto_detected &&
       ((addr_info->origin == ADDR_ORIGIN_STATIC) ||
        (addr_info->origin == ADDR_ORIGIN_WELLKNOWN))) {
        calculate_and_set_relative_lifetimes(lifetime, addr_obj, "RelativePreferredLifetime");
    }

    amxc_ts_format_precision(lifetime, lt, sizeof(lt), 0);
    if(strncmp(lt, UNKNOWN_LIFETIME, 36) != 0) {
        preferred_timer_start(ADDR_STATUS_PARAM, addr_info->addr_obj, addr_info->preferred_timer);
    } else if(addr_info->valid_timer->state != amxp_timer_running) {
        set_status(ADDR_STATUS_PARAM, addr_info->addr_obj, ADDR_STATUS_UNKNOWN);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ipv6_address_valid_lt_changed(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* addr_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    ipv6_addr_info_t* addr_info = NULL;
    const amxc_ts_t* lifetime = NULL;
    char lt[36];

    when_null_trace(addr_obj, exit, ERROR, "No object found");
    when_null_trace(addr_obj->priv, exit, ERROR, "Object has no private data, address can not be changed");
    addr_info = (ipv6_addr_info_t*) addr_obj->priv;

    amxp_timer_stop(addr_info->valid_timer);
    lifetime = amxc_var_constcast(amxc_ts_t, GETP_ARG(data, "parameters.ValidLifetime.to"));
    when_null_trace(lifetime, exit, ERROR, "Failed to get the ValidLifetime");

    if(!addr_info->auto_detected &&
       ((addr_info->origin == ADDR_ORIGIN_STATIC) ||
        (addr_info->origin == ADDR_ORIGIN_WELLKNOWN))) {
        calculate_and_set_relative_lifetimes(lifetime, addr_obj, "RelativeValidLifetime");
    }

    amxc_ts_format_precision(lifetime, lt, sizeof(lt), 0);
    if(strncmp(lt, UNKNOWN_LIFETIME, 36) != 0) {
        valid_timer_start(ADDR_STATUS_PARAM, addr_info->addr_obj, addr_info->valid_timer);
    } else if(addr_info->preferred_timer->state != amxp_timer_running) {
        set_status(ADDR_STATUS_PARAM, addr_info->addr_obj, PREFIX_STATUS_UNKNOWN);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

int init_ipv6(amxd_object_t* addr_obj) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipv6_addr_info_t* addr_info = NULL;
    bool is_persistent = false;

    when_null_trace(addr_obj, exit, WARNING, "No address object provided to initialise");

    // auto_detected instances (like lla) will have their private data created when they are added to the DM
    // not when the add event is handled
    if(addr_obj->priv == NULL) {
        ipv6_addr_info_new(&addr_info, addr_obj, false);
    } else {
        addr_info = (ipv6_addr_info_t*) addr_obj->priv;
    }
    when_null_trace(addr_info, exit, ERROR, "No address info provided");

    if(((addr_info->origin == ADDR_ORIGIN_WELLKNOWN) || (addr_info->origin == ADDR_ORIGIN_STATIC))) {
        if(!addr_info->auto_detected) {
            init_relative_lifetimes(addr_info->addr_obj);
        }
        lifetime_timers_start(ADDR_STATUS_PARAM,
                              addr_info->addr_obj,
                              addr_info->preferred_timer,
                              addr_info->valid_timer);
        is_persistent = true;
    }

    if(addr_info->auto_detected) {
        is_persistent = false;
    }

    rv = ipv6_toggle_persistency(addr_info->addr_obj, is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to toggle persistency for '%s'", addr_obj->name);
    rv = toggle_lifetime_read_only(addr_info->addr_obj, (addr_info->origin != ADDR_ORIGIN_STATIC));
    when_failed_trace(rv, exit, ERROR, "Failed to toggle read-only for '%s'", addr_obj->name);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void _ipv6_address_dhcpv6_or_slaac_changed(UNUSED const char* const sig_name,
                                           const amxc_var_t* const data,
                                           UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* addr_obj = amxd_dm_signal_get_object(ip_manager_get_dm(), data);
    ipv6_addr_info_t* addr_info = NULL;
    amxd_object_t* intf_obj = NULL;
    amxd_object_t* dhcp_iapd_addr = NULL;
    const char* status = GETP_CHAR(data, "parameters.Status.to");

    when_null_trace(addr_obj, exit, ERROR, "No IPv6Address object found");
    addr_info = (ipv6_addr_info_t*) addr_obj->priv;
    when_null_trace(addr_obj, exit, ERROR, "IPv6Address has no private data");

    if(addr_info->origin == ADDR_ORIGIN_AUTOCONFIGURED) {
        amxd_object_t* prefix_obj = get_prefix_object_from_addr(addr_obj);
        ipv6_prefix_info_t* prefix_info = NULL;
        when_null_trace(prefix_obj, exit, ERROR, "Prefix object is NULL");
        prefix_info = (ipv6_prefix_info_t*) prefix_obj->priv;
        when_null_trace(prefix_info, exit, ERROR, "Prefix object has no private data");

        when_false_trace(prefix_info->prefix_origin == PREFIX_ORIGIN_ROUTERADVERTISEMENT,
                         exit, INFO, "Autoconfigured IPv6Address status changed but address not configured via SLAAC");
    } else {
        when_false_trace(addr_info->origin == ADDR_ORIGIN_DHCPV6,
                         exit, INFO, "IPv6Address status changed but address not configured via SLAAC or DHCPv6");
    }

    intf_obj = amxd_object_findf(addr_obj, "^.^.");
    when_null_trace(intf_obj, exit, ERROR, "Could not find matching interface object for IPv6Address");
    dhcp_iapd_addr = amxd_object_findf(intf_obj, "IPv6Address.[Alias == '" DHCP_IAPD_ALIAS "'].");
    when_null_trace(dhcp_iapd_addr, exit, ERROR, "Could not find " DHCP_IAPD_ALIAS " IPv6Address instance");

    if(strcmp(status, "Enabled") == 0) {
        amxd_trans_t* trans = ip_manager_trans_create(dhcp_iapd_addr);
        amxd_trans_set_value(bool, trans, "Enable", false);
        ip_manager_trans_apply(trans);
    } else {
        amxc_llist_t paths;
        const char* ipv6_addr_delegate = GET_CHAR(amxd_object_get_param_value(intf_obj, "IPv6AddressDelegate"), NULL);

        amxc_llist_init(&paths);
        amxd_object_resolve_pathf(intf_obj, &paths,
                                  "IPv6Address.[(Origin == 'DHCPv6' || Origin == 'AutoConfigured') && TypeFlags == '@gua' && Status == 'Enabled'].");
        if(amxc_llist_is_empty(&paths) && str_empty(ipv6_addr_delegate)) {
            amxd_trans_t* trans = ip_manager_trans_create(dhcp_iapd_addr);
            amxd_trans_set_value(bool, trans, "Enable", true);
            ip_manager_trans_apply(trans);
        }
        amxc_llist_clean(&paths, NULL);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
