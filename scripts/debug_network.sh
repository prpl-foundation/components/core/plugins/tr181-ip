#!/bin/sh

LIBDEBUG=/usr/lib/debuginfo/debug_functions.sh

if [ ! -f "$LIBDEBUG" ]; then
    echo "No debug library found : $LIBDEBUG"
    exit 1
fi

. $LIBDEBUG

#Detect if ip is busybox limited implementation
IS_BUSYBOX_IP=$(ip -help 2>&1 | grep "BusyBox")

show_title "show ip addres/link information"
show_cmd ip address show
if [ -z "$IS_BUSYBOX_IP" ] ; then
	show_cmd ip -s link
else
	show_cmd ip link
fi
show_cmd tc -s qdisc
show_cmd netstat -lan

show_title "show ip routes"
show_cmd ip -4 route
show_cmd ip -6 route

show_title "show ip neighbours"

if [ -z "$IS_BUSYBOX_IP" ] ; then
	show_cmd ip -B n
fi
show_cmd ip -4 n
show_cmd ip -6 n
